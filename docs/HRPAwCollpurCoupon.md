# HRPAwCollpurCoupon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**dealId** | **NSNumber*** |  | 
**purchaseId** | **NSNumber*** |  | 
**couponCode** | **NSString*** |  | 
**status** | **NSString*** |  | 
**couponDeliveryDatetime** | **NSDate*** |  | [optional] 
**couponDateUpdated** | **NSDate*** |  | [optional] 
**redeemedAt** | **NSDate*** |  | [optional] 
**redeemedByTerminal** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


