# HRPNotificationRelated

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **NSNumber*** |  | [optional] [default to @0.0]
**brandId** | **NSNumber*** |  | [optional] [default to @0.0]
**venueId** | **NSNumber*** |  | [optional] [default to @0.0]
**competitionId** | **NSNumber*** |  | [optional] [default to @0.0]
**couponId** | **NSNumber*** |  | [optional] [default to @0.0]
**eventId** | **NSNumber*** |  | [optional] [default to @0.0]
**dealPaidId** | **NSNumber*** |  | [optional] [default to @0.0]
**dealGroupId** | **NSNumber*** |  | [optional] [default to @0.0]
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


