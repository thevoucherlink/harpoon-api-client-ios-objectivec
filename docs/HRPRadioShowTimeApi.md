# HRPRadioShowTimeApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioShowTimeCount**](HRPRadioShowTimeApi.md#radioshowtimecount) | **GET** /RadioShowTimes/count | Count instances of the model matched by where from the data source.
[**radioShowTimeCreate**](HRPRadioShowTimeApi.md#radioshowtimecreate) | **POST** /RadioShowTimes | Create a new instance of the model and persist it into the data source.
[**radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream**](HRPRadioShowTimeApi.md#radioshowtimecreatechangestreamgetradioshowtimeschangestream) | **GET** /RadioShowTimes/change-stream | Create a change stream.
[**radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream**](HRPRadioShowTimeApi.md#radioshowtimecreatechangestreampostradioshowtimeschangestream) | **POST** /RadioShowTimes/change-stream | Create a change stream.
[**radioShowTimeDeleteById**](HRPRadioShowTimeApi.md#radioshowtimedeletebyid) | **DELETE** /RadioShowTimes/{id} | Delete a model instance by {{id}} from the data source.
[**radioShowTimeExistsGetRadioShowTimesidExists**](HRPRadioShowTimeApi.md#radioshowtimeexistsgetradioshowtimesidexists) | **GET** /RadioShowTimes/{id}/exists | Check whether a model instance exists in the data source.
[**radioShowTimeExistsHeadRadioShowTimesid**](HRPRadioShowTimeApi.md#radioshowtimeexistsheadradioshowtimesid) | **HEAD** /RadioShowTimes/{id} | Check whether a model instance exists in the data source.
[**radioShowTimeFind**](HRPRadioShowTimeApi.md#radioshowtimefind) | **GET** /RadioShowTimes | Find all instances of the model matched by filter from the data source.
[**radioShowTimeFindById**](HRPRadioShowTimeApi.md#radioshowtimefindbyid) | **GET** /RadioShowTimes/{id} | Find a model instance by {{id}} from the data source.
[**radioShowTimeFindOne**](HRPRadioShowTimeApi.md#radioshowtimefindone) | **GET** /RadioShowTimes/findOne | Find first instance of the model matched by filter from the data source.
[**radioShowTimePrototypeGetRadioShow**](HRPRadioShowTimeApi.md#radioshowtimeprototypegetradioshow) | **GET** /RadioShowTimes/{id}/radioShow | Fetches belongsTo relation radioShow.
[**radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid**](HRPRadioShowTimeApi.md#radioshowtimeprototypeupdateattributespatchradioshowtimesid) | **PATCH** /RadioShowTimes/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid**](HRPRadioShowTimeApi.md#radioshowtimeprototypeupdateattributesputradioshowtimesid) | **PUT** /RadioShowTimes/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowTimeReplaceById**](HRPRadioShowTimeApi.md#radioshowtimereplacebyid) | **POST** /RadioShowTimes/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioShowTimeReplaceOrCreate**](HRPRadioShowTimeApi.md#radioshowtimereplaceorcreate) | **POST** /RadioShowTimes/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioShowTimeUpdateAll**](HRPRadioShowTimeApi.md#radioshowtimeupdateall) | **POST** /RadioShowTimes/update | Update instances of the model matched by {{where}} from the data source.
[**radioShowTimeUpsertPatchRadioShowTimes**](HRPRadioShowTimeApi.md#radioshowtimeupsertpatchradioshowtimes) | **PATCH** /RadioShowTimes | Patch an existing model instance or insert a new one into the data source.
[**radioShowTimeUpsertPutRadioShowTimes**](HRPRadioShowTimeApi.md#radioshowtimeupsertputradioshowtimes) | **PUT** /RadioShowTimes | Patch an existing model instance or insert a new one into the data source.
[**radioShowTimeUpsertWithWhere**](HRPRadioShowTimeApi.md#radioshowtimeupsertwithwhere) | **POST** /RadioShowTimes/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **radioShowTimeCount**
```objc
-(NSNumber*) radioShowTimeCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance radioShowTimeCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeCreate**
```objc
-(NSNumber*) radioShowTimeCreateWithData: (HRPRadioShowTime*) data
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioShowTime* data = [[HRPRadioShowTime alloc] init]; // Model instance data (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance radioShowTimeCreateWithData:data
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioShowTime***](HRPRadioShowTime*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream**
```objc
-(NSNumber*) radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Create a change stream.
[apiInstance radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream**
```objc
-(NSNumber*) radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Create a change stream.
[apiInstance radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeDeleteById**
```objc
-(NSNumber*) radioShowTimeDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance radioShowTimeDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeExistsGetRadioShowTimesidExists**
```objc
-(NSNumber*) radioShowTimeExistsGetRadioShowTimesidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance radioShowTimeExistsGetRadioShowTimesidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeExistsGetRadioShowTimesidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeExistsHeadRadioShowTimesid**
```objc
-(NSNumber*) radioShowTimeExistsHeadRadioShowTimesidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance radioShowTimeExistsHeadRadioShowTimesidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeExistsHeadRadioShowTimesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeFind**
```objc
-(NSNumber*) radioShowTimeFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRadioShowTime>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance radioShowTimeFindWithFilter:filter
          completionHandler: ^(NSArray<HRPRadioShowTime>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPRadioShowTime>***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeFindById**
```objc
-(NSNumber*) radioShowTimeFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance radioShowTimeFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeFindOne**
```objc
-(NSNumber*) radioShowTimeFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance radioShowTimeFindOneWithFilter:filter
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimePrototypeGetRadioShow**
```objc
-(NSNumber*) radioShowTimePrototypeGetRadioShowWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Fetches belongsTo relation radioShow.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShowTime id
NSNumber* refresh = @true; //  (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Fetches belongsTo relation radioShow.
[apiInstance radioShowTimePrototypeGetRadioShowWithId:_id
              refresh:refresh
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimePrototypeGetRadioShow: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShowTime id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid**
```objc
-(NSNumber*) radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesidWithId: (NSString*) _id
    data: (HRPRadioShowTime*) data
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShowTime id
HRPRadioShowTime* data = [[HRPRadioShowTime alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesidWithId:_id
              data:data
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShowTime id | 
 **data** | [**HRPRadioShowTime***](HRPRadioShowTime*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid**
```objc
-(NSNumber*) radioShowTimePrototypeUpdateAttributesPutRadioShowTimesidWithId: (NSString*) _id
    data: (HRPRadioShowTime*) data
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShowTime id
HRPRadioShowTime* data = [[HRPRadioShowTime alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance radioShowTimePrototypeUpdateAttributesPutRadioShowTimesidWithId:_id
              data:data
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShowTime id | 
 **data** | [**HRPRadioShowTime***](HRPRadioShowTime*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeReplaceById**
```objc
-(NSNumber*) radioShowTimeReplaceByIdWithId: (NSString*) _id
    data: (HRPRadioShowTime*) data
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPRadioShowTime* data = [[HRPRadioShowTime alloc] init]; // Model instance data (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance radioShowTimeReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPRadioShowTime***](HRPRadioShowTime*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeReplaceOrCreate**
```objc
-(NSNumber*) radioShowTimeReplaceOrCreateWithData: (HRPRadioShowTime*) data
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioShowTime* data = [[HRPRadioShowTime alloc] init]; // Model instance data (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance radioShowTimeReplaceOrCreateWithData:data
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioShowTime***](HRPRadioShowTime*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeUpdateAll**
```objc
-(NSNumber*) radioShowTimeUpdateAllWithWhere: (NSString*) where
    data: (HRPRadioShowTime*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRadioShowTime* data = [[HRPRadioShowTime alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance radioShowTimeUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRadioShowTime***](HRPRadioShowTime*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeUpsertPatchRadioShowTimes**
```objc
-(NSNumber*) radioShowTimeUpsertPatchRadioShowTimesWithData: (HRPRadioShowTime*) data
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioShowTime* data = [[HRPRadioShowTime alloc] init]; // Model instance data (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance radioShowTimeUpsertPatchRadioShowTimesWithData:data
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeUpsertPatchRadioShowTimes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioShowTime***](HRPRadioShowTime*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeUpsertPutRadioShowTimes**
```objc
-(NSNumber*) radioShowTimeUpsertPutRadioShowTimesWithData: (HRPRadioShowTime*) data
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioShowTime* data = [[HRPRadioShowTime alloc] init]; // Model instance data (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance radioShowTimeUpsertPutRadioShowTimesWithData:data
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeUpsertPutRadioShowTimes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioShowTime***](HRPRadioShowTime*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowTimeUpsertWithWhere**
```objc
-(NSNumber*) radioShowTimeUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPRadioShowTime*) data
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRadioShowTime* data = [[HRPRadioShowTime alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioShowTimeApi*apiInstance = [[HRPRadioShowTimeApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance radioShowTimeUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowTimeApi->radioShowTimeUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRadioShowTime***](HRPRadioShowTime*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

