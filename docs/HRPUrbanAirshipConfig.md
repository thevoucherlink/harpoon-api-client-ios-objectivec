# HRPUrbanAirshipConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**developmentAppKey** | **NSString*** |  | 
**developmentAppSecret** | **NSString*** |  | [optional] 
**developmentLogLevel** | **NSString*** |  | [optional] [default to @"debug"]
**productionAppKey** | **NSString*** |  | [optional] 
**productionAppSecret** | **NSString*** |  | [optional] 
**productionLogLevel** | **NSString*** |  | [optional] [default to @"none"]
**inProduction** | **NSNumber*** |  | [optional] [default to @0]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


