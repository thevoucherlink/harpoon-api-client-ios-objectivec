# HRPDealPaidPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **NSNumber*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**code** | [**HRPCouponPurchaseCode***](HRPCouponPurchaseCode.md) |  | [optional] 
**isAvailable** | **NSNumber*** |  | [optional] [default to @0]
**createdAt** | **NSDate*** |  | [optional] 
**expiredAt** | **NSDate*** |  | [optional] 
**redeemedAt** | **NSDate*** |  | [optional] 
**redeemedByTerminal** | **NSString*** |  | [optional] 
**redemptionType** | **NSString*** |  | [optional] 
**status** | **NSString*** |  | [optional] [default to @"unknown"]
**unlockTime** | **NSNumber*** |  | [optional] [default to @15.0]
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


