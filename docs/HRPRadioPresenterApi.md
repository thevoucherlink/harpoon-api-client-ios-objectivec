# HRPRadioPresenterApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioPresenterCount**](HRPRadioPresenterApi.md#radiopresentercount) | **GET** /RadioPresenters/count | Count instances of the model matched by where from the data source.
[**radioPresenterCreate**](HRPRadioPresenterApi.md#radiopresentercreate) | **POST** /RadioPresenters | Create a new instance of the model and persist it into the data source.
[**radioPresenterCreateChangeStreamGetRadioPresentersChangeStream**](HRPRadioPresenterApi.md#radiopresentercreatechangestreamgetradiopresenterschangestream) | **GET** /RadioPresenters/change-stream | Create a change stream.
[**radioPresenterCreateChangeStreamPostRadioPresentersChangeStream**](HRPRadioPresenterApi.md#radiopresentercreatechangestreampostradiopresenterschangestream) | **POST** /RadioPresenters/change-stream | Create a change stream.
[**radioPresenterDeleteById**](HRPRadioPresenterApi.md#radiopresenterdeletebyid) | **DELETE** /RadioPresenters/{id} | Delete a model instance by {{id}} from the data source.
[**radioPresenterExistsGetRadioPresentersidExists**](HRPRadioPresenterApi.md#radiopresenterexistsgetradiopresentersidexists) | **GET** /RadioPresenters/{id}/exists | Check whether a model instance exists in the data source.
[**radioPresenterExistsHeadRadioPresentersid**](HRPRadioPresenterApi.md#radiopresenterexistsheadradiopresentersid) | **HEAD** /RadioPresenters/{id} | Check whether a model instance exists in the data source.
[**radioPresenterFind**](HRPRadioPresenterApi.md#radiopresenterfind) | **GET** /RadioPresenters | Find all instances of the model matched by filter from the data source.
[**radioPresenterFindById**](HRPRadioPresenterApi.md#radiopresenterfindbyid) | **GET** /RadioPresenters/{id} | Find a model instance by {{id}} from the data source.
[**radioPresenterFindOne**](HRPRadioPresenterApi.md#radiopresenterfindone) | **GET** /RadioPresenters/findOne | Find first instance of the model matched by filter from the data source.
[**radioPresenterPrototypeCountRadioShows**](HRPRadioPresenterApi.md#radiopresenterprototypecountradioshows) | **GET** /RadioPresenters/{id}/radioShows/count | Counts radioShows of RadioPresenter.
[**radioPresenterPrototypeCreateRadioShows**](HRPRadioPresenterApi.md#radiopresenterprototypecreateradioshows) | **POST** /RadioPresenters/{id}/radioShows | Creates a new instance in radioShows of this model.
[**radioPresenterPrototypeDeleteRadioShows**](HRPRadioPresenterApi.md#radiopresenterprototypedeleteradioshows) | **DELETE** /RadioPresenters/{id}/radioShows | Deletes all radioShows of this model.
[**radioPresenterPrototypeDestroyByIdRadioShows**](HRPRadioPresenterApi.md#radiopresenterprototypedestroybyidradioshows) | **DELETE** /RadioPresenters/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
[**radioPresenterPrototypeExistsRadioShows**](HRPRadioPresenterApi.md#radiopresenterprototypeexistsradioshows) | **HEAD** /RadioPresenters/{id}/radioShows/rel/{fk} | Check the existence of radioShows relation to an item by id.
[**radioPresenterPrototypeFindByIdRadioShows**](HRPRadioPresenterApi.md#radiopresenterprototypefindbyidradioshows) | **GET** /RadioPresenters/{id}/radioShows/{fk} | Find a related item by id for radioShows.
[**radioPresenterPrototypeGetRadioShows**](HRPRadioPresenterApi.md#radiopresenterprototypegetradioshows) | **GET** /RadioPresenters/{id}/radioShows | Queries radioShows of RadioPresenter.
[**radioPresenterPrototypeLinkRadioShows**](HRPRadioPresenterApi.md#radiopresenterprototypelinkradioshows) | **PUT** /RadioPresenters/{id}/radioShows/rel/{fk} | Add a related item by id for radioShows.
[**radioPresenterPrototypeUnlinkRadioShows**](HRPRadioPresenterApi.md#radiopresenterprototypeunlinkradioshows) | **DELETE** /RadioPresenters/{id}/radioShows/rel/{fk} | Remove the radioShows relation to an item by id.
[**radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid**](HRPRadioPresenterApi.md#radiopresenterprototypeupdateattributespatchradiopresentersid) | **PATCH** /RadioPresenters/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterPrototypeUpdateAttributesPutRadioPresentersid**](HRPRadioPresenterApi.md#radiopresenterprototypeupdateattributesputradiopresentersid) | **PUT** /RadioPresenters/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterPrototypeUpdateByIdRadioShows**](HRPRadioPresenterApi.md#radiopresenterprototypeupdatebyidradioshows) | **PUT** /RadioPresenters/{id}/radioShows/{fk} | Update a related item by id for radioShows.
[**radioPresenterReplaceById**](HRPRadioPresenterApi.md#radiopresenterreplacebyid) | **POST** /RadioPresenters/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioPresenterReplaceOrCreate**](HRPRadioPresenterApi.md#radiopresenterreplaceorcreate) | **POST** /RadioPresenters/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioPresenterUpdateAll**](HRPRadioPresenterApi.md#radiopresenterupdateall) | **POST** /RadioPresenters/update | Update instances of the model matched by {{where}} from the data source.
[**radioPresenterUploadImage**](HRPRadioPresenterApi.md#radiopresenteruploadimage) | **POST** /RadioPresenters/{id}/file/image | 
[**radioPresenterUpsertPatchRadioPresenters**](HRPRadioPresenterApi.md#radiopresenterupsertpatchradiopresenters) | **PATCH** /RadioPresenters | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterUpsertPutRadioPresenters**](HRPRadioPresenterApi.md#radiopresenterupsertputradiopresenters) | **PUT** /RadioPresenters | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterUpsertWithWhere**](HRPRadioPresenterApi.md#radiopresenterupsertwithwhere) | **POST** /RadioPresenters/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **radioPresenterCount**
```objc
-(NSNumber*) radioPresenterCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance radioPresenterCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterCreate**
```objc
-(NSNumber*) radioPresenterCreateWithData: (HRPRadioPresenter*) data
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioPresenter* data = [[HRPRadioPresenter alloc] init]; // Model instance data (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance radioPresenterCreateWithData:data
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioPresenter***](HRPRadioPresenter*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterCreateChangeStreamGetRadioPresentersChangeStream**
```objc
-(NSNumber*) radioPresenterCreateChangeStreamGetRadioPresentersChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Create a change stream.
[apiInstance radioPresenterCreateChangeStreamGetRadioPresentersChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterCreateChangeStreamGetRadioPresentersChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterCreateChangeStreamPostRadioPresentersChangeStream**
```objc
-(NSNumber*) radioPresenterCreateChangeStreamPostRadioPresentersChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Create a change stream.
[apiInstance radioPresenterCreateChangeStreamPostRadioPresentersChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterCreateChangeStreamPostRadioPresentersChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterDeleteById**
```objc
-(NSNumber*) radioPresenterDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance radioPresenterDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterExistsGetRadioPresentersidExists**
```objc
-(NSNumber*) radioPresenterExistsGetRadioPresentersidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance radioPresenterExistsGetRadioPresentersidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterExistsGetRadioPresentersidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterExistsHeadRadioPresentersid**
```objc
-(NSNumber*) radioPresenterExistsHeadRadioPresentersidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance radioPresenterExistsHeadRadioPresentersidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterExistsHeadRadioPresentersid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterFind**
```objc
-(NSNumber*) radioPresenterFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRadioPresenter>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance radioPresenterFindWithFilter:filter
          completionHandler: ^(NSArray<HRPRadioPresenter>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPRadioPresenter>***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterFindById**
```objc
-(NSNumber*) radioPresenterFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance radioPresenterFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterFindOne**
```objc
-(NSNumber*) radioPresenterFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance radioPresenterFindOneWithFilter:filter
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterPrototypeCountRadioShows**
```objc
-(NSNumber*) radioPresenterPrototypeCountRadioShowsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts radioShows of RadioPresenter.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioPresenter id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Counts radioShows of RadioPresenter.
[apiInstance radioPresenterPrototypeCountRadioShowsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterPrototypeCountRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioPresenter id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterPrototypeCreateRadioShows**
```objc
-(NSNumber*) radioPresenterPrototypeCreateRadioShowsWithId: (NSString*) _id
    data: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Creates a new instance in radioShows of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioPresenter id
HRPRadioShow* data = [[HRPRadioShow alloc] init]; //  (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Creates a new instance in radioShows of this model.
[apiInstance radioPresenterPrototypeCreateRadioShowsWithId:_id
              data:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterPrototypeCreateRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioPresenter id | 
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)|  | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterPrototypeDeleteRadioShows**
```objc
-(NSNumber*) radioPresenterPrototypeDeleteRadioShowsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all radioShows of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioPresenter id

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Deletes all radioShows of this model.
[apiInstance radioPresenterPrototypeDeleteRadioShowsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterPrototypeDeleteRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioPresenter id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterPrototypeDestroyByIdRadioShows**
```objc
-(NSNumber*) radioPresenterPrototypeDestroyByIdRadioShowsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for radioShows.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShows
NSString* _id = @"_id_example"; // RadioPresenter id

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Delete a related item by id for radioShows.
[apiInstance radioPresenterPrototypeDestroyByIdRadioShowsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterPrototypeDestroyByIdRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShows | 
 **_id** | **NSString***| RadioPresenter id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterPrototypeExistsRadioShows**
```objc
-(NSNumber*) radioPresenterPrototypeExistsRadioShowsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSNumber* output, NSError* error)) handler;
```

Check the existence of radioShows relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShows
NSString* _id = @"_id_example"; // RadioPresenter id

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Check the existence of radioShows relation to an item by id.
[apiInstance radioPresenterPrototypeExistsRadioShowsWithFk:fk
              _id:_id
          completionHandler: ^(NSNumber* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterPrototypeExistsRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShows | 
 **_id** | **NSString***| RadioPresenter id | 

### Return type

**NSNumber***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterPrototypeFindByIdRadioShows**
```objc
-(NSNumber*) radioPresenterPrototypeFindByIdRadioShowsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Find a related item by id for radioShows.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShows
NSString* _id = @"_id_example"; // RadioPresenter id

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Find a related item by id for radioShows.
[apiInstance radioPresenterPrototypeFindByIdRadioShowsWithFk:fk
              _id:_id
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterPrototypeFindByIdRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShows | 
 **_id** | **NSString***| RadioPresenter id | 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterPrototypeGetRadioShows**
```objc
-(NSNumber*) radioPresenterPrototypeGetRadioShowsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRadioShow>* output, NSError* error)) handler;
```

Queries radioShows of RadioPresenter.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioPresenter id
NSString* filter = @"filter_example"; //  (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Queries radioShows of RadioPresenter.
[apiInstance radioPresenterPrototypeGetRadioShowsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPRadioShow>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterPrototypeGetRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioPresenter id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPRadioShow>***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterPrototypeLinkRadioShows**
```objc
-(NSNumber*) radioPresenterPrototypeLinkRadioShowsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPRadioPresenterRadioShow*) data
        completionHandler: (void (^)(HRPRadioPresenterRadioShow* output, NSError* error)) handler;
```

Add a related item by id for radioShows.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShows
NSString* _id = @"_id_example"; // RadioPresenter id
HRPRadioPresenterRadioShow* data = [[HRPRadioPresenterRadioShow alloc] init]; //  (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Add a related item by id for radioShows.
[apiInstance radioPresenterPrototypeLinkRadioShowsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPRadioPresenterRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterPrototypeLinkRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShows | 
 **_id** | **NSString***| RadioPresenter id | 
 **data** | [**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow*.md)|  | [optional] 

### Return type

[**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterPrototypeUnlinkRadioShows**
```objc
-(NSNumber*) radioPresenterPrototypeUnlinkRadioShowsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Remove the radioShows relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShows
NSString* _id = @"_id_example"; // RadioPresenter id

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Remove the radioShows relation to an item by id.
[apiInstance radioPresenterPrototypeUnlinkRadioShowsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterPrototypeUnlinkRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShows | 
 **_id** | **NSString***| RadioPresenter id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid**
```objc
-(NSNumber*) radioPresenterPrototypeUpdateAttributesPatchRadioPresentersidWithId: (NSString*) _id
    data: (HRPRadioPresenter*) data
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioPresenter id
HRPRadioPresenter* data = [[HRPRadioPresenter alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance radioPresenterPrototypeUpdateAttributesPatchRadioPresentersidWithId:_id
              data:data
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioPresenter id | 
 **data** | [**HRPRadioPresenter***](HRPRadioPresenter*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterPrototypeUpdateAttributesPutRadioPresentersid**
```objc
-(NSNumber*) radioPresenterPrototypeUpdateAttributesPutRadioPresentersidWithId: (NSString*) _id
    data: (HRPRadioPresenter*) data
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioPresenter id
HRPRadioPresenter* data = [[HRPRadioPresenter alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance radioPresenterPrototypeUpdateAttributesPutRadioPresentersidWithId:_id
              data:data
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterPrototypeUpdateAttributesPutRadioPresentersid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioPresenter id | 
 **data** | [**HRPRadioPresenter***](HRPRadioPresenter*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterPrototypeUpdateByIdRadioShows**
```objc
-(NSNumber*) radioPresenterPrototypeUpdateByIdRadioShowsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Update a related item by id for radioShows.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShows
NSString* _id = @"_id_example"; // RadioPresenter id
HRPRadioShow* data = [[HRPRadioShow alloc] init]; //  (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Update a related item by id for radioShows.
[apiInstance radioPresenterPrototypeUpdateByIdRadioShowsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterPrototypeUpdateByIdRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShows | 
 **_id** | **NSString***| RadioPresenter id | 
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)|  | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterReplaceById**
```objc
-(NSNumber*) radioPresenterReplaceByIdWithId: (NSString*) _id
    data: (HRPRadioPresenter*) data
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPRadioPresenter* data = [[HRPRadioPresenter alloc] init]; // Model instance data (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance radioPresenterReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPRadioPresenter***](HRPRadioPresenter*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterReplaceOrCreate**
```objc
-(NSNumber*) radioPresenterReplaceOrCreateWithData: (HRPRadioPresenter*) data
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioPresenter* data = [[HRPRadioPresenter alloc] init]; // Model instance data (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance radioPresenterReplaceOrCreateWithData:data
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioPresenter***](HRPRadioPresenter*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterUpdateAll**
```objc
-(NSNumber*) radioPresenterUpdateAllWithWhere: (NSString*) where
    data: (HRPRadioPresenter*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRadioPresenter* data = [[HRPRadioPresenter alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance radioPresenterUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRadioPresenter***](HRPRadioPresenter*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterUploadImage**
```objc
-(NSNumber*) radioPresenterUploadImageWithId: (NSString*) _id
    data: (HRPMagentoFileUpload*) data
        completionHandler: (void (^)(HRPMagentoFileUpload* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Radio Presenter Id
HRPMagentoFileUpload* data = [[HRPMagentoFileUpload alloc] init]; // Corresponds to the file you're uploading formatted as an object. (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

[apiInstance radioPresenterUploadImageWithId:_id
              data:data
          completionHandler: ^(HRPMagentoFileUpload* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterUploadImage: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Radio Presenter Id | 
 **data** | [**HRPMagentoFileUpload***](HRPMagentoFileUpload*.md)| Corresponds to the file you&#39;re uploading formatted as an object. | [optional] 

### Return type

[**HRPMagentoFileUpload***](HRPMagentoFileUpload.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterUpsertPatchRadioPresenters**
```objc
-(NSNumber*) radioPresenterUpsertPatchRadioPresentersWithData: (HRPRadioPresenter*) data
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioPresenter* data = [[HRPRadioPresenter alloc] init]; // Model instance data (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance radioPresenterUpsertPatchRadioPresentersWithData:data
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterUpsertPatchRadioPresenters: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioPresenter***](HRPRadioPresenter*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterUpsertPutRadioPresenters**
```objc
-(NSNumber*) radioPresenterUpsertPutRadioPresentersWithData: (HRPRadioPresenter*) data
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioPresenter* data = [[HRPRadioPresenter alloc] init]; // Model instance data (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance radioPresenterUpsertPutRadioPresentersWithData:data
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterUpsertPutRadioPresenters: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioPresenter***](HRPRadioPresenter*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterUpsertWithWhere**
```objc
-(NSNumber*) radioPresenterUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPRadioPresenter*) data
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRadioPresenter* data = [[HRPRadioPresenter alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioPresenterApi*apiInstance = [[HRPRadioPresenterApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance radioPresenterUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterApi->radioPresenterUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRadioPresenter***](HRPRadioPresenter*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

