# HRPAwEventbookingTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**eventTicketId** | **NSNumber*** |  | 
**orderItemId** | **NSNumber*** |  | 
**code** | **NSString*** |  | 
**redeemed** | **NSNumber*** |  | 
**paymentStatus** | **NSNumber*** |  | 
**createdAt** | **NSDate*** |  | [optional] 
**redeemedAt** | **NSDate*** |  | [optional] 
**redeemedByTerminal** | **NSString*** |  | [optional] 
**fee** | **NSString*** |  | 
**isPassOnFee** | **NSNumber*** |  | 
**competitionwinnerId** | **NSNumber*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


