# HRPApp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSString*** |  | 
**internalId** | **NSNumber*** | Id used internally in Harpoon | [optional] 
**vendorId** | **NSNumber*** | Vendor App owner | [optional] 
**name** | **NSString*** | Name of the application | 
**category** | **NSString*** |  | [optional] [default to @"Lifestyle"]
**_description** | **NSString*** |  | [optional] 
**keywords** | **NSString*** |  | [optional] [default to @"buy,claim,offer,events,competitions"]
**varCopyright** | **NSString*** |  | [optional] [default to @"© 2016 The Voucher Link Ltd."]
**realm** | **NSString*** |  | [optional] 
**bundle** | **NSString*** | Bundle Identifier of the application, com.{company name}.{project name} | [optional] [default to @""]
**urlScheme** | **NSString*** | URL Scheme used for deeplinking | [optional] [default to @""]
**brandFollowOffer** | **NSNumber*** | Customer need to follow the Brand to display the Offers outside of the Brand profile page | [optional] [default to @0]
**privacyUrl** | **NSString*** | URL linking to the Privacy page for the app | [optional] [default to @"http://harpoonconnect.com/privacy/"]
**termsOfServiceUrl** | **NSString*** | URL linking to the Terms of Service page for the app | [optional] [default to @"http://harpoonconnect.com/terms/"]
**restrictionAge** | **NSNumber*** | Restrict the use of the app to just the customer with at least the value specified, 0 &#x3D; no limit | [optional] [default to @0.0]
**storeAndroidUrl** | **NSString*** | URL linking to the Google Play page for the app | [optional] [default to @""]
**storeIosUrl** | **NSString*** | URL linking to the iTunes App Store page for the app | [optional] [default to @""]
**typeNewsFeed** | **NSString*** | News Feed source, nativeFeed/rss | [optional] [default to @"nativeFeed"]
**clientSecret** | **NSString*** |  | [optional] 
**grantTypes** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**scopes** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**status** | **NSString*** | Status of the application, production/sandbox/disabled | [optional] [default to @"sandbox"]
**created** | **NSDate*** |  | [optional] 
**modified** | **NSDate*** |  | [optional] 
**styleOfferList** | **NSString*** | Size of the Offer List Item, big/small | [optional] [default to @"big"]
**appConfig** | [**HRPAppConfig***](HRPAppConfig.md) |  | [optional] 
**clientToken** | **NSString*** |  | [optional] 
**multiConfigTitle** | **NSString*** |  | [optional] [default to @"Choose your App:"]
**multiConfig** | [**NSArray&lt;HRPAppConfig&gt;***](HRPAppConfig.md) |  | [optional] 
**appId** | **NSString*** |  | [optional] 
**parentId** | **NSString*** |  | [optional] 
**rssFeeds** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**rssFeedGroups** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**radioStreams** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**customers** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**appConfigs** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**playlists** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**apps** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**parent** | **NSObject*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


