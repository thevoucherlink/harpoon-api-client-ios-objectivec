# HRPDealPaidApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dealPaidCheckout**](HRPDealPaidApi.md#dealpaidcheckout) | **POST** /DealPaids/{id}/checkout | 
[**dealPaidFind**](HRPDealPaidApi.md#dealpaidfind) | **GET** /DealPaids | Find all instances of the model matched by filter from the data source.
[**dealPaidFindById**](HRPDealPaidApi.md#dealpaidfindbyid) | **GET** /DealPaids/{id} | Find a model instance by {{id}} from the data source.
[**dealPaidFindOne**](HRPDealPaidApi.md#dealpaidfindone) | **GET** /DealPaids/findOne | Find first instance of the model matched by filter from the data source.
[**dealPaidRedeem**](HRPDealPaidApi.md#dealpaidredeem) | **POST** /DealPaids/{id}/redeem | 
[**dealPaidReplaceById**](HRPDealPaidApi.md#dealpaidreplacebyid) | **POST** /DealPaids/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**dealPaidReplaceOrCreate**](HRPDealPaidApi.md#dealpaidreplaceorcreate) | **POST** /DealPaids/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**dealPaidUpsertWithWhere**](HRPDealPaidApi.md#dealpaidupsertwithwhere) | **POST** /DealPaids/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**dealPaidVenues**](HRPDealPaidApi.md#dealpaidvenues) | **GET** /DealPaids/{id}/venues | 


# **dealPaidCheckout**
```objc
-(NSNumber*) dealPaidCheckoutWithId: (NSString*) _id
    data: (HRPDealPaidCheckoutData*) data
        completionHandler: (void (^)(HRPDealPaidCheckout* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPDealPaidCheckoutData* data = [[HRPDealPaidCheckoutData alloc] init]; //  (optional)

HRPDealPaidApi*apiInstance = [[HRPDealPaidApi alloc] init];

[apiInstance dealPaidCheckoutWithId:_id
              data:data
          completionHandler: ^(HRPDealPaidCheckout* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealPaidApi->dealPaidCheckout: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPDealPaidCheckoutData***](HRPDealPaidCheckoutData*.md)|  | [optional] 

### Return type

[**HRPDealPaidCheckout***](HRPDealPaidCheckout.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealPaidFind**
```objc
-(NSNumber*) dealPaidFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPDealPaid>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPDealPaidApi*apiInstance = [[HRPDealPaidApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance dealPaidFindWithFilter:filter
          completionHandler: ^(NSArray<HRPDealPaid>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealPaidApi->dealPaidFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPDealPaid>***](HRPDealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealPaidFindById**
```objc
-(NSNumber*) dealPaidFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPDealPaid* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPDealPaidApi*apiInstance = [[HRPDealPaidApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance dealPaidFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPDealPaid* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealPaidApi->dealPaidFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPDealPaid***](HRPDealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealPaidFindOne**
```objc
-(NSNumber*) dealPaidFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPDealPaid* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPDealPaidApi*apiInstance = [[HRPDealPaidApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance dealPaidFindOneWithFilter:filter
          completionHandler: ^(HRPDealPaid* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealPaidApi->dealPaidFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPDealPaid***](HRPDealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealPaidRedeem**
```objc
-(NSNumber*) dealPaidRedeemWithId: (NSString*) _id
    data: (HRPDealPaidRedeemData*) data
        completionHandler: (void (^)(HRPDealPaidPurchase* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPDealPaidRedeemData* data = [[HRPDealPaidRedeemData alloc] init]; //  (optional)

HRPDealPaidApi*apiInstance = [[HRPDealPaidApi alloc] init];

[apiInstance dealPaidRedeemWithId:_id
              data:data
          completionHandler: ^(HRPDealPaidPurchase* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealPaidApi->dealPaidRedeem: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPDealPaidRedeemData***](HRPDealPaidRedeemData*.md)|  | [optional] 

### Return type

[**HRPDealPaidPurchase***](HRPDealPaidPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealPaidReplaceById**
```objc
-(NSNumber*) dealPaidReplaceByIdWithId: (NSString*) _id
    data: (HRPDealPaid*) data
        completionHandler: (void (^)(HRPDealPaid* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPDealPaid* data = [[HRPDealPaid alloc] init]; // Model instance data (optional)

HRPDealPaidApi*apiInstance = [[HRPDealPaidApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance dealPaidReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPDealPaid* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealPaidApi->dealPaidReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPDealPaid***](HRPDealPaid*.md)| Model instance data | [optional] 

### Return type

[**HRPDealPaid***](HRPDealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealPaidReplaceOrCreate**
```objc
-(NSNumber*) dealPaidReplaceOrCreateWithData: (HRPDealPaid*) data
        completionHandler: (void (^)(HRPDealPaid* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPDealPaid* data = [[HRPDealPaid alloc] init]; // Model instance data (optional)

HRPDealPaidApi*apiInstance = [[HRPDealPaidApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance dealPaidReplaceOrCreateWithData:data
          completionHandler: ^(HRPDealPaid* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealPaidApi->dealPaidReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPDealPaid***](HRPDealPaid*.md)| Model instance data | [optional] 

### Return type

[**HRPDealPaid***](HRPDealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealPaidUpsertWithWhere**
```objc
-(NSNumber*) dealPaidUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPDealPaid*) data
        completionHandler: (void (^)(HRPDealPaid* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPDealPaid* data = [[HRPDealPaid alloc] init]; // An object of model property name/value pairs (optional)

HRPDealPaidApi*apiInstance = [[HRPDealPaidApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance dealPaidUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPDealPaid* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealPaidApi->dealPaidUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPDealPaid***](HRPDealPaid*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPDealPaid***](HRPDealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealPaidVenues**
```objc
-(NSNumber*) dealPaidVenuesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPVenue>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPDealPaidApi*apiInstance = [[HRPDealPaidApi alloc] init];

[apiInstance dealPaidVenuesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPVenue>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealPaidApi->dealPaidVenues: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPVenue>***](HRPVenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

