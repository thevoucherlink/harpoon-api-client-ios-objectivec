# HRPRssFeed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **NSString*** |  | 
**subCategory** | **NSString*** |  | [optional] 
**url** | **NSString*** |  | 
**name** | **NSString*** |  | [optional] 
**sortOrder** | **NSNumber*** |  | [optional] [default to @0.0]
**styleCss** | **NSString*** |  | [optional] 
**noAds** | **NSNumber*** |  | [optional] [default to @0]
**adMRectVisible** | **NSNumber*** |  | [optional] [default to @0]
**adMRectPosition** | **NSNumber*** |  | [optional] [default to @0.0]
**_id** | **NSNumber*** |  | [optional] 
**appId** | **NSString*** |  | [optional] 
**rssFeedGroupId** | **NSNumber*** |  | [optional] 
**rssFeedGroup** | **NSObject*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


