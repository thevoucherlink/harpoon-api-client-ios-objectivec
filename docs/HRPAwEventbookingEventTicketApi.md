# HRPAwEventbookingEventTicketApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingEventTicketCount**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketcount) | **GET** /AwEventbookingEventTickets/count | Count instances of the model matched by where from the data source.
[**awEventbookingEventTicketCreate**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketcreate) | **POST** /AwEventbookingEventTickets | Create a new instance of the model and persist it into the data source.
[**awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketcreatechangestreamgetaweventbookingeventticketschangestream) | **GET** /AwEventbookingEventTickets/change-stream | Create a change stream.
[**awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketcreatechangestreampostaweventbookingeventticketschangestream) | **POST** /AwEventbookingEventTickets/change-stream | Create a change stream.
[**awEventbookingEventTicketDeleteById**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketdeletebyid) | **DELETE** /AwEventbookingEventTickets/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketexistsgetaweventbookingeventticketsidexists) | **GET** /AwEventbookingEventTickets/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketexistsheadaweventbookingeventticketsid) | **HEAD** /AwEventbookingEventTickets/{id} | Check whether a model instance exists in the data source.
[**awEventbookingEventTicketFind**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketfind) | **GET** /AwEventbookingEventTickets | Find all instances of the model matched by filter from the data source.
[**awEventbookingEventTicketFindById**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketfindbyid) | **GET** /AwEventbookingEventTickets/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingEventTicketFindOne**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketfindone) | **GET** /AwEventbookingEventTickets/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingEventTicketPrototypeCountAttributes**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypecountattributes) | **GET** /AwEventbookingEventTickets/{id}/attributes/count | Counts attributes of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeCountAwEventbookingTicket**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypecountaweventbookingticket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket/count | Counts awEventbookingTicket of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeCountTickets**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypecounttickets) | **GET** /AwEventbookingEventTickets/{id}/tickets/count | Counts tickets of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeCreateAttributes**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypecreateattributes) | **POST** /AwEventbookingEventTickets/{id}/attributes | Creates a new instance in attributes of this model.
[**awEventbookingEventTicketPrototypeCreateAwEventbookingTicket**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypecreateaweventbookingticket) | **POST** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Creates a new instance in awEventbookingTicket of this model.
[**awEventbookingEventTicketPrototypeCreateTickets**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypecreatetickets) | **POST** /AwEventbookingEventTickets/{id}/tickets | Creates a new instance in tickets of this model.
[**awEventbookingEventTicketPrototypeDeleteAttributes**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypedeleteattributes) | **DELETE** /AwEventbookingEventTickets/{id}/attributes | Deletes all attributes of this model.
[**awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypedeleteaweventbookingticket) | **DELETE** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Deletes all awEventbookingTicket of this model.
[**awEventbookingEventTicketPrototypeDeleteTickets**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypedeletetickets) | **DELETE** /AwEventbookingEventTickets/{id}/tickets | Deletes all tickets of this model.
[**awEventbookingEventTicketPrototypeDestroyByIdAttributes**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypedestroybyidattributes) | **DELETE** /AwEventbookingEventTickets/{id}/attributes/{fk} | Delete a related item by id for attributes.
[**awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypedestroybyidaweventbookingticket) | **DELETE** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Delete a related item by id for awEventbookingTicket.
[**awEventbookingEventTicketPrototypeDestroyByIdTickets**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypedestroybyidtickets) | **DELETE** /AwEventbookingEventTickets/{id}/tickets/{fk} | Delete a related item by id for tickets.
[**awEventbookingEventTicketPrototypeFindByIdAttributes**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypefindbyidattributes) | **GET** /AwEventbookingEventTickets/{id}/attributes/{fk} | Find a related item by id for attributes.
[**awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypefindbyidaweventbookingticket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Find a related item by id for awEventbookingTicket.
[**awEventbookingEventTicketPrototypeFindByIdTickets**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypefindbyidtickets) | **GET** /AwEventbookingEventTickets/{id}/tickets/{fk} | Find a related item by id for tickets.
[**awEventbookingEventTicketPrototypeGetAttributes**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypegetattributes) | **GET** /AwEventbookingEventTickets/{id}/attributes | Queries attributes of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeGetAwEventbookingTicket**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypegetaweventbookingticket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Queries awEventbookingTicket of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeGetTickets**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypegettickets) | **GET** /AwEventbookingEventTickets/{id}/tickets | Queries tickets of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypeupdateattributespatchaweventbookingeventticketsid) | **PATCH** /AwEventbookingEventTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypeupdateattributesputaweventbookingeventticketsid) | **PUT** /AwEventbookingEventTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventTicketPrototypeUpdateByIdAttributes**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypeupdatebyidattributes) | **PUT** /AwEventbookingEventTickets/{id}/attributes/{fk} | Update a related item by id for attributes.
[**awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypeupdatebyidaweventbookingticket) | **PUT** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Update a related item by id for awEventbookingTicket.
[**awEventbookingEventTicketPrototypeUpdateByIdTickets**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketprototypeupdatebyidtickets) | **PUT** /AwEventbookingEventTickets/{id}/tickets/{fk} | Update a related item by id for tickets.
[**awEventbookingEventTicketReplaceById**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketreplacebyid) | **POST** /AwEventbookingEventTickets/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingEventTicketReplaceOrCreate**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketreplaceorcreate) | **POST** /AwEventbookingEventTickets/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingEventTicketUpdateAll**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketupdateall) | **POST** /AwEventbookingEventTickets/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketupsertpatchaweventbookingeventtickets) | **PATCH** /AwEventbookingEventTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventTicketUpsertPutAwEventbookingEventTickets**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketupsertputaweventbookingeventtickets) | **PUT** /AwEventbookingEventTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventTicketUpsertWithWhere**](HRPAwEventbookingEventTicketApi.md#aweventbookingeventticketupsertwithwhere) | **POST** /AwEventbookingEventTickets/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awEventbookingEventTicketCount**
```objc
-(NSNumber*) awEventbookingEventTicketCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance awEventbookingEventTicketCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketCreate**
```objc
-(NSNumber*) awEventbookingEventTicketCreateWithData: (HRPAwEventbookingEventTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingEventTicket* data = [[HRPAwEventbookingEventTicket alloc] init]; // Model instance data (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance awEventbookingEventTicketCreateWithData:data
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream**
```objc
-(NSNumber*) awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Create a change stream.
[apiInstance awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream**
```objc
-(NSNumber*) awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Create a change stream.
[apiInstance awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketDeleteById**
```objc
-(NSNumber*) awEventbookingEventTicketDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance awEventbookingEventTicketDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists**
```objc
-(NSNumber*) awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid**
```objc
-(NSNumber*) awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketFind**
```objc
-(NSNumber*) awEventbookingEventTicketFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwEventbookingEventTicket>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance awEventbookingEventTicketFindWithFilter:filter
          completionHandler: ^(NSArray<HRPAwEventbookingEventTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPAwEventbookingEventTicket>***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketFindById**
```objc
-(NSNumber*) awEventbookingEventTicketFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance awEventbookingEventTicketFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketFindOne**
```objc
-(NSNumber*) awEventbookingEventTicketFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance awEventbookingEventTicketFindOneWithFilter:filter
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeCountAttributes**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeCountAttributesWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts attributes of AwEventbookingEventTicket.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Counts attributes of AwEventbookingEventTicket.
[apiInstance awEventbookingEventTicketPrototypeCountAttributesWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeCountAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeCountAwEventbookingTicket**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeCountAwEventbookingTicketWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts awEventbookingTicket of AwEventbookingEventTicket.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Counts awEventbookingTicket of AwEventbookingEventTicket.
[apiInstance awEventbookingEventTicketPrototypeCountAwEventbookingTicketWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeCountAwEventbookingTicket: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeCountTickets**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeCountTicketsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts tickets of AwEventbookingEventTicket.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Counts tickets of AwEventbookingEventTicket.
[apiInstance awEventbookingEventTicketPrototypeCountTicketsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeCountTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeCreateAttributes**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeCreateAttributesWithId: (NSString*) _id
    data: (HRPAwEventbookingEventTicketAttribute*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicketAttribute* output, NSError* error)) handler;
```

Creates a new instance in attributes of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
HRPAwEventbookingEventTicketAttribute* data = [[HRPAwEventbookingEventTicketAttribute alloc] init]; //  (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Creates a new instance in attributes of this model.
[apiInstance awEventbookingEventTicketPrototypeCreateAttributesWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEventTicketAttribute* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeCreateAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **data** | [**HRPAwEventbookingEventTicketAttribute***](HRPAwEventbookingEventTicketAttribute*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingEventTicketAttribute***](HRPAwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeCreateAwEventbookingTicket**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeCreateAwEventbookingTicketWithId: (NSString*) _id
    data: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Creates a new instance in awEventbookingTicket of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; //  (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Creates a new instance in awEventbookingTicket of this model.
[apiInstance awEventbookingEventTicketPrototypeCreateAwEventbookingTicketWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeCreateAwEventbookingTicket: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeCreateTickets**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeCreateTicketsWithId: (NSString*) _id
    data: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Creates a new instance in tickets of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; //  (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Creates a new instance in tickets of this model.
[apiInstance awEventbookingEventTicketPrototypeCreateTicketsWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeCreateTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeDeleteAttributes**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeDeleteAttributesWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all attributes of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Deletes all attributes of this model.
[apiInstance awEventbookingEventTicketPrototypeDeleteAttributesWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeDeleteAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeDeleteAwEventbookingTicketWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all awEventbookingTicket of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Deletes all awEventbookingTicket of this model.
[apiInstance awEventbookingEventTicketPrototypeDeleteAwEventbookingTicketWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeDeleteTickets**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeDeleteTicketsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all tickets of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Deletes all tickets of this model.
[apiInstance awEventbookingEventTicketPrototypeDeleteTicketsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeDeleteTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeDestroyByIdAttributes**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeDestroyByIdAttributesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for attributes.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for attributes
NSString* _id = @"_id_example"; // AwEventbookingEventTicket id

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Delete a related item by id for attributes.
[apiInstance awEventbookingEventTicketPrototypeDestroyByIdAttributesWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeDestroyByIdAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for attributes | 
 **_id** | **NSString***| AwEventbookingEventTicket id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicketWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for awEventbookingTicket.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for awEventbookingTicket
NSString* _id = @"_id_example"; // AwEventbookingEventTicket id

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Delete a related item by id for awEventbookingTicket.
[apiInstance awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicketWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for awEventbookingTicket | 
 **_id** | **NSString***| AwEventbookingEventTicket id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeDestroyByIdTickets**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeDestroyByIdTicketsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for tickets.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for tickets
NSString* _id = @"_id_example"; // AwEventbookingEventTicket id

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Delete a related item by id for tickets.
[apiInstance awEventbookingEventTicketPrototypeDestroyByIdTicketsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeDestroyByIdTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for tickets | 
 **_id** | **NSString***| AwEventbookingEventTicket id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeFindByIdAttributes**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeFindByIdAttributesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPAwEventbookingEventTicketAttribute* output, NSError* error)) handler;
```

Find a related item by id for attributes.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for attributes
NSString* _id = @"_id_example"; // AwEventbookingEventTicket id

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Find a related item by id for attributes.
[apiInstance awEventbookingEventTicketPrototypeFindByIdAttributesWithFk:fk
              _id:_id
          completionHandler: ^(HRPAwEventbookingEventTicketAttribute* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeFindByIdAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for attributes | 
 **_id** | **NSString***| AwEventbookingEventTicket id | 

### Return type

[**HRPAwEventbookingEventTicketAttribute***](HRPAwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicketWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Find a related item by id for awEventbookingTicket.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for awEventbookingTicket
NSString* _id = @"_id_example"; // AwEventbookingEventTicket id

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Find a related item by id for awEventbookingTicket.
[apiInstance awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicketWithFk:fk
              _id:_id
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for awEventbookingTicket | 
 **_id** | **NSString***| AwEventbookingEventTicket id | 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeFindByIdTickets**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeFindByIdTicketsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Find a related item by id for tickets.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for tickets
NSString* _id = @"_id_example"; // AwEventbookingEventTicket id

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Find a related item by id for tickets.
[apiInstance awEventbookingEventTicketPrototypeFindByIdTicketsWithFk:fk
              _id:_id
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeFindByIdTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for tickets | 
 **_id** | **NSString***| AwEventbookingEventTicket id | 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeGetAttributes**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeGetAttributesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwEventbookingEventTicketAttribute>* output, NSError* error)) handler;
```

Queries attributes of AwEventbookingEventTicket.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
NSString* filter = @"filter_example"; //  (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Queries attributes of AwEventbookingEventTicket.
[apiInstance awEventbookingEventTicketPrototypeGetAttributesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPAwEventbookingEventTicketAttribute>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeGetAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPAwEventbookingEventTicketAttribute>***](HRPAwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeGetAwEventbookingTicket**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeGetAwEventbookingTicketWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwEventbookingTicket>* output, NSError* error)) handler;
```

Queries awEventbookingTicket of AwEventbookingEventTicket.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
NSString* filter = @"filter_example"; //  (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Queries awEventbookingTicket of AwEventbookingEventTicket.
[apiInstance awEventbookingEventTicketPrototypeGetAwEventbookingTicketWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPAwEventbookingTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeGetAwEventbookingTicket: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPAwEventbookingTicket>***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeGetTickets**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeGetTicketsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwEventbookingTicket>* output, NSError* error)) handler;
```

Queries tickets of AwEventbookingEventTicket.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
NSString* filter = @"filter_example"; //  (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Queries tickets of AwEventbookingEventTicket.
[apiInstance awEventbookingEventTicketPrototypeGetTicketsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPAwEventbookingTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeGetTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPAwEventbookingTicket>***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsidWithId: (NSString*) _id
    data: (HRPAwEventbookingEventTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
HRPAwEventbookingEventTicket* data = [[HRPAwEventbookingEventTicket alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsidWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **data** | [**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsidWithId: (NSString*) _id
    data: (HRPAwEventbookingEventTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
HRPAwEventbookingEventTicket* data = [[HRPAwEventbookingEventTicket alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsidWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **data** | [**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeUpdateByIdAttributes**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeUpdateByIdAttributesWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPAwEventbookingEventTicketAttribute*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicketAttribute* output, NSError* error)) handler;
```

Update a related item by id for attributes.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for attributes
NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
HRPAwEventbookingEventTicketAttribute* data = [[HRPAwEventbookingEventTicketAttribute alloc] init]; //  (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Update a related item by id for attributes.
[apiInstance awEventbookingEventTicketPrototypeUpdateByIdAttributesWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEventTicketAttribute* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeUpdateByIdAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for attributes | 
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **data** | [**HRPAwEventbookingEventTicketAttribute***](HRPAwEventbookingEventTicketAttribute*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingEventTicketAttribute***](HRPAwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicketWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Update a related item by id for awEventbookingTicket.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for awEventbookingTicket
NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; //  (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Update a related item by id for awEventbookingTicket.
[apiInstance awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicketWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for awEventbookingTicket | 
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketPrototypeUpdateByIdTickets**
```objc
-(NSNumber*) awEventbookingEventTicketPrototypeUpdateByIdTicketsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Update a related item by id for tickets.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for tickets
NSString* _id = @"_id_example"; // AwEventbookingEventTicket id
HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; //  (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Update a related item by id for tickets.
[apiInstance awEventbookingEventTicketPrototypeUpdateByIdTicketsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeUpdateByIdTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for tickets | 
 **_id** | **NSString***| AwEventbookingEventTicket id | 
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketReplaceById**
```objc
-(NSNumber*) awEventbookingEventTicketReplaceByIdWithId: (NSString*) _id
    data: (HRPAwEventbookingEventTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPAwEventbookingEventTicket* data = [[HRPAwEventbookingEventTicket alloc] init]; // Model instance data (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance awEventbookingEventTicketReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketReplaceOrCreate**
```objc
-(NSNumber*) awEventbookingEventTicketReplaceOrCreateWithData: (HRPAwEventbookingEventTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingEventTicket* data = [[HRPAwEventbookingEventTicket alloc] init]; // Model instance data (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance awEventbookingEventTicketReplaceOrCreateWithData:data
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketUpdateAll**
```objc
-(NSNumber*) awEventbookingEventTicketUpdateAllWithWhere: (NSString*) where
    data: (HRPAwEventbookingEventTicket*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwEventbookingEventTicket* data = [[HRPAwEventbookingEventTicket alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance awEventbookingEventTicketUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets**
```objc
-(NSNumber*) awEventbookingEventTicketUpsertPatchAwEventbookingEventTicketsWithData: (HRPAwEventbookingEventTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingEventTicket* data = [[HRPAwEventbookingEventTicket alloc] init]; // Model instance data (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awEventbookingEventTicketUpsertPatchAwEventbookingEventTicketsWithData:data
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketUpsertPutAwEventbookingEventTickets**
```objc
-(NSNumber*) awEventbookingEventTicketUpsertPutAwEventbookingEventTicketsWithData: (HRPAwEventbookingEventTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingEventTicket* data = [[HRPAwEventbookingEventTicket alloc] init]; // Model instance data (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awEventbookingEventTicketUpsertPutAwEventbookingEventTicketsWithData:data
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketUpsertPutAwEventbookingEventTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventTicketUpsertWithWhere**
```objc
-(NSNumber*) awEventbookingEventTicketUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPAwEventbookingEventTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwEventbookingEventTicket* data = [[HRPAwEventbookingEventTicket alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingEventTicketApi*apiInstance = [[HRPAwEventbookingEventTicketApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance awEventbookingEventTicketUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventTicketApi->awEventbookingEventTicketUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

