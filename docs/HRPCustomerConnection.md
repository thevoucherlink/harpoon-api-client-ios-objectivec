# HRPCustomerConnection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**facebook** | [**HRPCustomerConnectionFacebook***](HRPCustomerConnectionFacebook.md) |  | [optional] 
**twitter** | [**HRPCustomerConnectionTwitter***](HRPCustomerConnectionTwitter.md) |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


