# HRPCustomerRefreshTokenCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refreshToken** | **NSString*** | Refresh Token can be found in a logged in and authorized Customer associated to an AuthToken under the property refreshToken | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


