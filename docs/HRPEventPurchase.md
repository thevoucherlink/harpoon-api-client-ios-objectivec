# HRPEventPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **NSNumber*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**event** | [**HRPEvent***](HRPEvent.md) |  | [optional] 
**redemptionType** | **NSString*** |  | [optional] 
**unlockTime** | **NSNumber*** |  | [optional] [default to @15.0]
**code** | **NSString*** |  | [optional] 
**qrcode** | **NSString*** |  | [optional] 
**isAvailable** | **NSNumber*** |  | [optional] [default to @0]
**status** | **NSString*** |  | [optional] [default to @"unknown"]
**createdAt** | **NSDate*** |  | [optional] 
**expiredAt** | **NSDate*** |  | [optional] 
**redeemedAt** | **NSDate*** |  | [optional] 
**redeemedByTerminal** | **NSString*** |  | [optional] 
**basePrice** | **NSNumber*** |  | [optional] [default to @0.0]
**ticketPrice** | **NSNumber*** |  | [optional] [default to @9.0]
**currency** | **NSString*** |  | [optional] [default to @"EUR"]
**attendee** | [**HRPCustomer***](HRPCustomer.md) |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


