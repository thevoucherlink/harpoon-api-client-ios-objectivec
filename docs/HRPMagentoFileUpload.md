# HRPMagentoFileUpload

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **NSString*** | File contents encoded as base64 | 
**contentType** | **NSString*** | The file&#39;s encode type e.g application/json | [optional] 
**name** | **NSString*** | the file name including the extension | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


