# HRPCustomerLoginCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **NSString*** | Customer Email | 
**password** | **NSString*** | Customer Password | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


