# HRPCompetitionAnswerData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**competitionAnswer** | **NSString*** | Competition Answer | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


