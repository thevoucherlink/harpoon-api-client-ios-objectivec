# HRPAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**streetAddress** | **NSString*** | Main street line for the address (first line) | [optional] 
**streetAddressComp** | **NSString*** | Line to complete the address (second line) | [optional] 
**city** | **NSString*** |  | [optional] 
**region** | **NSString*** | Region, County, Province or State | [optional] 
**countryCode** | **NSString*** | Country ISO code (must be max of 2 capital letter) | [optional] [default to @"IE"]
**postcode** | **NSString*** |  | [optional] 
**attnFirstName** | **NSString*** | Postal Attn {person first name} | [optional] 
**attnLastName** | **NSString*** | Postal Attn {person last name} | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


