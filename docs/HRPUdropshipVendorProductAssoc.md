# HRPUdropshipVendorProductAssoc

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isUdmulti** | **NSNumber*** |  | [optional] 
**_id** | **NSNumber*** |  | 
**isAttribute** | **NSNumber*** |  | 
**productId** | **NSNumber*** |  | 
**vendorId** | **NSNumber*** |  | 
**udropshipVendor** | **NSObject*** |  | [optional] 
**catalogProduct** | **NSObject*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


