# HRPUdropshipVendorPartner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **NSString*** |  | 
**invitedEmail** | **NSString*** |  | 
**invitedAt** | **NSDate*** |  | 
**acceptedAt** | **NSDate*** |  | 
**createdAt** | **NSDate*** |  | 
**updatedAt** | **NSDate*** |  | 
**configList** | **NSNumber*** |  | 
**configFeed** | **NSNumber*** |  | 
**configNeedFollow** | **NSNumber*** |  | 
**configAcceptEvent** | **NSNumber*** |  | 
**configAcceptCoupon** | **NSNumber*** |  | 
**configAcceptDealsimple** | **NSNumber*** |  | 
**configAcceptDealgroup** | **NSNumber*** |  | 
**configAcceptNotificationpush** | **NSNumber*** |  | 
**configAcceptNotificationbeacon** | **NSNumber*** |  | 
**isOwner** | **NSNumber*** |  | 
**vendorId** | **NSNumber*** |  | 
**partnerId** | **NSNumber*** |  | 
**_id** | **NSNumber*** |  | 
**udropshipVendor** | **NSObject*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


