# HRPFormSection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **NSString*** |  | [optional] 
**rows** | [**NSArray&lt;HRPFormRow&gt;***](HRPFormRow.md) |  | 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


