# HRPAnalyticRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requests** | **NSArray&lt;NSObject*&gt;*** |  | 
**scope** | **NSString*** |  | [optional] [default to @"inapp"]
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


