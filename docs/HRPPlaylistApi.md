# HRPPlaylistApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playlistCount**](HRPPlaylistApi.md#playlistcount) | **GET** /Playlists/count | Count instances of the model matched by where from the data source.
[**playlistCreate**](HRPPlaylistApi.md#playlistcreate) | **POST** /Playlists | Create a new instance of the model and persist it into the data source.
[**playlistCreateChangeStreamGetPlaylistsChangeStream**](HRPPlaylistApi.md#playlistcreatechangestreamgetplaylistschangestream) | **GET** /Playlists/change-stream | Create a change stream.
[**playlistCreateChangeStreamPostPlaylistsChangeStream**](HRPPlaylistApi.md#playlistcreatechangestreampostplaylistschangestream) | **POST** /Playlists/change-stream | Create a change stream.
[**playlistDeleteById**](HRPPlaylistApi.md#playlistdeletebyid) | **DELETE** /Playlists/{id} | Delete a model instance by {{id}} from the data source.
[**playlistExistsGetPlaylistsidExists**](HRPPlaylistApi.md#playlistexistsgetplaylistsidexists) | **GET** /Playlists/{id}/exists | Check whether a model instance exists in the data source.
[**playlistExistsHeadPlaylistsid**](HRPPlaylistApi.md#playlistexistsheadplaylistsid) | **HEAD** /Playlists/{id} | Check whether a model instance exists in the data source.
[**playlistFind**](HRPPlaylistApi.md#playlistfind) | **GET** /Playlists | Find all instances of the model matched by filter from the data source.
[**playlistFindById**](HRPPlaylistApi.md#playlistfindbyid) | **GET** /Playlists/{id} | Find a model instance by {{id}} from the data source.
[**playlistFindOne**](HRPPlaylistApi.md#playlistfindone) | **GET** /Playlists/findOne | Find first instance of the model matched by filter from the data source.
[**playlistPrototypeCountPlaylistItems**](HRPPlaylistApi.md#playlistprototypecountplaylistitems) | **GET** /Playlists/{id}/playlistItems/count | Counts playlistItems of Playlist.
[**playlistPrototypeCreatePlaylistItems**](HRPPlaylistApi.md#playlistprototypecreateplaylistitems) | **POST** /Playlists/{id}/playlistItems | Creates a new instance in playlistItems of this model.
[**playlistPrototypeDeletePlaylistItems**](HRPPlaylistApi.md#playlistprototypedeleteplaylistitems) | **DELETE** /Playlists/{id}/playlistItems | Deletes all playlistItems of this model.
[**playlistPrototypeDestroyByIdPlaylistItems**](HRPPlaylistApi.md#playlistprototypedestroybyidplaylistitems) | **DELETE** /Playlists/{id}/playlistItems/{fk} | Delete a related item by id for playlistItems.
[**playlistPrototypeFindByIdPlaylistItems**](HRPPlaylistApi.md#playlistprototypefindbyidplaylistitems) | **GET** /Playlists/{id}/playlistItems/{fk} | Find a related item by id for playlistItems.
[**playlistPrototypeGetPlaylistItems**](HRPPlaylistApi.md#playlistprototypegetplaylistitems) | **GET** /Playlists/{id}/playlistItems | Queries playlistItems of Playlist.
[**playlistPrototypeUpdateAttributesPatchPlaylistsid**](HRPPlaylistApi.md#playlistprototypeupdateattributespatchplaylistsid) | **PATCH** /Playlists/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistPrototypeUpdateAttributesPutPlaylistsid**](HRPPlaylistApi.md#playlistprototypeupdateattributesputplaylistsid) | **PUT** /Playlists/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistPrototypeUpdateByIdPlaylistItems**](HRPPlaylistApi.md#playlistprototypeupdatebyidplaylistitems) | **PUT** /Playlists/{id}/playlistItems/{fk} | Update a related item by id for playlistItems.
[**playlistReplaceById**](HRPPlaylistApi.md#playlistreplacebyid) | **POST** /Playlists/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playlistReplaceOrCreate**](HRPPlaylistApi.md#playlistreplaceorcreate) | **POST** /Playlists/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playlistUpdateAll**](HRPPlaylistApi.md#playlistupdateall) | **POST** /Playlists/update | Update instances of the model matched by {{where}} from the data source.
[**playlistUpsertPatchPlaylists**](HRPPlaylistApi.md#playlistupsertpatchplaylists) | **PATCH** /Playlists | Patch an existing model instance or insert a new one into the data source.
[**playlistUpsertPutPlaylists**](HRPPlaylistApi.md#playlistupsertputplaylists) | **PUT** /Playlists | Patch an existing model instance or insert a new one into the data source.
[**playlistUpsertWithWhere**](HRPPlaylistApi.md#playlistupsertwithwhere) | **POST** /Playlists/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **playlistCount**
```objc
-(NSNumber*) playlistCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance playlistCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistCreate**
```objc
-(NSNumber*) playlistCreateWithData: (HRPPlaylist*) data
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlaylist* data = [[HRPPlaylist alloc] init]; // Model instance data (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance playlistCreateWithData:data
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlaylist***](HRPPlaylist*.md)| Model instance data | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistCreateChangeStreamGetPlaylistsChangeStream**
```objc
-(NSNumber*) playlistCreateChangeStreamGetPlaylistsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Create a change stream.
[apiInstance playlistCreateChangeStreamGetPlaylistsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistCreateChangeStreamGetPlaylistsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistCreateChangeStreamPostPlaylistsChangeStream**
```objc
-(NSNumber*) playlistCreateChangeStreamPostPlaylistsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Create a change stream.
[apiInstance playlistCreateChangeStreamPostPlaylistsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistCreateChangeStreamPostPlaylistsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistDeleteById**
```objc
-(NSNumber*) playlistDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance playlistDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistExistsGetPlaylistsidExists**
```objc
-(NSNumber*) playlistExistsGetPlaylistsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance playlistExistsGetPlaylistsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistExistsGetPlaylistsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistExistsHeadPlaylistsid**
```objc
-(NSNumber*) playlistExistsHeadPlaylistsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance playlistExistsHeadPlaylistsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistExistsHeadPlaylistsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistFind**
```objc
-(NSNumber*) playlistFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPPlaylist>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance playlistFindWithFilter:filter
          completionHandler: ^(NSArray<HRPPlaylist>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPPlaylist>***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistFindById**
```objc
-(NSNumber*) playlistFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance playlistFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistFindOne**
```objc
-(NSNumber*) playlistFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance playlistFindOneWithFilter:filter
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistPrototypeCountPlaylistItems**
```objc
-(NSNumber*) playlistPrototypeCountPlaylistItemsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts playlistItems of Playlist.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Playlist id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Counts playlistItems of Playlist.
[apiInstance playlistPrototypeCountPlaylistItemsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistPrototypeCountPlaylistItems: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Playlist id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistPrototypeCreatePlaylistItems**
```objc
-(NSNumber*) playlistPrototypeCreatePlaylistItemsWithId: (NSString*) _id
    data: (HRPPlaylistItem*) data
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Creates a new instance in playlistItems of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Playlist id
HRPPlaylistItem* data = [[HRPPlaylistItem alloc] init]; //  (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Creates a new instance in playlistItems of this model.
[apiInstance playlistPrototypeCreatePlaylistItemsWithId:_id
              data:data
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistPrototypeCreatePlaylistItems: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Playlist id | 
 **data** | [**HRPPlaylistItem***](HRPPlaylistItem*.md)|  | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistPrototypeDeletePlaylistItems**
```objc
-(NSNumber*) playlistPrototypeDeletePlaylistItemsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all playlistItems of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Playlist id

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Deletes all playlistItems of this model.
[apiInstance playlistPrototypeDeletePlaylistItemsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistPrototypeDeletePlaylistItems: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Playlist id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistPrototypeDestroyByIdPlaylistItems**
```objc
-(NSNumber*) playlistPrototypeDestroyByIdPlaylistItemsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for playlistItems.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for playlistItems
NSString* _id = @"_id_example"; // Playlist id

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Delete a related item by id for playlistItems.
[apiInstance playlistPrototypeDestroyByIdPlaylistItemsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistPrototypeDestroyByIdPlaylistItems: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for playlistItems | 
 **_id** | **NSString***| Playlist id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistPrototypeFindByIdPlaylistItems**
```objc
-(NSNumber*) playlistPrototypeFindByIdPlaylistItemsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Find a related item by id for playlistItems.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for playlistItems
NSString* _id = @"_id_example"; // Playlist id

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Find a related item by id for playlistItems.
[apiInstance playlistPrototypeFindByIdPlaylistItemsWithFk:fk
              _id:_id
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistPrototypeFindByIdPlaylistItems: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for playlistItems | 
 **_id** | **NSString***| Playlist id | 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistPrototypeGetPlaylistItems**
```objc
-(NSNumber*) playlistPrototypeGetPlaylistItemsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPPlaylistItem>* output, NSError* error)) handler;
```

Queries playlistItems of Playlist.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Playlist id
NSString* filter = @"filter_example"; //  (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Queries playlistItems of Playlist.
[apiInstance playlistPrototypeGetPlaylistItemsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPPlaylistItem>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistPrototypeGetPlaylistItems: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Playlist id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPPlaylistItem>***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistPrototypeUpdateAttributesPatchPlaylistsid**
```objc
-(NSNumber*) playlistPrototypeUpdateAttributesPatchPlaylistsidWithId: (NSString*) _id
    data: (HRPPlaylist*) data
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Playlist id
HRPPlaylist* data = [[HRPPlaylist alloc] init]; // An object of model property name/value pairs (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance playlistPrototypeUpdateAttributesPatchPlaylistsidWithId:_id
              data:data
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistPrototypeUpdateAttributesPatchPlaylistsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Playlist id | 
 **data** | [**HRPPlaylist***](HRPPlaylist*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistPrototypeUpdateAttributesPutPlaylistsid**
```objc
-(NSNumber*) playlistPrototypeUpdateAttributesPutPlaylistsidWithId: (NSString*) _id
    data: (HRPPlaylist*) data
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Playlist id
HRPPlaylist* data = [[HRPPlaylist alloc] init]; // An object of model property name/value pairs (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance playlistPrototypeUpdateAttributesPutPlaylistsidWithId:_id
              data:data
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistPrototypeUpdateAttributesPutPlaylistsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Playlist id | 
 **data** | [**HRPPlaylist***](HRPPlaylist*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistPrototypeUpdateByIdPlaylistItems**
```objc
-(NSNumber*) playlistPrototypeUpdateByIdPlaylistItemsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPPlaylistItem*) data
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Update a related item by id for playlistItems.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for playlistItems
NSString* _id = @"_id_example"; // Playlist id
HRPPlaylistItem* data = [[HRPPlaylistItem alloc] init]; //  (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Update a related item by id for playlistItems.
[apiInstance playlistPrototypeUpdateByIdPlaylistItemsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistPrototypeUpdateByIdPlaylistItems: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for playlistItems | 
 **_id** | **NSString***| Playlist id | 
 **data** | [**HRPPlaylistItem***](HRPPlaylistItem*.md)|  | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistReplaceById**
```objc
-(NSNumber*) playlistReplaceByIdWithId: (NSString*) _id
    data: (HRPPlaylist*) data
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPPlaylist* data = [[HRPPlaylist alloc] init]; // Model instance data (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance playlistReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPPlaylist***](HRPPlaylist*.md)| Model instance data | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistReplaceOrCreate**
```objc
-(NSNumber*) playlistReplaceOrCreateWithData: (HRPPlaylist*) data
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlaylist* data = [[HRPPlaylist alloc] init]; // Model instance data (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance playlistReplaceOrCreateWithData:data
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlaylist***](HRPPlaylist*.md)| Model instance data | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistUpdateAll**
```objc
-(NSNumber*) playlistUpdateAllWithWhere: (NSString*) where
    data: (HRPPlaylist*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPPlaylist* data = [[HRPPlaylist alloc] init]; // An object of model property name/value pairs (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance playlistUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPPlaylist***](HRPPlaylist*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistUpsertPatchPlaylists**
```objc
-(NSNumber*) playlistUpsertPatchPlaylistsWithData: (HRPPlaylist*) data
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlaylist* data = [[HRPPlaylist alloc] init]; // Model instance data (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance playlistUpsertPatchPlaylistsWithData:data
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistUpsertPatchPlaylists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlaylist***](HRPPlaylist*.md)| Model instance data | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistUpsertPutPlaylists**
```objc
-(NSNumber*) playlistUpsertPutPlaylistsWithData: (HRPPlaylist*) data
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlaylist* data = [[HRPPlaylist alloc] init]; // Model instance data (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance playlistUpsertPutPlaylistsWithData:data
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistUpsertPutPlaylists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlaylist***](HRPPlaylist*.md)| Model instance data | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistUpsertWithWhere**
```objc
-(NSNumber*) playlistUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPPlaylist*) data
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPPlaylist* data = [[HRPPlaylist alloc] init]; // An object of model property name/value pairs (optional)

HRPPlaylistApi*apiInstance = [[HRPPlaylistApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance playlistUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistApi->playlistUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPPlaylist***](HRPPlaylist*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

