# HRPRssTags

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**imgTumbnail** | **NSString*** |  | [optional] [default to @"<media>"]
**imgMain** | **NSString*** |  | [optional] [default to @"media"]
**link** | **NSString*** |  | [optional] [default to @"link"]
**postDateTime** | **NSString*** |  | [optional] [default to @"pubDate"]
**_description** | **NSString*** |  | [optional] [default to @"content:encoded"]
**category** | **NSString*** |  | [optional] [default to @"category"]
**title** | **NSString*** |  | [optional] [default to @"title"]
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


