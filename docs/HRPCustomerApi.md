# HRPCustomerApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerActivities**](HRPCustomerApi.md#customeractivities) | **GET** /Customers/{id}/activities | Get Customer activities
[**customerBadge**](HRPCustomerApi.md#customerbadge) | **GET** /Customers/{id}/badge | Get Customer badges
[**customerCardCreate**](HRPCustomerApi.md#customercardcreate) | **POST** /Customers/{id}/cards | Create a Card to a Customer
[**customerCardDelete**](HRPCustomerApi.md#customercarddelete) | **DELETE** /Customers/{id}/cards/{fk} | Remove a Card from a Customer
[**customerCardFindById**](HRPCustomerApi.md#customercardfindbyid) | **GET** /Customers/{id}/cards/{fk} | Get a specific Customer Card
[**customerCardUpdate**](HRPCustomerApi.md#customercardupdate) | **PUT** /Customers/{id}/cards/{fk} | Update a Customer Card
[**customerCards**](HRPCustomerApi.md#customercards) | **GET** /Customers/{id}/cards | Get Customer Cards
[**customerCompetitionOrders**](HRPCustomerApi.md#customercompetitionorders) | **GET** /Customers/{id}/competitions/{fk}/purchases | 
[**customerCouponOrders**](HRPCustomerApi.md#customercouponorders) | **GET** /Customers/{id}/coupons/{fk}/purchases | 
[**customerCreate**](HRPCustomerApi.md#customercreate) | **POST** /Customers | Create a new instance of the model and persist it into the data source.
[**customerDealGroupOrders**](HRPCustomerApi.md#customerdealgrouporders) | **GET** /Customers/{id}/dealGroups/{fk}/purchases | 
[**customerDealPaidOrders**](HRPCustomerApi.md#customerdealpaidorders) | **GET** /Customers/{id}/dealPaids/{fk}/purchases | 
[**customerEventAttending**](HRPCustomerApi.md#customereventattending) | **GET** /Customers/{id}/events/attending | 
[**customerEventOrders**](HRPCustomerApi.md#customereventorders) | **GET** /Customers/{id}/events/{fk}/purchases | 
[**customerFind**](HRPCustomerApi.md#customerfind) | **GET** /Customers | Find all instances of the model matched by filter from the data source.
[**customerFindById**](HRPCustomerApi.md#customerfindbyid) | **GET** /Customers/{id} | Find a model instance by {{id}} from the data source.
[**customerFindOne**](HRPCustomerApi.md#customerfindone) | **GET** /Customers/findOne | Find first instance of the model matched by filter from the data source.
[**customerFollow**](HRPCustomerApi.md#customerfollow) | **PUT** /Customers/{id}/followers/{fk} | Set Customer to follow of another Customer
[**customerFollowers**](HRPCustomerApi.md#customerfollowers) | **GET** /Customers/{id}/followers | Get Customer followers
[**customerFollowings**](HRPCustomerApi.md#customerfollowings) | **GET** /Customers/{id}/followings | Get Customers following a Customer
[**customerLogin**](HRPCustomerApi.md#customerlogin) | **POST** /Customers/login | Login a Customer and receive Authorization Code
[**customerNotifications**](HRPCustomerApi.md#customernotifications) | **GET** /Customers/{id}/notifications | Get Customer Notifications
[**customerPasswordResetAlt**](HRPCustomerApi.md#customerpasswordresetalt) | **POST** /Customers/password/reset | Reset Customer password from just their known email
[**customerPasswordUpdate**](HRPCustomerApi.md#customerpasswordupdate) | **PUT** /Customers/{id}/password | Update the Customer password knowing it&#39;s id and current password
[**customerRefreshToken**](HRPCustomerApi.md#customerrefreshtoken) | **POST** /Customers/token/refresh | Use a Customer Refresh Access Token to receive a new Access Token
[**customerReplaceById**](HRPCustomerApi.md#customerreplacebyid) | **POST** /Customers/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**customerReplaceOrCreate**](HRPCustomerApi.md#customerreplaceorcreate) | **POST** /Customers/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**customerTokenFromAuthorizationCode**](HRPCustomerApi.md#customertokenfromauthorizationcode) | **POST** /Customers/token/request | Authorize a Customer and receive an Access Token
[**customerUnfollow**](HRPCustomerApi.md#customerunfollow) | **DELETE** /Customers/{id}/followers/{fk} | Remove Customer from following of another Customer
[**customerUpdate**](HRPCustomerApi.md#customerupdate) | **PUT** /Customers/{id} | Update the Customer details knowing it&#39;s id
[**customerUpsertWithWhere**](HRPCustomerApi.md#customerupsertwithwhere) | **POST** /Customers/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**customerVerify**](HRPCustomerApi.md#customerverify) | **GET** /Customers/token/verify | Verify that the request is authenticated with a Customer Access Token
[**customerWalletCompetition**](HRPCustomerApi.md#customerwalletcompetition) | **GET** /Customers/{id}/wallet/competitions | 
[**customerWalletCoupon**](HRPCustomerApi.md#customerwalletcoupon) | **GET** /Customers/{id}/wallet/coupons | 
[**customerWalletDeal**](HRPCustomerApi.md#customerwalletdeal) | **GET** /Customers/{id}/wallet/deals | 
[**customerWalletDealGroup**](HRPCustomerApi.md#customerwalletdealgroup) | **GET** /Customers/{id}/wallet/dealgroups | 
[**customerWalletDealPaid**](HRPCustomerApi.md#customerwalletdealpaid) | **GET** /Customers/{id}/wallet/dealpaids | 
[**customerWalletEvent**](HRPCustomerApi.md#customerwalletevent) | **GET** /Customers/{id}/wallet/events | 
[**customerWalletOffer**](HRPCustomerApi.md#customerwalletoffer) | **GET** /Customers/{id}/wallet/offers | 


# **customerActivities**
```objc
-(NSNumber*) customerActivitiesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCustomerActivity>* output, NSError* error)) handler;
```

Get Customer activities

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Get Customer activities
[apiInstance customerActivitiesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPCustomerActivity>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerActivities: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPCustomerActivity>***](HRPCustomerActivity.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerBadge**
```objc
-(NSNumber*) customerBadgeWithId: (NSString*) _id
        completionHandler: (void (^)(HRPCustomerBadge* output, NSError* error)) handler;
```

Get Customer badges

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Get Customer badges
[apiInstance customerBadgeWithId:_id
          completionHandler: ^(HRPCustomerBadge* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerBadge: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPCustomerBadge***](HRPCustomerBadge.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerCardCreate**
```objc
-(NSNumber*) customerCardCreateWithId: (NSString*) _id
    data: (HRPCustomerCardCreateData*) data
        completionHandler: (void (^)(HRPCard* output, NSError* error)) handler;
```

Create a Card to a Customer

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCustomerCardCreateData* data = [[HRPCustomerCardCreateData alloc] init]; //  (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Create a Card to a Customer
[apiInstance customerCardCreateWithId:_id
              data:data
          completionHandler: ^(HRPCard* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerCardCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCustomerCardCreateData***](HRPCustomerCardCreateData*.md)|  | [optional] 

### Return type

[**HRPCard***](HRPCard.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerCardDelete**
```objc
-(NSNumber*) customerCardDeleteWithId: (NSString*) _id
    fk: (NSString*) fk
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Remove a Card from a Customer

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* fk = @"fk_example"; // Card id

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Remove a Card from a Customer
[apiInstance customerCardDeleteWithId:_id
              fk:fk
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerCardDelete: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **fk** | **NSString***| Card id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerCardFindById**
```objc
-(NSNumber*) customerCardFindByIdWithId: (NSString*) _id
    fk: (NSString*) fk
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPCard* output, NSError* error)) handler;
```

Get a specific Customer Card

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* fk = @"fk_example"; // Card id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Get a specific Customer Card
[apiInstance customerCardFindByIdWithId:_id
              fk:fk
              filter:filter
          completionHandler: ^(HRPCard* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerCardFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **fk** | **NSString***| Card id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPCard***](HRPCard.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerCardUpdate**
```objc
-(NSNumber*) customerCardUpdateWithId: (NSString*) _id
    fk: (NSString*) fk
    data: (HRPCard*) data
        completionHandler: (void (^)(HRPCard* output, NSError* error)) handler;
```

Update a Customer Card

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* fk = @"fk_example"; // Card id
HRPCard* data = [[HRPCard alloc] init]; //  (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Update a Customer Card
[apiInstance customerCardUpdateWithId:_id
              fk:fk
              data:data
          completionHandler: ^(HRPCard* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerCardUpdate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **fk** | **NSString***| Card id | 
 **data** | [**HRPCard***](HRPCard*.md)|  | [optional] 

### Return type

[**HRPCard***](HRPCard.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerCards**
```objc
-(NSNumber*) customerCardsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCard>* output, NSError* error)) handler;
```

Get Customer Cards

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Get Customer Cards
[apiInstance customerCardsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPCard>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerCards: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPCard>***](HRPCard.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerCompetitionOrders**
```objc
-(NSNumber*) customerCompetitionOrdersWithId: (NSString*) _id
    fk: (NSString*) fk
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCompetitionPurchase>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* fk = @"fk_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerCompetitionOrdersWithId:_id
              fk:fk
              filter:filter
          completionHandler: ^(NSArray<HRPCompetitionPurchase>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerCompetitionOrders: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **fk** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPCompetitionPurchase>***](HRPCompetitionPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerCouponOrders**
```objc
-(NSNumber*) customerCouponOrdersWithId: (NSString*) _id
    fk: (NSString*) fk
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCouponPurchase>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* fk = @"fk_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerCouponOrdersWithId:_id
              fk:fk
              filter:filter
          completionHandler: ^(NSArray<HRPCouponPurchase>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerCouponOrders: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **fk** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPCouponPurchase>***](HRPCouponPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerCreate**
```objc
-(NSNumber*) customerCreateWithData: (HRPCustomer*) data
        completionHandler: (void (^)(HRPCustomer* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCustomer* data = [[HRPCustomer alloc] init]; // Model instance data (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance customerCreateWithData:data
          completionHandler: ^(HRPCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCustomer***](HRPCustomer*.md)| Model instance data | [optional] 

### Return type

[**HRPCustomer***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerDealGroupOrders**
```objc
-(NSNumber*) customerDealGroupOrdersWithId: (NSString*) _id
    fk: (NSString*) fk
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPDealGroupPurchase>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* fk = @"fk_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerDealGroupOrdersWithId:_id
              fk:fk
              filter:filter
          completionHandler: ^(NSArray<HRPDealGroupPurchase>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerDealGroupOrders: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **fk** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPDealGroupPurchase>***](HRPDealGroupPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerDealPaidOrders**
```objc
-(NSNumber*) customerDealPaidOrdersWithId: (NSString*) _id
    fk: (NSString*) fk
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPDealPaidPurchase>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* fk = @"fk_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerDealPaidOrdersWithId:_id
              fk:fk
              filter:filter
          completionHandler: ^(NSArray<HRPDealPaidPurchase>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerDealPaidOrders: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **fk** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPDealPaidPurchase>***](HRPDealPaidPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerEventAttending**
```objc
-(NSNumber*) customerEventAttendingWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPEvent>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerEventAttendingWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerEventAttending: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPEvent>***](HRPEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerEventOrders**
```objc
-(NSNumber*) customerEventOrdersWithId: (NSString*) _id
    fk: (NSString*) fk
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPEventPurchase>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* fk = @"fk_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerEventOrdersWithId:_id
              fk:fk
              filter:filter
          completionHandler: ^(NSArray<HRPEventPurchase>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerEventOrders: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **fk** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPEventPurchase>***](HRPEventPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerFind**
```objc
-(NSNumber*) customerFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCustomer>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance customerFindWithFilter:filter
          completionHandler: ^(NSArray<HRPCustomer>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPCustomer>***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerFindById**
```objc
-(NSNumber*) customerFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPCustomer* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance customerFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPCustomer***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerFindOne**
```objc
-(NSNumber*) customerFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPCustomer* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance customerFindOneWithFilter:filter
          completionHandler: ^(HRPCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPCustomer***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerFollow**
```objc
-(NSNumber*) customerFollowWithId: (NSString*) _id
    fk: (NSString*) fk
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Set Customer to follow of another Customer

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* fk = @"fk_example"; // Other Customer id

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Set Customer to follow of another Customer
[apiInstance customerFollowWithId:_id
              fk:fk
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerFollow: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **fk** | **NSString***| Other Customer id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerFollowers**
```objc
-(NSNumber*) customerFollowersWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPFollower>* output, NSError* error)) handler;
```

Get Customer followers

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Get Customer followers
[apiInstance customerFollowersWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPFollower>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerFollowers: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPFollower>***](HRPFollower.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerFollowings**
```objc
-(NSNumber*) customerFollowingsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPFollowing>* output, NSError* error)) handler;
```

Get Customers following a Customer

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Get Customers following a Customer
[apiInstance customerFollowingsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPFollowing>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerFollowings: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPFollowing>***](HRPFollowing.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerLogin**
```objc
-(NSNumber*) customerLoginWithCredentials: (HRPCustomerLoginCredentials*) credentials
        completionHandler: (void (^)(HRPCustomer* output, NSError* error)) handler;
```

Login a Customer and receive Authorization Code

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCustomerLoginCredentials* credentials = [[HRPCustomerLoginCredentials alloc] init]; // Login via email and password (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Login a Customer and receive Authorization Code
[apiInstance customerLoginWithCredentials:credentials
          completionHandler: ^(HRPCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerLogin: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials** | [**HRPCustomerLoginCredentials***](HRPCustomerLoginCredentials*.md)| Login via email and password | [optional] 

### Return type

[**HRPCustomer***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerNotifications**
```objc
-(NSNumber*) customerNotificationsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCustomerNotification>* output, NSError* error)) handler;
```

Get Customer Notifications

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Get Customer Notifications
[apiInstance customerNotificationsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPCustomerNotification>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerNotifications: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPCustomerNotification>***](HRPCustomerNotification.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerPasswordResetAlt**
```objc
-(NSNumber*) customerPasswordResetAltWithCredentials: (HRPCustomerPasswordResetCredentials*) credentials
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Reset Customer password from just their known email

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCustomerPasswordResetCredentials* credentials = [[HRPCustomerPasswordResetCredentials alloc] init]; // Customer credentials to reset the password (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Reset Customer password from just their known email
[apiInstance customerPasswordResetAltWithCredentials:credentials
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerPasswordResetAlt: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials** | [**HRPCustomerPasswordResetCredentials***](HRPCustomerPasswordResetCredentials*.md)| Customer credentials to reset the password | [optional] 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerPasswordUpdate**
```objc
-(NSNumber*) customerPasswordUpdateWithId: (NSString*) _id
    credentials: (HRPCustomerPasswordUpdateCredentials*) credentials
        completionHandler: (void (^)(HRPCustomer* output, NSError* error)) handler;
```

Update the Customer password knowing it's id and current password

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCustomerPasswordUpdateCredentials* credentials = [[HRPCustomerPasswordUpdateCredentials alloc] init]; // Customer credentials to update the current password (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Update the Customer password knowing it's id and current password
[apiInstance customerPasswordUpdateWithId:_id
              credentials:credentials
          completionHandler: ^(HRPCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerPasswordUpdate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **credentials** | [**HRPCustomerPasswordUpdateCredentials***](HRPCustomerPasswordUpdateCredentials*.md)| Customer credentials to update the current password | [optional] 

### Return type

[**HRPCustomer***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerRefreshToken**
```objc
-(NSNumber*) customerRefreshTokenWithCredentials: (HRPCustomerRefreshTokenCredentials*) credentials
        completionHandler: (void (^)(HRPAuthToken* output, NSError* error)) handler;
```

Use a Customer Refresh Access Token to receive a new Access Token

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCustomerRefreshTokenCredentials* credentials = [[HRPCustomerRefreshTokenCredentials alloc] init]; // Customer credentials to refresh an expired token (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Use a Customer Refresh Access Token to receive a new Access Token
[apiInstance customerRefreshTokenWithCredentials:credentials
          completionHandler: ^(HRPAuthToken* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerRefreshToken: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials** | [**HRPCustomerRefreshTokenCredentials***](HRPCustomerRefreshTokenCredentials*.md)| Customer credentials to refresh an expired token | [optional] 

### Return type

[**HRPAuthToken***](HRPAuthToken.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerReplaceById**
```objc
-(NSNumber*) customerReplaceByIdWithId: (NSString*) _id
    data: (HRPCustomer*) data
        completionHandler: (void (^)(HRPCustomer* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCustomer* data = [[HRPCustomer alloc] init]; // Model instance data (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance customerReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCustomer***](HRPCustomer*.md)| Model instance data | [optional] 

### Return type

[**HRPCustomer***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerReplaceOrCreate**
```objc
-(NSNumber*) customerReplaceOrCreateWithData: (HRPCustomer*) data
        completionHandler: (void (^)(HRPCustomer* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCustomer* data = [[HRPCustomer alloc] init]; // Model instance data (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance customerReplaceOrCreateWithData:data
          completionHandler: ^(HRPCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCustomer***](HRPCustomer*.md)| Model instance data | [optional] 

### Return type

[**HRPCustomer***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerTokenFromAuthorizationCode**
```objc
-(NSNumber*) customerTokenFromAuthorizationCodeWithCredentials: (HRPCustomerTokenFromAuthorizationCodeCredentials*) credentials
        completionHandler: (void (^)(HRPAuthToken* output, NSError* error)) handler;
```

Authorize a Customer and receive an Access Token

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCustomerTokenFromAuthorizationCodeCredentials* credentials = [[HRPCustomerTokenFromAuthorizationCodeCredentials alloc] init]; // Customer credentials to request authorized (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Authorize a Customer and receive an Access Token
[apiInstance customerTokenFromAuthorizationCodeWithCredentials:credentials
          completionHandler: ^(HRPAuthToken* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerTokenFromAuthorizationCode: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials** | [**HRPCustomerTokenFromAuthorizationCodeCredentials***](HRPCustomerTokenFromAuthorizationCodeCredentials*.md)| Customer credentials to request authorized | [optional] 

### Return type

[**HRPAuthToken***](HRPAuthToken.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerUnfollow**
```objc
-(NSNumber*) customerUnfollowWithId: (NSString*) _id
    fk: (NSString*) fk
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Remove Customer from following of another Customer

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* fk = @"fk_example"; // Other Customer id

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Remove Customer from following of another Customer
[apiInstance customerUnfollowWithId:_id
              fk:fk
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerUnfollow: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **fk** | **NSString***| Other Customer id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerUpdate**
```objc
-(NSNumber*) customerUpdateWithId: (NSString*) _id
    data: (HRPCustomerUpdateData*) data
        completionHandler: (void (^)(HRPCustomer* output, NSError* error)) handler;
```

Update the Customer details knowing it's id

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCustomerUpdateData* data = [[HRPCustomerUpdateData alloc] init]; // Customer data to be updated (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Update the Customer details knowing it's id
[apiInstance customerUpdateWithId:_id
              data:data
          completionHandler: ^(HRPCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerUpdate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCustomerUpdateData***](HRPCustomerUpdateData*.md)| Customer data to be updated | [optional] 

### Return type

[**HRPCustomer***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerUpsertWithWhere**
```objc
-(NSNumber*) customerUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPCustomer*) data
        completionHandler: (void (^)(HRPCustomer* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPCustomer* data = [[HRPCustomer alloc] init]; // An object of model property name/value pairs (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance customerUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPCustomer***](HRPCustomer*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCustomer***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerVerify**
```objc
-(NSNumber*) customerVerifyWithCompletionHandler: 
        (void (^)(NSObject* output, NSError* error)) handler;
```

Verify that the request is authenticated with a Customer Access Token

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];



HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

// Verify that the request is authenticated with a Customer Access Token
[apiInstance customerVerifyWithCompletionHandler: 
          ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerVerify: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerWalletCompetition**
```objc
-(NSNumber*) customerWalletCompetitionWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCompetition>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerWalletCompetitionWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPCompetition>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerWalletCompetition: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPCompetition>***](HRPCompetition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerWalletCoupon**
```objc
-(NSNumber*) customerWalletCouponWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCoupon>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerWalletCouponWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPCoupon>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerWalletCoupon: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPCoupon>***](HRPCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerWalletDeal**
```objc
-(NSNumber*) customerWalletDealWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPOffer>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerWalletDealWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPOffer>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerWalletDeal: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPOffer>***](HRPOffer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerWalletDealGroup**
```objc
-(NSNumber*) customerWalletDealGroupWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPDealGroup>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerWalletDealGroupWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPDealGroup>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerWalletDealGroup: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPDealGroup>***](HRPDealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerWalletDealPaid**
```objc
-(NSNumber*) customerWalletDealPaidWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPDealPaid>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerWalletDealPaidWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPDealPaid>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerWalletDealPaid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPDealPaid>***](HRPDealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerWalletEvent**
```objc
-(NSNumber*) customerWalletEventWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPEvent>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerWalletEventWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerWalletEvent: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPEvent>***](HRPEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **customerWalletOffer**
```objc
-(NSNumber*) customerWalletOfferWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPOffer>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCustomerApi*apiInstance = [[HRPCustomerApi alloc] init];

[apiInstance customerWalletOfferWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPOffer>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCustomerApi->customerWalletOffer: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPOffer>***](HRPOffer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

