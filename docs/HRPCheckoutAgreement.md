# HRPCheckoutAgreement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | [optional] 
**contentHeight** | **NSString*** |  | [optional] 
**checkboxText** | **NSString*** |  | [optional] 
**isActive** | **NSNumber*** |  | 
**isHtml** | **NSNumber*** |  | 
**_id** | **NSNumber*** |  | 
**content** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


