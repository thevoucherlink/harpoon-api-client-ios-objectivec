# HRPPlayerSourceApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playerSourceCount**](HRPPlayerSourceApi.md#playersourcecount) | **GET** /PlayerSources/count | Count instances of the model matched by where from the data source.
[**playerSourceCreate**](HRPPlayerSourceApi.md#playersourcecreate) | **POST** /PlayerSources | Create a new instance of the model and persist it into the data source.
[**playerSourceCreateChangeStreamGetPlayerSourcesChangeStream**](HRPPlayerSourceApi.md#playersourcecreatechangestreamgetplayersourceschangestream) | **GET** /PlayerSources/change-stream | Create a change stream.
[**playerSourceCreateChangeStreamPostPlayerSourcesChangeStream**](HRPPlayerSourceApi.md#playersourcecreatechangestreampostplayersourceschangestream) | **POST** /PlayerSources/change-stream | Create a change stream.
[**playerSourceDeleteById**](HRPPlayerSourceApi.md#playersourcedeletebyid) | **DELETE** /PlayerSources/{id} | Delete a model instance by {{id}} from the data source.
[**playerSourceExistsGetPlayerSourcesidExists**](HRPPlayerSourceApi.md#playersourceexistsgetplayersourcesidexists) | **GET** /PlayerSources/{id}/exists | Check whether a model instance exists in the data source.
[**playerSourceExistsHeadPlayerSourcesid**](HRPPlayerSourceApi.md#playersourceexistsheadplayersourcesid) | **HEAD** /PlayerSources/{id} | Check whether a model instance exists in the data source.
[**playerSourceFind**](HRPPlayerSourceApi.md#playersourcefind) | **GET** /PlayerSources | Find all instances of the model matched by filter from the data source.
[**playerSourceFindById**](HRPPlayerSourceApi.md#playersourcefindbyid) | **GET** /PlayerSources/{id} | Find a model instance by {{id}} from the data source.
[**playerSourceFindOne**](HRPPlayerSourceApi.md#playersourcefindone) | **GET** /PlayerSources/findOne | Find first instance of the model matched by filter from the data source.
[**playerSourcePrototypeGetPlaylistItem**](HRPPlayerSourceApi.md#playersourceprototypegetplaylistitem) | **GET** /PlayerSources/{id}/playlistItem | Fetches belongsTo relation playlistItem.
[**playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid**](HRPPlayerSourceApi.md#playersourceprototypeupdateattributespatchplayersourcesid) | **PATCH** /PlayerSources/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerSourcePrototypeUpdateAttributesPutPlayerSourcesid**](HRPPlayerSourceApi.md#playersourceprototypeupdateattributesputplayersourcesid) | **PUT** /PlayerSources/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerSourceReplaceById**](HRPPlayerSourceApi.md#playersourcereplacebyid) | **POST** /PlayerSources/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playerSourceReplaceOrCreate**](HRPPlayerSourceApi.md#playersourcereplaceorcreate) | **POST** /PlayerSources/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playerSourceUpdateAll**](HRPPlayerSourceApi.md#playersourceupdateall) | **POST** /PlayerSources/update | Update instances of the model matched by {{where}} from the data source.
[**playerSourceUpsertPatchPlayerSources**](HRPPlayerSourceApi.md#playersourceupsertpatchplayersources) | **PATCH** /PlayerSources | Patch an existing model instance or insert a new one into the data source.
[**playerSourceUpsertPutPlayerSources**](HRPPlayerSourceApi.md#playersourceupsertputplayersources) | **PUT** /PlayerSources | Patch an existing model instance or insert a new one into the data source.
[**playerSourceUpsertWithWhere**](HRPPlayerSourceApi.md#playersourceupsertwithwhere) | **POST** /PlayerSources/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **playerSourceCount**
```objc
-(NSNumber*) playerSourceCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance playerSourceCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceCreate**
```objc
-(NSNumber*) playerSourceCreateWithData: (HRPPlayerSource*) data
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlayerSource* data = [[HRPPlayerSource alloc] init]; // Model instance data (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance playerSourceCreateWithData:data
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlayerSource***](HRPPlayerSource*.md)| Model instance data | [optional] 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceCreateChangeStreamGetPlayerSourcesChangeStream**
```objc
-(NSNumber*) playerSourceCreateChangeStreamGetPlayerSourcesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Create a change stream.
[apiInstance playerSourceCreateChangeStreamGetPlayerSourcesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceCreateChangeStreamGetPlayerSourcesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceCreateChangeStreamPostPlayerSourcesChangeStream**
```objc
-(NSNumber*) playerSourceCreateChangeStreamPostPlayerSourcesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Create a change stream.
[apiInstance playerSourceCreateChangeStreamPostPlayerSourcesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceCreateChangeStreamPostPlayerSourcesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceDeleteById**
```objc
-(NSNumber*) playerSourceDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance playerSourceDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceExistsGetPlayerSourcesidExists**
```objc
-(NSNumber*) playerSourceExistsGetPlayerSourcesidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance playerSourceExistsGetPlayerSourcesidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceExistsGetPlayerSourcesidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceExistsHeadPlayerSourcesid**
```objc
-(NSNumber*) playerSourceExistsHeadPlayerSourcesidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance playerSourceExistsHeadPlayerSourcesidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceExistsHeadPlayerSourcesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceFind**
```objc
-(NSNumber*) playerSourceFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPPlayerSource>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance playerSourceFindWithFilter:filter
          completionHandler: ^(NSArray<HRPPlayerSource>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPPlayerSource>***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceFindById**
```objc
-(NSNumber*) playerSourceFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance playerSourceFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceFindOne**
```objc
-(NSNumber*) playerSourceFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance playerSourceFindOneWithFilter:filter
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourcePrototypeGetPlaylistItem**
```objc
-(NSNumber*) playerSourcePrototypeGetPlaylistItemWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Fetches belongsTo relation playlistItem.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlayerSource id
NSNumber* refresh = @true; //  (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Fetches belongsTo relation playlistItem.
[apiInstance playerSourcePrototypeGetPlaylistItemWithId:_id
              refresh:refresh
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourcePrototypeGetPlaylistItem: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlayerSource id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid**
```objc
-(NSNumber*) playerSourcePrototypeUpdateAttributesPatchPlayerSourcesidWithId: (NSString*) _id
    data: (HRPPlayerSource*) data
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlayerSource id
HRPPlayerSource* data = [[HRPPlayerSource alloc] init]; // An object of model property name/value pairs (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance playerSourcePrototypeUpdateAttributesPatchPlayerSourcesidWithId:_id
              data:data
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlayerSource id | 
 **data** | [**HRPPlayerSource***](HRPPlayerSource*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourcePrototypeUpdateAttributesPutPlayerSourcesid**
```objc
-(NSNumber*) playerSourcePrototypeUpdateAttributesPutPlayerSourcesidWithId: (NSString*) _id
    data: (HRPPlayerSource*) data
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlayerSource id
HRPPlayerSource* data = [[HRPPlayerSource alloc] init]; // An object of model property name/value pairs (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance playerSourcePrototypeUpdateAttributesPutPlayerSourcesidWithId:_id
              data:data
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourcePrototypeUpdateAttributesPutPlayerSourcesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlayerSource id | 
 **data** | [**HRPPlayerSource***](HRPPlayerSource*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceReplaceById**
```objc
-(NSNumber*) playerSourceReplaceByIdWithId: (NSString*) _id
    data: (HRPPlayerSource*) data
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPPlayerSource* data = [[HRPPlayerSource alloc] init]; // Model instance data (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance playerSourceReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPPlayerSource***](HRPPlayerSource*.md)| Model instance data | [optional] 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceReplaceOrCreate**
```objc
-(NSNumber*) playerSourceReplaceOrCreateWithData: (HRPPlayerSource*) data
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlayerSource* data = [[HRPPlayerSource alloc] init]; // Model instance data (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance playerSourceReplaceOrCreateWithData:data
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlayerSource***](HRPPlayerSource*.md)| Model instance data | [optional] 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceUpdateAll**
```objc
-(NSNumber*) playerSourceUpdateAllWithWhere: (NSString*) where
    data: (HRPPlayerSource*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPPlayerSource* data = [[HRPPlayerSource alloc] init]; // An object of model property name/value pairs (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance playerSourceUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPPlayerSource***](HRPPlayerSource*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceUpsertPatchPlayerSources**
```objc
-(NSNumber*) playerSourceUpsertPatchPlayerSourcesWithData: (HRPPlayerSource*) data
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlayerSource* data = [[HRPPlayerSource alloc] init]; // Model instance data (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance playerSourceUpsertPatchPlayerSourcesWithData:data
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceUpsertPatchPlayerSources: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlayerSource***](HRPPlayerSource*.md)| Model instance data | [optional] 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceUpsertPutPlayerSources**
```objc
-(NSNumber*) playerSourceUpsertPutPlayerSourcesWithData: (HRPPlayerSource*) data
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlayerSource* data = [[HRPPlayerSource alloc] init]; // Model instance data (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance playerSourceUpsertPutPlayerSourcesWithData:data
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceUpsertPutPlayerSources: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlayerSource***](HRPPlayerSource*.md)| Model instance data | [optional] 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerSourceUpsertWithWhere**
```objc
-(NSNumber*) playerSourceUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPPlayerSource*) data
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPPlayerSource* data = [[HRPPlayerSource alloc] init]; // An object of model property name/value pairs (optional)

HRPPlayerSourceApi*apiInstance = [[HRPPlayerSourceApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance playerSourceUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerSourceApi->playerSourceUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPPlayerSource***](HRPPlayerSource*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

