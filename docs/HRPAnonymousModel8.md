# HRPAnonymousModel8

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brandId** | **NSNumber*** |  | [optional] 
**competitionId** | **NSNumber*** |  | [optional] 
**couponId** | **NSNumber*** |  | [optional] 
**customerId** | **NSNumber*** |  | [optional] 
**eventId** | **NSNumber*** |  | [optional] 
**dealPaid** | **NSNumber*** |  | [optional] 
**dealGroup** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


