# HRPCategoryApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**categoryFindBrandCategories**](HRPCategoryApi.md#categoryfindbrandcategories) | **GET** /Categories/types/brand | 
[**categoryFindCampaignTypeCategories**](HRPCategoryApi.md#categoryfindcampaigntypecategories) | **GET** /Categories/types/campaignType | 
[**categoryFindOfferCategories**](HRPCategoryApi.md#categoryfindoffercategories) | **GET** /Categories/types/offer | 
[**categoryReplaceById**](HRPCategoryApi.md#categoryreplacebyid) | **POST** /Categories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**categoryReplaceOrCreate**](HRPCategoryApi.md#categoryreplaceorcreate) | **POST** /Categories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**categoryUpsertWithWhere**](HRPCategoryApi.md#categoryupsertwithwhere) | **POST** /Categories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **categoryFindBrandCategories**
```objc
-(NSNumber*) categoryFindBrandCategoriesWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCategory>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCategoryApi*apiInstance = [[HRPCategoryApi alloc] init];

[apiInstance categoryFindBrandCategoriesWithFilter:filter
          completionHandler: ^(NSArray<HRPCategory>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCategoryApi->categoryFindBrandCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPCategory>***](HRPCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **categoryFindCampaignTypeCategories**
```objc
-(NSNumber*) categoryFindCampaignTypeCategoriesWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCategory>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCategoryApi*apiInstance = [[HRPCategoryApi alloc] init];

[apiInstance categoryFindCampaignTypeCategoriesWithFilter:filter
          completionHandler: ^(NSArray<HRPCategory>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCategoryApi->categoryFindCampaignTypeCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPCategory>***](HRPCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **categoryFindOfferCategories**
```objc
-(NSNumber*) categoryFindOfferCategoriesWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCategory>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCategoryApi*apiInstance = [[HRPCategoryApi alloc] init];

[apiInstance categoryFindOfferCategoriesWithFilter:filter
          completionHandler: ^(NSArray<HRPCategory>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCategoryApi->categoryFindOfferCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPCategory>***](HRPCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **categoryReplaceById**
```objc
-(NSNumber*) categoryReplaceByIdWithId: (NSString*) _id
    data: (HRPCategory*) data
        completionHandler: (void (^)(HRPCategory* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCategory* data = [[HRPCategory alloc] init]; // Model instance data (optional)

HRPCategoryApi*apiInstance = [[HRPCategoryApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance categoryReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCategoryApi->categoryReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCategory***](HRPCategory*.md)| Model instance data | [optional] 

### Return type

[**HRPCategory***](HRPCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **categoryReplaceOrCreate**
```objc
-(NSNumber*) categoryReplaceOrCreateWithData: (HRPCategory*) data
        completionHandler: (void (^)(HRPCategory* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCategory* data = [[HRPCategory alloc] init]; // Model instance data (optional)

HRPCategoryApi*apiInstance = [[HRPCategoryApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance categoryReplaceOrCreateWithData:data
          completionHandler: ^(HRPCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCategoryApi->categoryReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCategory***](HRPCategory*.md)| Model instance data | [optional] 

### Return type

[**HRPCategory***](HRPCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **categoryUpsertWithWhere**
```objc
-(NSNumber*) categoryUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPCategory*) data
        completionHandler: (void (^)(HRPCategory* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPCategory* data = [[HRPCategory alloc] init]; // An object of model property name/value pairs (optional)

HRPCategoryApi*apiInstance = [[HRPCategoryApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance categoryUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCategoryApi->categoryUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPCategory***](HRPCategory*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCategory***](HRPCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

