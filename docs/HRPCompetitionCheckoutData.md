# HRPCompetitionCheckoutData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chances** | [**NSArray&lt;HRPCompetitionCheckoutChanceData&gt;***](HRPCompetitionCheckoutChanceData.md) | Chances to checkout | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


