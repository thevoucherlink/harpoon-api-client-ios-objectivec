# HRPRadioStreamApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioStreamCount**](HRPRadioStreamApi.md#radiostreamcount) | **GET** /RadioStreams/count | Count instances of the model matched by where from the data source.
[**radioStreamCreate**](HRPRadioStreamApi.md#radiostreamcreate) | **POST** /RadioStreams | Create a new instance of the model and persist it into the data source.
[**radioStreamCreateChangeStreamGetRadioStreamsChangeStream**](HRPRadioStreamApi.md#radiostreamcreatechangestreamgetradiostreamschangestream) | **GET** /RadioStreams/change-stream | Create a change stream.
[**radioStreamCreateChangeStreamPostRadioStreamsChangeStream**](HRPRadioStreamApi.md#radiostreamcreatechangestreampostradiostreamschangestream) | **POST** /RadioStreams/change-stream | Create a change stream.
[**radioStreamDeleteById**](HRPRadioStreamApi.md#radiostreamdeletebyid) | **DELETE** /RadioStreams/{id} | Delete a model instance by {{id}} from the data source.
[**radioStreamExistsGetRadioStreamsidExists**](HRPRadioStreamApi.md#radiostreamexistsgetradiostreamsidexists) | **GET** /RadioStreams/{id}/exists | Check whether a model instance exists in the data source.
[**radioStreamExistsHeadRadioStreamsid**](HRPRadioStreamApi.md#radiostreamexistsheadradiostreamsid) | **HEAD** /RadioStreams/{id} | Check whether a model instance exists in the data source.
[**radioStreamFind**](HRPRadioStreamApi.md#radiostreamfind) | **GET** /RadioStreams | Find all instances of the model matched by filter from the data source.
[**radioStreamFindById**](HRPRadioStreamApi.md#radiostreamfindbyid) | **GET** /RadioStreams/{id} | Find a model instance by {{id}} from the data source.
[**radioStreamFindOne**](HRPRadioStreamApi.md#radiostreamfindone) | **GET** /RadioStreams/findOne | Find first instance of the model matched by filter from the data source.
[**radioStreamPrototypeCountRadioShows**](HRPRadioStreamApi.md#radiostreamprototypecountradioshows) | **GET** /RadioStreams/{id}/radioShows/count | Counts radioShows of RadioStream.
[**radioStreamPrototypeCreateRadioShows**](HRPRadioStreamApi.md#radiostreamprototypecreateradioshows) | **POST** /RadioStreams/{id}/radioShows | Creates a new instance in radioShows of this model.
[**radioStreamPrototypeDeleteRadioShows**](HRPRadioStreamApi.md#radiostreamprototypedeleteradioshows) | **DELETE** /RadioStreams/{id}/radioShows | Deletes all radioShows of this model.
[**radioStreamPrototypeDestroyByIdRadioShows**](HRPRadioStreamApi.md#radiostreamprototypedestroybyidradioshows) | **DELETE** /RadioStreams/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
[**radioStreamPrototypeFindByIdRadioShows**](HRPRadioStreamApi.md#radiostreamprototypefindbyidradioshows) | **GET** /RadioStreams/{id}/radioShows/{fk} | Find a related item by id for radioShows.
[**radioStreamPrototypeGetRadioShows**](HRPRadioStreamApi.md#radiostreamprototypegetradioshows) | **GET** /RadioStreams/{id}/radioShows | Queries radioShows of RadioStream.
[**radioStreamPrototypeUpdateAttributesPatchRadioStreamsid**](HRPRadioStreamApi.md#radiostreamprototypeupdateattributespatchradiostreamsid) | **PATCH** /RadioStreams/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioStreamPrototypeUpdateAttributesPutRadioStreamsid**](HRPRadioStreamApi.md#radiostreamprototypeupdateattributesputradiostreamsid) | **PUT** /RadioStreams/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioStreamPrototypeUpdateByIdRadioShows**](HRPRadioStreamApi.md#radiostreamprototypeupdatebyidradioshows) | **PUT** /RadioStreams/{id}/radioShows/{fk} | Update a related item by id for radioShows.
[**radioStreamRadioShowCurrent**](HRPRadioStreamApi.md#radiostreamradioshowcurrent) | **GET** /RadioStreams/{id}/radioShows/current | 
[**radioStreamRadioShowSchedule**](HRPRadioStreamApi.md#radiostreamradioshowschedule) | **GET** /RadioStreams/{id}/radioShows/schedule | 
[**radioStreamReplaceById**](HRPRadioStreamApi.md#radiostreamreplacebyid) | **POST** /RadioStreams/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioStreamReplaceOrCreate**](HRPRadioStreamApi.md#radiostreamreplaceorcreate) | **POST** /RadioStreams/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioStreamUpdateAll**](HRPRadioStreamApi.md#radiostreamupdateall) | **POST** /RadioStreams/update | Update instances of the model matched by {{where}} from the data source.
[**radioStreamUpsertPatchRadioStreams**](HRPRadioStreamApi.md#radiostreamupsertpatchradiostreams) | **PATCH** /RadioStreams | Patch an existing model instance or insert a new one into the data source.
[**radioStreamUpsertPutRadioStreams**](HRPRadioStreamApi.md#radiostreamupsertputradiostreams) | **PUT** /RadioStreams | Patch an existing model instance or insert a new one into the data source.
[**radioStreamUpsertWithWhere**](HRPRadioStreamApi.md#radiostreamupsertwithwhere) | **POST** /RadioStreams/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **radioStreamCount**
```objc
-(NSNumber*) radioStreamCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance radioStreamCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamCreate**
```objc
-(NSNumber*) radioStreamCreateWithData: (HRPRadioStream*) data
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioStream* data = [[HRPRadioStream alloc] init]; // Model instance data (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance radioStreamCreateWithData:data
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioStream***](HRPRadioStream*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamCreateChangeStreamGetRadioStreamsChangeStream**
```objc
-(NSNumber*) radioStreamCreateChangeStreamGetRadioStreamsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Create a change stream.
[apiInstance radioStreamCreateChangeStreamGetRadioStreamsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamCreateChangeStreamGetRadioStreamsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamCreateChangeStreamPostRadioStreamsChangeStream**
```objc
-(NSNumber*) radioStreamCreateChangeStreamPostRadioStreamsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Create a change stream.
[apiInstance radioStreamCreateChangeStreamPostRadioStreamsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamCreateChangeStreamPostRadioStreamsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamDeleteById**
```objc
-(NSNumber*) radioStreamDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance radioStreamDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamExistsGetRadioStreamsidExists**
```objc
-(NSNumber*) radioStreamExistsGetRadioStreamsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance radioStreamExistsGetRadioStreamsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamExistsGetRadioStreamsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamExistsHeadRadioStreamsid**
```objc
-(NSNumber*) radioStreamExistsHeadRadioStreamsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance radioStreamExistsHeadRadioStreamsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamExistsHeadRadioStreamsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamFind**
```objc
-(NSNumber*) radioStreamFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRadioStream>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance radioStreamFindWithFilter:filter
          completionHandler: ^(NSArray<HRPRadioStream>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPRadioStream>***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamFindById**
```objc
-(NSNumber*) radioStreamFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance radioStreamFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamFindOne**
```objc
-(NSNumber*) radioStreamFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance radioStreamFindOneWithFilter:filter
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamPrototypeCountRadioShows**
```objc
-(NSNumber*) radioStreamPrototypeCountRadioShowsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts radioShows of RadioStream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioStream id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Counts radioShows of RadioStream.
[apiInstance radioStreamPrototypeCountRadioShowsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamPrototypeCountRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioStream id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamPrototypeCreateRadioShows**
```objc
-(NSNumber*) radioStreamPrototypeCreateRadioShowsWithId: (NSString*) _id
    data: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Creates a new instance in radioShows of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioStream id
HRPRadioShow* data = [[HRPRadioShow alloc] init]; //  (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Creates a new instance in radioShows of this model.
[apiInstance radioStreamPrototypeCreateRadioShowsWithId:_id
              data:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamPrototypeCreateRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioStream id | 
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)|  | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamPrototypeDeleteRadioShows**
```objc
-(NSNumber*) radioStreamPrototypeDeleteRadioShowsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all radioShows of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioStream id

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Deletes all radioShows of this model.
[apiInstance radioStreamPrototypeDeleteRadioShowsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamPrototypeDeleteRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioStream id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamPrototypeDestroyByIdRadioShows**
```objc
-(NSNumber*) radioStreamPrototypeDestroyByIdRadioShowsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for radioShows.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShows
NSString* _id = @"_id_example"; // RadioStream id

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Delete a related item by id for radioShows.
[apiInstance radioStreamPrototypeDestroyByIdRadioShowsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamPrototypeDestroyByIdRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShows | 
 **_id** | **NSString***| RadioStream id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamPrototypeFindByIdRadioShows**
```objc
-(NSNumber*) radioStreamPrototypeFindByIdRadioShowsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Find a related item by id for radioShows.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShows
NSString* _id = @"_id_example"; // RadioStream id

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Find a related item by id for radioShows.
[apiInstance radioStreamPrototypeFindByIdRadioShowsWithFk:fk
              _id:_id
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamPrototypeFindByIdRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShows | 
 **_id** | **NSString***| RadioStream id | 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamPrototypeGetRadioShows**
```objc
-(NSNumber*) radioStreamPrototypeGetRadioShowsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRadioShow>* output, NSError* error)) handler;
```

Queries radioShows of RadioStream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioStream id
NSString* filter = @"filter_example"; //  (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Queries radioShows of RadioStream.
[apiInstance radioStreamPrototypeGetRadioShowsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPRadioShow>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamPrototypeGetRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioStream id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPRadioShow>***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamPrototypeUpdateAttributesPatchRadioStreamsid**
```objc
-(NSNumber*) radioStreamPrototypeUpdateAttributesPatchRadioStreamsidWithId: (NSString*) _id
    data: (HRPRadioStream*) data
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioStream id
HRPRadioStream* data = [[HRPRadioStream alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance radioStreamPrototypeUpdateAttributesPatchRadioStreamsidWithId:_id
              data:data
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamPrototypeUpdateAttributesPatchRadioStreamsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioStream id | 
 **data** | [**HRPRadioStream***](HRPRadioStream*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamPrototypeUpdateAttributesPutRadioStreamsid**
```objc
-(NSNumber*) radioStreamPrototypeUpdateAttributesPutRadioStreamsidWithId: (NSString*) _id
    data: (HRPRadioStream*) data
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioStream id
HRPRadioStream* data = [[HRPRadioStream alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance radioStreamPrototypeUpdateAttributesPutRadioStreamsidWithId:_id
              data:data
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamPrototypeUpdateAttributesPutRadioStreamsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioStream id | 
 **data** | [**HRPRadioStream***](HRPRadioStream*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamPrototypeUpdateByIdRadioShows**
```objc
-(NSNumber*) radioStreamPrototypeUpdateByIdRadioShowsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Update a related item by id for radioShows.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShows
NSString* _id = @"_id_example"; // RadioStream id
HRPRadioShow* data = [[HRPRadioShow alloc] init]; //  (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Update a related item by id for radioShows.
[apiInstance radioStreamPrototypeUpdateByIdRadioShowsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamPrototypeUpdateByIdRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShows | 
 **_id** | **NSString***| RadioStream id | 
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)|  | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamRadioShowCurrent**
```objc
-(NSNumber*) radioStreamRadioShowCurrentWithId: (NSString*) _id
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model Id

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

[apiInstance radioStreamRadioShowCurrentWithId:_id
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamRadioShowCurrent: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model Id | 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamRadioShowSchedule**
```objc
-(NSNumber*) radioStreamRadioShowScheduleWithId: (NSString*) _id
        completionHandler: (void (^)(NSArray<HRPRadioShow>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model Id

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

[apiInstance radioStreamRadioShowScheduleWithId:_id
          completionHandler: ^(NSArray<HRPRadioShow>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamRadioShowSchedule: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model Id | 

### Return type

[**NSArray<HRPRadioShow>***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamReplaceById**
```objc
-(NSNumber*) radioStreamReplaceByIdWithId: (NSString*) _id
    data: (HRPRadioStream*) data
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPRadioStream* data = [[HRPRadioStream alloc] init]; // Model instance data (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance radioStreamReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPRadioStream***](HRPRadioStream*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamReplaceOrCreate**
```objc
-(NSNumber*) radioStreamReplaceOrCreateWithData: (HRPRadioStream*) data
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioStream* data = [[HRPRadioStream alloc] init]; // Model instance data (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance radioStreamReplaceOrCreateWithData:data
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioStream***](HRPRadioStream*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamUpdateAll**
```objc
-(NSNumber*) radioStreamUpdateAllWithWhere: (NSString*) where
    data: (HRPRadioStream*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRadioStream* data = [[HRPRadioStream alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance radioStreamUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRadioStream***](HRPRadioStream*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamUpsertPatchRadioStreams**
```objc
-(NSNumber*) radioStreamUpsertPatchRadioStreamsWithData: (HRPRadioStream*) data
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioStream* data = [[HRPRadioStream alloc] init]; // Model instance data (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance radioStreamUpsertPatchRadioStreamsWithData:data
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamUpsertPatchRadioStreams: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioStream***](HRPRadioStream*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamUpsertPutRadioStreams**
```objc
-(NSNumber*) radioStreamUpsertPutRadioStreamsWithData: (HRPRadioStream*) data
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioStream* data = [[HRPRadioStream alloc] init]; // Model instance data (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance radioStreamUpsertPutRadioStreamsWithData:data
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamUpsertPutRadioStreams: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioStream***](HRPRadioStream*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioStreamUpsertWithWhere**
```objc
-(NSNumber*) radioStreamUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPRadioStream*) data
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRadioStream* data = [[HRPRadioStream alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioStreamApi*apiInstance = [[HRPRadioStreamApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance radioStreamUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioStreamApi->radioStreamUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRadioStream***](HRPRadioStream*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

