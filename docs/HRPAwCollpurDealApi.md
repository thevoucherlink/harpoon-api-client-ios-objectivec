# HRPAwCollpurDealApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awCollpurDealCount**](HRPAwCollpurDealApi.md#awcollpurdealcount) | **GET** /AwCollpurDeals/count | Count instances of the model matched by where from the data source.
[**awCollpurDealCreate**](HRPAwCollpurDealApi.md#awcollpurdealcreate) | **POST** /AwCollpurDeals | Create a new instance of the model and persist it into the data source.
[**awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream**](HRPAwCollpurDealApi.md#awcollpurdealcreatechangestreamgetawcollpurdealschangestream) | **GET** /AwCollpurDeals/change-stream | Create a change stream.
[**awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream**](HRPAwCollpurDealApi.md#awcollpurdealcreatechangestreampostawcollpurdealschangestream) | **POST** /AwCollpurDeals/change-stream | Create a change stream.
[**awCollpurDealDeleteById**](HRPAwCollpurDealApi.md#awcollpurdealdeletebyid) | **DELETE** /AwCollpurDeals/{id} | Delete a model instance by {{id}} from the data source.
[**awCollpurDealExistsGetAwCollpurDealsidExists**](HRPAwCollpurDealApi.md#awcollpurdealexistsgetawcollpurdealsidexists) | **GET** /AwCollpurDeals/{id}/exists | Check whether a model instance exists in the data source.
[**awCollpurDealExistsHeadAwCollpurDealsid**](HRPAwCollpurDealApi.md#awcollpurdealexistsheadawcollpurdealsid) | **HEAD** /AwCollpurDeals/{id} | Check whether a model instance exists in the data source.
[**awCollpurDealFind**](HRPAwCollpurDealApi.md#awcollpurdealfind) | **GET** /AwCollpurDeals | Find all instances of the model matched by filter from the data source.
[**awCollpurDealFindById**](HRPAwCollpurDealApi.md#awcollpurdealfindbyid) | **GET** /AwCollpurDeals/{id} | Find a model instance by {{id}} from the data source.
[**awCollpurDealFindOne**](HRPAwCollpurDealApi.md#awcollpurdealfindone) | **GET** /AwCollpurDeals/findOne | Find first instance of the model matched by filter from the data source.
[**awCollpurDealPrototypeCountAwCollpurDealPurchases**](HRPAwCollpurDealApi.md#awcollpurdealprototypecountawcollpurdealpurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases/count | Counts awCollpurDealPurchases of AwCollpurDeal.
[**awCollpurDealPrototypeCreateAwCollpurDealPurchases**](HRPAwCollpurDealApi.md#awcollpurdealprototypecreateawcollpurdealpurchases) | **POST** /AwCollpurDeals/{id}/awCollpurDealPurchases | Creates a new instance in awCollpurDealPurchases of this model.
[**awCollpurDealPrototypeDeleteAwCollpurDealPurchases**](HRPAwCollpurDealApi.md#awcollpurdealprototypedeleteawcollpurdealpurchases) | **DELETE** /AwCollpurDeals/{id}/awCollpurDealPurchases | Deletes all awCollpurDealPurchases of this model.
[**awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases**](HRPAwCollpurDealApi.md#awcollpurdealprototypedestroybyidawcollpurdealpurchases) | **DELETE** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Delete a related item by id for awCollpurDealPurchases.
[**awCollpurDealPrototypeFindByIdAwCollpurDealPurchases**](HRPAwCollpurDealApi.md#awcollpurdealprototypefindbyidawcollpurdealpurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Find a related item by id for awCollpurDealPurchases.
[**awCollpurDealPrototypeGetAwCollpurDealPurchases**](HRPAwCollpurDealApi.md#awcollpurdealprototypegetawcollpurdealpurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases | Queries awCollpurDealPurchases of AwCollpurDeal.
[**awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid**](HRPAwCollpurDealApi.md#awcollpurdealprototypeupdateattributespatchawcollpurdealsid) | **PATCH** /AwCollpurDeals/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid**](HRPAwCollpurDealApi.md#awcollpurdealprototypeupdateattributesputawcollpurdealsid) | **PUT** /AwCollpurDeals/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases**](HRPAwCollpurDealApi.md#awcollpurdealprototypeupdatebyidawcollpurdealpurchases) | **PUT** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Update a related item by id for awCollpurDealPurchases.
[**awCollpurDealReplaceById**](HRPAwCollpurDealApi.md#awcollpurdealreplacebyid) | **POST** /AwCollpurDeals/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awCollpurDealReplaceOrCreate**](HRPAwCollpurDealApi.md#awcollpurdealreplaceorcreate) | **POST** /AwCollpurDeals/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awCollpurDealUpdateAll**](HRPAwCollpurDealApi.md#awcollpurdealupdateall) | **POST** /AwCollpurDeals/update | Update instances of the model matched by {{where}} from the data source.
[**awCollpurDealUpsertPatchAwCollpurDeals**](HRPAwCollpurDealApi.md#awcollpurdealupsertpatchawcollpurdeals) | **PATCH** /AwCollpurDeals | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealUpsertPutAwCollpurDeals**](HRPAwCollpurDealApi.md#awcollpurdealupsertputawcollpurdeals) | **PUT** /AwCollpurDeals | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealUpsertWithWhere**](HRPAwCollpurDealApi.md#awcollpurdealupsertwithwhere) | **POST** /AwCollpurDeals/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awCollpurDealCount**
```objc
-(NSNumber*) awCollpurDealCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance awCollpurDealCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealCreate**
```objc
-(NSNumber*) awCollpurDealCreateWithData: (HRPAwCollpurDeal*) data
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwCollpurDeal* data = [[HRPAwCollpurDeal alloc] init]; // Model instance data (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance awCollpurDealCreateWithData:data
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwCollpurDeal***](HRPAwCollpurDeal*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream**
```objc
-(NSNumber*) awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Create a change stream.
[apiInstance awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream**
```objc
-(NSNumber*) awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Create a change stream.
[apiInstance awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealDeleteById**
```objc
-(NSNumber*) awCollpurDealDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance awCollpurDealDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealExistsGetAwCollpurDealsidExists**
```objc
-(NSNumber*) awCollpurDealExistsGetAwCollpurDealsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awCollpurDealExistsGetAwCollpurDealsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealExistsGetAwCollpurDealsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealExistsHeadAwCollpurDealsid**
```objc
-(NSNumber*) awCollpurDealExistsHeadAwCollpurDealsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awCollpurDealExistsHeadAwCollpurDealsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealExistsHeadAwCollpurDealsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealFind**
```objc
-(NSNumber*) awCollpurDealFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwCollpurDeal>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance awCollpurDealFindWithFilter:filter
          completionHandler: ^(NSArray<HRPAwCollpurDeal>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPAwCollpurDeal>***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealFindById**
```objc
-(NSNumber*) awCollpurDealFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance awCollpurDealFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealFindOne**
```objc
-(NSNumber*) awCollpurDealFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance awCollpurDealFindOneWithFilter:filter
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPrototypeCountAwCollpurDealPurchases**
```objc
-(NSNumber*) awCollpurDealPrototypeCountAwCollpurDealPurchasesWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts awCollpurDealPurchases of AwCollpurDeal.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurDeal id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Counts awCollpurDealPurchases of AwCollpurDeal.
[apiInstance awCollpurDealPrototypeCountAwCollpurDealPurchasesWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealPrototypeCountAwCollpurDealPurchases: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurDeal id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPrototypeCreateAwCollpurDealPurchases**
```objc
-(NSNumber*) awCollpurDealPrototypeCreateAwCollpurDealPurchasesWithId: (NSString*) _id
    data: (HRPAwCollpurDealPurchases*) data
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Creates a new instance in awCollpurDealPurchases of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurDeal id
HRPAwCollpurDealPurchases* data = [[HRPAwCollpurDealPurchases alloc] init]; //  (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Creates a new instance in awCollpurDealPurchases of this model.
[apiInstance awCollpurDealPrototypeCreateAwCollpurDealPurchasesWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealPrototypeCreateAwCollpurDealPurchases: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurDeal id | 
 **data** | [**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases*.md)|  | [optional] 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPrototypeDeleteAwCollpurDealPurchases**
```objc
-(NSNumber*) awCollpurDealPrototypeDeleteAwCollpurDealPurchasesWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all awCollpurDealPurchases of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurDeal id

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Deletes all awCollpurDealPurchases of this model.
[apiInstance awCollpurDealPrototypeDeleteAwCollpurDealPurchasesWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealPrototypeDeleteAwCollpurDealPurchases: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurDeal id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases**
```objc
-(NSNumber*) awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchasesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for awCollpurDealPurchases.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for awCollpurDealPurchases
NSString* _id = @"_id_example"; // AwCollpurDeal id

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Delete a related item by id for awCollpurDealPurchases.
[apiInstance awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchasesWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for awCollpurDealPurchases | 
 **_id** | **NSString***| AwCollpurDeal id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPrototypeFindByIdAwCollpurDealPurchases**
```objc
-(NSNumber*) awCollpurDealPrototypeFindByIdAwCollpurDealPurchasesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Find a related item by id for awCollpurDealPurchases.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for awCollpurDealPurchases
NSString* _id = @"_id_example"; // AwCollpurDeal id

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Find a related item by id for awCollpurDealPurchases.
[apiInstance awCollpurDealPrototypeFindByIdAwCollpurDealPurchasesWithFk:fk
              _id:_id
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealPrototypeFindByIdAwCollpurDealPurchases: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for awCollpurDealPurchases | 
 **_id** | **NSString***| AwCollpurDeal id | 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPrototypeGetAwCollpurDealPurchases**
```objc
-(NSNumber*) awCollpurDealPrototypeGetAwCollpurDealPurchasesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwCollpurDealPurchases>* output, NSError* error)) handler;
```

Queries awCollpurDealPurchases of AwCollpurDeal.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurDeal id
NSString* filter = @"filter_example"; //  (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Queries awCollpurDealPurchases of AwCollpurDeal.
[apiInstance awCollpurDealPrototypeGetAwCollpurDealPurchasesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPAwCollpurDealPurchases>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealPrototypeGetAwCollpurDealPurchases: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurDeal id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPAwCollpurDealPurchases>***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid**
```objc
-(NSNumber*) awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsidWithId: (NSString*) _id
    data: (HRPAwCollpurDeal*) data
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurDeal id
HRPAwCollpurDeal* data = [[HRPAwCollpurDeal alloc] init]; // An object of model property name/value pairs (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsidWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurDeal id | 
 **data** | [**HRPAwCollpurDeal***](HRPAwCollpurDeal*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid**
```objc
-(NSNumber*) awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsidWithId: (NSString*) _id
    data: (HRPAwCollpurDeal*) data
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurDeal id
HRPAwCollpurDeal* data = [[HRPAwCollpurDeal alloc] init]; // An object of model property name/value pairs (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsidWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurDeal id | 
 **data** | [**HRPAwCollpurDeal***](HRPAwCollpurDeal*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases**
```objc
-(NSNumber*) awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchasesWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPAwCollpurDealPurchases*) data
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Update a related item by id for awCollpurDealPurchases.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for awCollpurDealPurchases
NSString* _id = @"_id_example"; // AwCollpurDeal id
HRPAwCollpurDealPurchases* data = [[HRPAwCollpurDealPurchases alloc] init]; //  (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Update a related item by id for awCollpurDealPurchases.
[apiInstance awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchasesWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for awCollpurDealPurchases | 
 **_id** | **NSString***| AwCollpurDeal id | 
 **data** | [**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases*.md)|  | [optional] 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealReplaceById**
```objc
-(NSNumber*) awCollpurDealReplaceByIdWithId: (NSString*) _id
    data: (HRPAwCollpurDeal*) data
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPAwCollpurDeal* data = [[HRPAwCollpurDeal alloc] init]; // Model instance data (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance awCollpurDealReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPAwCollpurDeal***](HRPAwCollpurDeal*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealReplaceOrCreate**
```objc
-(NSNumber*) awCollpurDealReplaceOrCreateWithData: (HRPAwCollpurDeal*) data
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwCollpurDeal* data = [[HRPAwCollpurDeal alloc] init]; // Model instance data (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance awCollpurDealReplaceOrCreateWithData:data
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwCollpurDeal***](HRPAwCollpurDeal*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealUpdateAll**
```objc
-(NSNumber*) awCollpurDealUpdateAllWithWhere: (NSString*) where
    data: (HRPAwCollpurDeal*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwCollpurDeal* data = [[HRPAwCollpurDeal alloc] init]; // An object of model property name/value pairs (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance awCollpurDealUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwCollpurDeal***](HRPAwCollpurDeal*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealUpsertPatchAwCollpurDeals**
```objc
-(NSNumber*) awCollpurDealUpsertPatchAwCollpurDealsWithData: (HRPAwCollpurDeal*) data
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwCollpurDeal* data = [[HRPAwCollpurDeal alloc] init]; // Model instance data (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awCollpurDealUpsertPatchAwCollpurDealsWithData:data
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealUpsertPatchAwCollpurDeals: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwCollpurDeal***](HRPAwCollpurDeal*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealUpsertPutAwCollpurDeals**
```objc
-(NSNumber*) awCollpurDealUpsertPutAwCollpurDealsWithData: (HRPAwCollpurDeal*) data
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwCollpurDeal* data = [[HRPAwCollpurDeal alloc] init]; // Model instance data (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awCollpurDealUpsertPutAwCollpurDealsWithData:data
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealUpsertPutAwCollpurDeals: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwCollpurDeal***](HRPAwCollpurDeal*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealUpsertWithWhere**
```objc
-(NSNumber*) awCollpurDealUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPAwCollpurDeal*) data
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwCollpurDeal* data = [[HRPAwCollpurDeal alloc] init]; // An object of model property name/value pairs (optional)

HRPAwCollpurDealApi*apiInstance = [[HRPAwCollpurDealApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance awCollpurDealUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealApi->awCollpurDealUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwCollpurDeal***](HRPAwCollpurDeal*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

