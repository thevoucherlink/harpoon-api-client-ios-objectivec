# HRPTritonConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stationId** | **NSString*** |  | 
**stationName** | **NSString*** |  | [optional] 
**mp3Mount** | **NSString*** |  | [optional] 
**aacMount** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


