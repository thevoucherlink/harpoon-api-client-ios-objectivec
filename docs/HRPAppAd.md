# HRPAppAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unitId** | **NSString*** |  | 
**unitName** | **NSString*** |  | [optional] 
**unitType** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


