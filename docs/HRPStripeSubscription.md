# HRPStripeSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applicationFeePercent** | **NSNumber*** |  | [optional] 
**cancelAtPeriodEnd** | **NSNumber*** |  | [optional] 
**canceledAt** | **NSNumber*** |  | [optional] 
**currentPeriodEnd** | **NSNumber*** |  | [optional] 
**currentPeriodStart** | **NSNumber*** |  | [optional] 
**metadata** | **NSString*** |  | [optional] 
**quantity** | **NSNumber*** |  | [optional] 
**start** | **NSNumber*** |  | [optional] 
**status** | **NSString*** |  | [optional] 
**taxPercent** | **NSNumber*** |  | [optional] 
**trialEnd** | **NSNumber*** |  | [optional] 
**trialStart** | **NSNumber*** |  | [optional] 
**customer** | **NSString*** |  | 
**discount** | [**HRPStripeDiscount***](HRPStripeDiscount.md) |  | [optional] 
**plan** | [**HRPStripePlan***](HRPStripePlan.md) |  | 
**object** | **NSString*** |  | [optional] 
**endedAt** | **NSNumber*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


