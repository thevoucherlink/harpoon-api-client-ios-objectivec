# HRPAwCollpurCouponApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awCollpurCouponCount**](HRPAwCollpurCouponApi.md#awcollpurcouponcount) | **GET** /AwCollpurCoupons/count | Count instances of the model matched by where from the data source.
[**awCollpurCouponCreate**](HRPAwCollpurCouponApi.md#awcollpurcouponcreate) | **POST** /AwCollpurCoupons | Create a new instance of the model and persist it into the data source.
[**awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream**](HRPAwCollpurCouponApi.md#awcollpurcouponcreatechangestreamgetawcollpurcouponschangestream) | **GET** /AwCollpurCoupons/change-stream | Create a change stream.
[**awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream**](HRPAwCollpurCouponApi.md#awcollpurcouponcreatechangestreampostawcollpurcouponschangestream) | **POST** /AwCollpurCoupons/change-stream | Create a change stream.
[**awCollpurCouponDeleteById**](HRPAwCollpurCouponApi.md#awcollpurcoupondeletebyid) | **DELETE** /AwCollpurCoupons/{id} | Delete a model instance by {{id}} from the data source.
[**awCollpurCouponExistsGetAwCollpurCouponsidExists**](HRPAwCollpurCouponApi.md#awcollpurcouponexistsgetawcollpurcouponsidexists) | **GET** /AwCollpurCoupons/{id}/exists | Check whether a model instance exists in the data source.
[**awCollpurCouponExistsHeadAwCollpurCouponsid**](HRPAwCollpurCouponApi.md#awcollpurcouponexistsheadawcollpurcouponsid) | **HEAD** /AwCollpurCoupons/{id} | Check whether a model instance exists in the data source.
[**awCollpurCouponFind**](HRPAwCollpurCouponApi.md#awcollpurcouponfind) | **GET** /AwCollpurCoupons | Find all instances of the model matched by filter from the data source.
[**awCollpurCouponFindById**](HRPAwCollpurCouponApi.md#awcollpurcouponfindbyid) | **GET** /AwCollpurCoupons/{id} | Find a model instance by {{id}} from the data source.
[**awCollpurCouponFindOne**](HRPAwCollpurCouponApi.md#awcollpurcouponfindone) | **GET** /AwCollpurCoupons/findOne | Find first instance of the model matched by filter from the data source.
[**awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid**](HRPAwCollpurCouponApi.md#awcollpurcouponprototypeupdateattributespatchawcollpurcouponsid) | **PATCH** /AwCollpurCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid**](HRPAwCollpurCouponApi.md#awcollpurcouponprototypeupdateattributesputawcollpurcouponsid) | **PUT** /AwCollpurCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurCouponReplaceById**](HRPAwCollpurCouponApi.md#awcollpurcouponreplacebyid) | **POST** /AwCollpurCoupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awCollpurCouponReplaceOrCreate**](HRPAwCollpurCouponApi.md#awcollpurcouponreplaceorcreate) | **POST** /AwCollpurCoupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awCollpurCouponUpdateAll**](HRPAwCollpurCouponApi.md#awcollpurcouponupdateall) | **POST** /AwCollpurCoupons/update | Update instances of the model matched by {{where}} from the data source.
[**awCollpurCouponUpsertPatchAwCollpurCoupons**](HRPAwCollpurCouponApi.md#awcollpurcouponupsertpatchawcollpurcoupons) | **PATCH** /AwCollpurCoupons | Patch an existing model instance or insert a new one into the data source.
[**awCollpurCouponUpsertPutAwCollpurCoupons**](HRPAwCollpurCouponApi.md#awcollpurcouponupsertputawcollpurcoupons) | **PUT** /AwCollpurCoupons | Patch an existing model instance or insert a new one into the data source.
[**awCollpurCouponUpsertWithWhere**](HRPAwCollpurCouponApi.md#awcollpurcouponupsertwithwhere) | **POST** /AwCollpurCoupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awCollpurCouponCount**
```objc
-(NSNumber*) awCollpurCouponCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance awCollpurCouponCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponCreate**
```objc
-(NSNumber*) awCollpurCouponCreateWithData: (HRPAwCollpurCoupon*) data
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwCollpurCoupon* data = [[HRPAwCollpurCoupon alloc] init]; // Model instance data (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance awCollpurCouponCreateWithData:data
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwCollpurCoupon***](HRPAwCollpurCoupon*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream**
```objc
-(NSNumber*) awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Create a change stream.
[apiInstance awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream**
```objc
-(NSNumber*) awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Create a change stream.
[apiInstance awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponDeleteById**
```objc
-(NSNumber*) awCollpurCouponDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance awCollpurCouponDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponExistsGetAwCollpurCouponsidExists**
```objc
-(NSNumber*) awCollpurCouponExistsGetAwCollpurCouponsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awCollpurCouponExistsGetAwCollpurCouponsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponExistsGetAwCollpurCouponsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponExistsHeadAwCollpurCouponsid**
```objc
-(NSNumber*) awCollpurCouponExistsHeadAwCollpurCouponsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awCollpurCouponExistsHeadAwCollpurCouponsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponExistsHeadAwCollpurCouponsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponFind**
```objc
-(NSNumber*) awCollpurCouponFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwCollpurCoupon>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance awCollpurCouponFindWithFilter:filter
          completionHandler: ^(NSArray<HRPAwCollpurCoupon>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPAwCollpurCoupon>***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponFindById**
```objc
-(NSNumber*) awCollpurCouponFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance awCollpurCouponFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponFindOne**
```objc
-(NSNumber*) awCollpurCouponFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance awCollpurCouponFindOneWithFilter:filter
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid**
```objc
-(NSNumber*) awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsidWithId: (NSString*) _id
    data: (HRPAwCollpurCoupon*) data
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurCoupon id
HRPAwCollpurCoupon* data = [[HRPAwCollpurCoupon alloc] init]; // An object of model property name/value pairs (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsidWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurCoupon id | 
 **data** | [**HRPAwCollpurCoupon***](HRPAwCollpurCoupon*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid**
```objc
-(NSNumber*) awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsidWithId: (NSString*) _id
    data: (HRPAwCollpurCoupon*) data
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurCoupon id
HRPAwCollpurCoupon* data = [[HRPAwCollpurCoupon alloc] init]; // An object of model property name/value pairs (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsidWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurCoupon id | 
 **data** | [**HRPAwCollpurCoupon***](HRPAwCollpurCoupon*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponReplaceById**
```objc
-(NSNumber*) awCollpurCouponReplaceByIdWithId: (NSString*) _id
    data: (HRPAwCollpurCoupon*) data
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPAwCollpurCoupon* data = [[HRPAwCollpurCoupon alloc] init]; // Model instance data (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance awCollpurCouponReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPAwCollpurCoupon***](HRPAwCollpurCoupon*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponReplaceOrCreate**
```objc
-(NSNumber*) awCollpurCouponReplaceOrCreateWithData: (HRPAwCollpurCoupon*) data
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwCollpurCoupon* data = [[HRPAwCollpurCoupon alloc] init]; // Model instance data (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance awCollpurCouponReplaceOrCreateWithData:data
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwCollpurCoupon***](HRPAwCollpurCoupon*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponUpdateAll**
```objc
-(NSNumber*) awCollpurCouponUpdateAllWithWhere: (NSString*) where
    data: (HRPAwCollpurCoupon*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwCollpurCoupon* data = [[HRPAwCollpurCoupon alloc] init]; // An object of model property name/value pairs (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance awCollpurCouponUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwCollpurCoupon***](HRPAwCollpurCoupon*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponUpsertPatchAwCollpurCoupons**
```objc
-(NSNumber*) awCollpurCouponUpsertPatchAwCollpurCouponsWithData: (HRPAwCollpurCoupon*) data
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwCollpurCoupon* data = [[HRPAwCollpurCoupon alloc] init]; // Model instance data (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awCollpurCouponUpsertPatchAwCollpurCouponsWithData:data
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponUpsertPatchAwCollpurCoupons: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwCollpurCoupon***](HRPAwCollpurCoupon*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponUpsertPutAwCollpurCoupons**
```objc
-(NSNumber*) awCollpurCouponUpsertPutAwCollpurCouponsWithData: (HRPAwCollpurCoupon*) data
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwCollpurCoupon* data = [[HRPAwCollpurCoupon alloc] init]; // Model instance data (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awCollpurCouponUpsertPutAwCollpurCouponsWithData:data
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponUpsertPutAwCollpurCoupons: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwCollpurCoupon***](HRPAwCollpurCoupon*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurCouponUpsertWithWhere**
```objc
-(NSNumber*) awCollpurCouponUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPAwCollpurCoupon*) data
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwCollpurCoupon* data = [[HRPAwCollpurCoupon alloc] init]; // An object of model property name/value pairs (optional)

HRPAwCollpurCouponApi*apiInstance = [[HRPAwCollpurCouponApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance awCollpurCouponUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurCouponApi->awCollpurCouponUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwCollpurCoupon***](HRPAwCollpurCoupon*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

