# HRPVenue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | [optional] 
**address** | **NSString*** |  | [optional] 
**city** | **NSString*** |  | [optional] 
**country** | **NSString*** |  | [optional] 
**coordinates** | [**HRPGeoLocation***](HRPGeoLocation.md) |  | [optional] 
**phone** | **NSString*** |  | [optional] 
**email** | **NSString*** |  | [optional] 
**brand** | [**HRPBrand***](HRPBrand.md) |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


