# HRPContact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **NSString*** | Email address like hello@harpoonconnect.com | [optional] 
**website** | **NSString*** | URL like http://harpoonconnect.com or https://www.harpoonconnect.com | [optional] 
**phone** | **NSString*** | Telephone number like 08123456789 | [optional] 
**address** | [**HRPAddress***](HRPAddress.md) |  | [optional] 
**twitter** | **NSString*** | URL starting with https://www.twitter.com/ followed by your username or id | [optional] 
**facebook** | **NSString*** | URL starting with https://www.facebook.com/ followed by your username or id | [optional] 
**whatsapp** | **NSString*** | Telephone number including country code like: +35381234567890 | [optional] 
**snapchat** | **NSString*** | SnapChat username | [optional] 
**googlePlus** | **NSString*** | URL starting with https://plus.google.com/+ followed by your username or id | [optional] 
**linkedIn** | **NSString*** | LinkedIn profile URL | [optional] 
**hashtag** | **NSString*** | A string starting with # and followed by alphanumeric characters (both lower and upper case) | [optional] 
**text** | **NSString*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


