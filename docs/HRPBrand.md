# HRPBrand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | [optional] 
**logo** | **NSString*** |  | [optional] 
**cover** | **NSString*** |  | [optional] 
**_description** | **NSString*** |  | [optional] 
**email** | **NSString*** |  | [optional] 
**phone** | **NSString*** |  | [optional] 
**address** | **NSString*** |  | [optional] 
**managerName** | **NSString*** |  | [optional] 
**followerCount** | **NSNumber*** |  | [optional] [default to @0.0]
**isFollowed** | **NSNumber*** |  | [optional] [default to @0]
**categories** | [**NSArray&lt;HRPCategory&gt;***](HRPCategory.md) |  | [optional] 
**website** | **NSString*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


