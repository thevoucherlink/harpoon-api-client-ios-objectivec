# HRPHarpoonHpublicBrandvenue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**brandId** | **NSNumber*** |  | 
**name** | **NSString*** |  | 
**address** | **NSString*** |  | [optional] 
**latitude** | **NSNumber*** |  | 
**longitude** | **NSNumber*** |  | 
**createdAt** | **NSDate*** |  | 
**updatedAt** | **NSDate*** |  | 
**email** | **NSString*** |  | [optional] 
**phone** | **NSString*** |  | [optional] 
**country** | **NSString*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


