# HRPHarpoonHpublicApplicationpartner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**applicationId** | **NSNumber*** |  | 
**brandId** | **NSNumber*** |  | 
**status** | **NSString*** |  | 
**invitedEmail** | **NSString*** |  | [optional] 
**invitedAt** | **NSDate*** |  | [optional] 
**acceptedAt** | **NSDate*** |  | [optional] 
**createdAt** | **NSDate*** |  | [optional] 
**updatedAt** | **NSDate*** |  | [optional] 
**configList** | **NSNumber*** |  | [optional] 
**configFeed** | **NSNumber*** |  | [optional] 
**configNeedFollow** | **NSNumber*** |  | [optional] 
**configAcceptEvent** | **NSNumber*** |  | [optional] 
**configAcceptCoupon** | **NSNumber*** |  | [optional] 
**configAcceptDealsimple** | **NSNumber*** |  | [optional] 
**configAcceptDealgroup** | **NSNumber*** |  | [optional] 
**configAcceptNotificationpush** | **NSNumber*** |  | [optional] 
**configAcceptNotificationbeacon** | **NSNumber*** |  | [optional] 
**isOwner** | **NSNumber*** |  | [optional] 
**configApproveEvent** | **NSNumber*** |  | [optional] 
**configApproveCoupon** | **NSNumber*** |  | [optional] 
**configApproveDealsimple** | **NSNumber*** |  | [optional] 
**configApproveDealgroup** | **NSNumber*** |  | [optional] 
**udropshipVendor** | **NSObject*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


