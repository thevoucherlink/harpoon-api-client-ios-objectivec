# HRPStripeInvoiceApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeInvoiceCount**](HRPStripeInvoiceApi.md#stripeinvoicecount) | **GET** /StripeInvoices/count | Count instances of the model matched by where from the data source.
[**stripeInvoiceCreate**](HRPStripeInvoiceApi.md#stripeinvoicecreate) | **POST** /StripeInvoices | Create a new instance of the model and persist it into the data source.
[**stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream**](HRPStripeInvoiceApi.md#stripeinvoicecreatechangestreamgetstripeinvoiceschangestream) | **GET** /StripeInvoices/change-stream | Create a change stream.
[**stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream**](HRPStripeInvoiceApi.md#stripeinvoicecreatechangestreampoststripeinvoiceschangestream) | **POST** /StripeInvoices/change-stream | Create a change stream.
[**stripeInvoiceDeleteById**](HRPStripeInvoiceApi.md#stripeinvoicedeletebyid) | **DELETE** /StripeInvoices/{id} | Delete a model instance by {{id}} from the data source.
[**stripeInvoiceExistsGetStripeInvoicesidExists**](HRPStripeInvoiceApi.md#stripeinvoiceexistsgetstripeinvoicesidexists) | **GET** /StripeInvoices/{id}/exists | Check whether a model instance exists in the data source.
[**stripeInvoiceExistsHeadStripeInvoicesid**](HRPStripeInvoiceApi.md#stripeinvoiceexistsheadstripeinvoicesid) | **HEAD** /StripeInvoices/{id} | Check whether a model instance exists in the data source.
[**stripeInvoiceFind**](HRPStripeInvoiceApi.md#stripeinvoicefind) | **GET** /StripeInvoices | Find all instances of the model matched by filter from the data source.
[**stripeInvoiceFindById**](HRPStripeInvoiceApi.md#stripeinvoicefindbyid) | **GET** /StripeInvoices/{id} | Find a model instance by {{id}} from the data source.
[**stripeInvoiceFindOne**](HRPStripeInvoiceApi.md#stripeinvoicefindone) | **GET** /StripeInvoices/findOne | Find first instance of the model matched by filter from the data source.
[**stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid**](HRPStripeInvoiceApi.md#stripeinvoiceprototypeupdateattributespatchstripeinvoicesid) | **PATCH** /StripeInvoices/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid**](HRPStripeInvoiceApi.md#stripeinvoiceprototypeupdateattributesputstripeinvoicesid) | **PUT** /StripeInvoices/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoiceReplaceById**](HRPStripeInvoiceApi.md#stripeinvoicereplacebyid) | **POST** /StripeInvoices/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeInvoiceReplaceOrCreate**](HRPStripeInvoiceApi.md#stripeinvoicereplaceorcreate) | **POST** /StripeInvoices/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeInvoiceUpdateAll**](HRPStripeInvoiceApi.md#stripeinvoiceupdateall) | **POST** /StripeInvoices/update | Update instances of the model matched by {{where}} from the data source.
[**stripeInvoiceUpsertPatchStripeInvoices**](HRPStripeInvoiceApi.md#stripeinvoiceupsertpatchstripeinvoices) | **PATCH** /StripeInvoices | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceUpsertPutStripeInvoices**](HRPStripeInvoiceApi.md#stripeinvoiceupsertputstripeinvoices) | **PUT** /StripeInvoices | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceUpsertWithWhere**](HRPStripeInvoiceApi.md#stripeinvoiceupsertwithwhere) | **POST** /StripeInvoices/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **stripeInvoiceCount**
```objc
-(NSNumber*) stripeInvoiceCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance stripeInvoiceCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceCreate**
```objc
-(NSNumber*) stripeInvoiceCreateWithData: (HRPStripeInvoice*) data
        completionHandler: (void (^)(HRPStripeInvoice* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeInvoice* data = [[HRPStripeInvoice alloc] init]; // Model instance data (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance stripeInvoiceCreateWithData:data
          completionHandler: ^(HRPStripeInvoice* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeInvoice***](HRPStripeInvoice*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeInvoice***](HRPStripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream**
```objc
-(NSNumber*) stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Create a change stream.
[apiInstance stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream**
```objc
-(NSNumber*) stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Create a change stream.
[apiInstance stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceDeleteById**
```objc
-(NSNumber*) stripeInvoiceDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance stripeInvoiceDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceExistsGetStripeInvoicesidExists**
```objc
-(NSNumber*) stripeInvoiceExistsGetStripeInvoicesidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance stripeInvoiceExistsGetStripeInvoicesidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceExistsGetStripeInvoicesidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceExistsHeadStripeInvoicesid**
```objc
-(NSNumber*) stripeInvoiceExistsHeadStripeInvoicesidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance stripeInvoiceExistsHeadStripeInvoicesidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceExistsHeadStripeInvoicesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceFind**
```objc
-(NSNumber*) stripeInvoiceFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPStripeInvoice>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance stripeInvoiceFindWithFilter:filter
          completionHandler: ^(NSArray<HRPStripeInvoice>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPStripeInvoice>***](HRPStripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceFindById**
```objc
-(NSNumber*) stripeInvoiceFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPStripeInvoice* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance stripeInvoiceFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPStripeInvoice* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPStripeInvoice***](HRPStripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceFindOne**
```objc
-(NSNumber*) stripeInvoiceFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPStripeInvoice* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance stripeInvoiceFindOneWithFilter:filter
          completionHandler: ^(HRPStripeInvoice* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPStripeInvoice***](HRPStripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid**
```objc
-(NSNumber*) stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesidWithId: (NSString*) _id
    data: (HRPStripeInvoice*) data
        completionHandler: (void (^)(HRPStripeInvoice* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // StripeInvoice id
HRPStripeInvoice* data = [[HRPStripeInvoice alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesidWithId:_id
              data:data
          completionHandler: ^(HRPStripeInvoice* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| StripeInvoice id | 
 **data** | [**HRPStripeInvoice***](HRPStripeInvoice*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeInvoice***](HRPStripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid**
```objc
-(NSNumber*) stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesidWithId: (NSString*) _id
    data: (HRPStripeInvoice*) data
        completionHandler: (void (^)(HRPStripeInvoice* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // StripeInvoice id
HRPStripeInvoice* data = [[HRPStripeInvoice alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesidWithId:_id
              data:data
          completionHandler: ^(HRPStripeInvoice* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| StripeInvoice id | 
 **data** | [**HRPStripeInvoice***](HRPStripeInvoice*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeInvoice***](HRPStripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceReplaceById**
```objc
-(NSNumber*) stripeInvoiceReplaceByIdWithId: (NSString*) _id
    data: (HRPStripeInvoice*) data
        completionHandler: (void (^)(HRPStripeInvoice* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPStripeInvoice* data = [[HRPStripeInvoice alloc] init]; // Model instance data (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance stripeInvoiceReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPStripeInvoice* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPStripeInvoice***](HRPStripeInvoice*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeInvoice***](HRPStripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceReplaceOrCreate**
```objc
-(NSNumber*) stripeInvoiceReplaceOrCreateWithData: (HRPStripeInvoice*) data
        completionHandler: (void (^)(HRPStripeInvoice* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeInvoice* data = [[HRPStripeInvoice alloc] init]; // Model instance data (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance stripeInvoiceReplaceOrCreateWithData:data
          completionHandler: ^(HRPStripeInvoice* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeInvoice***](HRPStripeInvoice*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeInvoice***](HRPStripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceUpdateAll**
```objc
-(NSNumber*) stripeInvoiceUpdateAllWithWhere: (NSString*) where
    data: (HRPStripeInvoice*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPStripeInvoice* data = [[HRPStripeInvoice alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance stripeInvoiceUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPStripeInvoice***](HRPStripeInvoice*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceUpsertPatchStripeInvoices**
```objc
-(NSNumber*) stripeInvoiceUpsertPatchStripeInvoicesWithData: (HRPStripeInvoice*) data
        completionHandler: (void (^)(HRPStripeInvoice* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeInvoice* data = [[HRPStripeInvoice alloc] init]; // Model instance data (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance stripeInvoiceUpsertPatchStripeInvoicesWithData:data
          completionHandler: ^(HRPStripeInvoice* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceUpsertPatchStripeInvoices: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeInvoice***](HRPStripeInvoice*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeInvoice***](HRPStripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceUpsertPutStripeInvoices**
```objc
-(NSNumber*) stripeInvoiceUpsertPutStripeInvoicesWithData: (HRPStripeInvoice*) data
        completionHandler: (void (^)(HRPStripeInvoice* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeInvoice* data = [[HRPStripeInvoice alloc] init]; // Model instance data (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance stripeInvoiceUpsertPutStripeInvoicesWithData:data
          completionHandler: ^(HRPStripeInvoice* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceUpsertPutStripeInvoices: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeInvoice***](HRPStripeInvoice*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeInvoice***](HRPStripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceUpsertWithWhere**
```objc
-(NSNumber*) stripeInvoiceUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPStripeInvoice*) data
        completionHandler: (void (^)(HRPStripeInvoice* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPStripeInvoice* data = [[HRPStripeInvoice alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeInvoiceApi*apiInstance = [[HRPStripeInvoiceApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance stripeInvoiceUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPStripeInvoice* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceApi->stripeInvoiceUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPStripeInvoice***](HRPStripeInvoice*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeInvoice***](HRPStripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

