# HRPCouponApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**couponCheckout**](HRPCouponApi.md#couponcheckout) | **POST** /Coupons/{id}/checkout | 
[**couponFind**](HRPCouponApi.md#couponfind) | **GET** /Coupons | Find all instances of the model matched by filter from the data source.
[**couponFindById**](HRPCouponApi.md#couponfindbyid) | **GET** /Coupons/{id} | Find a model instance by {{id}} from the data source.
[**couponFindOne**](HRPCouponApi.md#couponfindone) | **GET** /Coupons/findOne | Find first instance of the model matched by filter from the data source.
[**couponRedeem**](HRPCouponApi.md#couponredeem) | **POST** /Coupons/{id}/redeem | 
[**couponReplaceById**](HRPCouponApi.md#couponreplacebyid) | **POST** /Coupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**couponReplaceOrCreate**](HRPCouponApi.md#couponreplaceorcreate) | **POST** /Coupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**couponUpsertWithWhere**](HRPCouponApi.md#couponupsertwithwhere) | **POST** /Coupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**couponVenues**](HRPCouponApi.md#couponvenues) | **GET** /Coupons/{id}/venues | 


# **couponCheckout**
```objc
-(NSNumber*) couponCheckoutWithId: (NSString*) _id
    data: (HRPCouponCheckoutData*) data
        completionHandler: (void (^)(HRPCouponCheckout* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCouponCheckoutData* data = [[HRPCouponCheckoutData alloc] init]; //  (optional)

HRPCouponApi*apiInstance = [[HRPCouponApi alloc] init];

[apiInstance couponCheckoutWithId:_id
              data:data
          completionHandler: ^(HRPCouponCheckout* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCouponApi->couponCheckout: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCouponCheckoutData***](HRPCouponCheckoutData*.md)|  | [optional] 

### Return type

[**HRPCouponCheckout***](HRPCouponCheckout.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **couponFind**
```objc
-(NSNumber*) couponFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCoupon>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPCouponApi*apiInstance = [[HRPCouponApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance couponFindWithFilter:filter
          completionHandler: ^(NSArray<HRPCoupon>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCouponApi->couponFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPCoupon>***](HRPCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **couponFindById**
```objc
-(NSNumber*) couponFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPCoupon* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCouponApi*apiInstance = [[HRPCouponApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance couponFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCouponApi->couponFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPCoupon***](HRPCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **couponFindOne**
```objc
-(NSNumber*) couponFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPCoupon* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPCouponApi*apiInstance = [[HRPCouponApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance couponFindOneWithFilter:filter
          completionHandler: ^(HRPCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCouponApi->couponFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPCoupon***](HRPCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **couponRedeem**
```objc
-(NSNumber*) couponRedeemWithId: (NSString*) _id
    data: (HRPCouponRedeemData*) data
        completionHandler: (void (^)(HRPCouponPurchase* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCouponRedeemData* data = [[HRPCouponRedeemData alloc] init]; //  (optional)

HRPCouponApi*apiInstance = [[HRPCouponApi alloc] init];

[apiInstance couponRedeemWithId:_id
              data:data
          completionHandler: ^(HRPCouponPurchase* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCouponApi->couponRedeem: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCouponRedeemData***](HRPCouponRedeemData*.md)|  | [optional] 

### Return type

[**HRPCouponPurchase***](HRPCouponPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **couponReplaceById**
```objc
-(NSNumber*) couponReplaceByIdWithId: (NSString*) _id
    data: (HRPCoupon*) data
        completionHandler: (void (^)(HRPCoupon* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCoupon* data = [[HRPCoupon alloc] init]; // Model instance data (optional)

HRPCouponApi*apiInstance = [[HRPCouponApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance couponReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCouponApi->couponReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCoupon***](HRPCoupon*.md)| Model instance data | [optional] 

### Return type

[**HRPCoupon***](HRPCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **couponReplaceOrCreate**
```objc
-(NSNumber*) couponReplaceOrCreateWithData: (HRPCoupon*) data
        completionHandler: (void (^)(HRPCoupon* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCoupon* data = [[HRPCoupon alloc] init]; // Model instance data (optional)

HRPCouponApi*apiInstance = [[HRPCouponApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance couponReplaceOrCreateWithData:data
          completionHandler: ^(HRPCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCouponApi->couponReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCoupon***](HRPCoupon*.md)| Model instance data | [optional] 

### Return type

[**HRPCoupon***](HRPCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **couponUpsertWithWhere**
```objc
-(NSNumber*) couponUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPCoupon*) data
        completionHandler: (void (^)(HRPCoupon* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPCoupon* data = [[HRPCoupon alloc] init]; // An object of model property name/value pairs (optional)

HRPCouponApi*apiInstance = [[HRPCouponApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance couponUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCouponApi->couponUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPCoupon***](HRPCoupon*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCoupon***](HRPCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **couponVenues**
```objc
-(NSNumber*) couponVenuesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPVenue>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCouponApi*apiInstance = [[HRPCouponApi alloc] init];

[apiInstance couponVenuesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPVenue>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCouponApi->couponVenues: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPVenue>***](HRPVenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

