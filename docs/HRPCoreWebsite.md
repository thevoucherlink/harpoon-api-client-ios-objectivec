# HRPCoreWebsite

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**code** | **NSString*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**sortOrder** | **NSNumber*** |  | 
**defaultGroupId** | **NSNumber*** |  | 
**isDefault** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


