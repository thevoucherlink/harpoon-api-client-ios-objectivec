# HRPCatalogInventoryStockItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**backorders** | **NSNumber*** |  | 
**enableQtyIncrements** | **NSNumber*** |  | 
**isDecimalDivided** | **NSNumber*** |  | 
**isQtyDecimal** | **NSNumber*** |  | 
**isInStock** | **NSNumber*** |  | 
**_id** | **NSNumber*** |  | 
**lowStockDate** | **NSDate*** |  | [optional] 
**manageStock** | **NSNumber*** |  | 
**minQty** | **NSString*** |  | 
**maxSaleQty** | **NSString*** |  | 
**minSaleQty** | **NSString*** |  | 
**notifyStockQty** | **NSString*** |  | [optional] 
**productId** | **NSNumber*** |  | 
**qty** | **NSString*** |  | 
**stockId** | **NSNumber*** |  | 
**useConfigBackorders** | **NSNumber*** |  | 
**qtyIncrements** | **NSString*** |  | 
**stockStatusChangedAuto** | **NSNumber*** |  | 
**useConfigEnableQtyInc** | **NSNumber*** |  | 
**useConfigMaxSaleQty** | **NSNumber*** |  | 
**useConfigManageStock** | **NSNumber*** |  | 
**useConfigMinQty** | **NSNumber*** |  | 
**useConfigNotifyStockQty** | **NSNumber*** |  | 
**useConfigMinSaleQty** | **NSNumber*** |  | 
**useConfigQtyIncrements** | **NSNumber*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


