# HRPAwEventbookingEventApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingEventCount**](HRPAwEventbookingEventApi.md#aweventbookingeventcount) | **GET** /AwEventbookingEvents/count | Count instances of the model matched by where from the data source.
[**awEventbookingEventCreate**](HRPAwEventbookingEventApi.md#aweventbookingeventcreate) | **POST** /AwEventbookingEvents | Create a new instance of the model and persist it into the data source.
[**awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream**](HRPAwEventbookingEventApi.md#aweventbookingeventcreatechangestreamgetaweventbookingeventschangestream) | **GET** /AwEventbookingEvents/change-stream | Create a change stream.
[**awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream**](HRPAwEventbookingEventApi.md#aweventbookingeventcreatechangestreampostaweventbookingeventschangestream) | **POST** /AwEventbookingEvents/change-stream | Create a change stream.
[**awEventbookingEventDeleteById**](HRPAwEventbookingEventApi.md#aweventbookingeventdeletebyid) | **DELETE** /AwEventbookingEvents/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingEventExistsGetAwEventbookingEventsidExists**](HRPAwEventbookingEventApi.md#aweventbookingeventexistsgetaweventbookingeventsidexists) | **GET** /AwEventbookingEvents/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingEventExistsHeadAwEventbookingEventsid**](HRPAwEventbookingEventApi.md#aweventbookingeventexistsheadaweventbookingeventsid) | **HEAD** /AwEventbookingEvents/{id} | Check whether a model instance exists in the data source.
[**awEventbookingEventFind**](HRPAwEventbookingEventApi.md#aweventbookingeventfind) | **GET** /AwEventbookingEvents | Find all instances of the model matched by filter from the data source.
[**awEventbookingEventFindById**](HRPAwEventbookingEventApi.md#aweventbookingeventfindbyid) | **GET** /AwEventbookingEvents/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingEventFindOne**](HRPAwEventbookingEventApi.md#aweventbookingeventfindone) | **GET** /AwEventbookingEvents/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingEventPrototypeCountAttributes**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypecountattributes) | **GET** /AwEventbookingEvents/{id}/attributes/count | Counts attributes of AwEventbookingEvent.
[**awEventbookingEventPrototypeCountPurchasedTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypecountpurchasedtickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets/count | Counts purchasedTickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeCountTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypecounttickets) | **GET** /AwEventbookingEvents/{id}/tickets/count | Counts tickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeCreateAttributes**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypecreateattributes) | **POST** /AwEventbookingEvents/{id}/attributes | Creates a new instance in attributes of this model.
[**awEventbookingEventPrototypeCreatePurchasedTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypecreatepurchasedtickets) | **POST** /AwEventbookingEvents/{id}/purchasedTickets | Creates a new instance in purchasedTickets of this model.
[**awEventbookingEventPrototypeCreateTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypecreatetickets) | **POST** /AwEventbookingEvents/{id}/tickets | Creates a new instance in tickets of this model.
[**awEventbookingEventPrototypeDeleteAttributes**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypedeleteattributes) | **DELETE** /AwEventbookingEvents/{id}/attributes | Deletes all attributes of this model.
[**awEventbookingEventPrototypeDeletePurchasedTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypedeletepurchasedtickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets | Deletes all purchasedTickets of this model.
[**awEventbookingEventPrototypeDeleteTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypedeletetickets) | **DELETE** /AwEventbookingEvents/{id}/tickets | Deletes all tickets of this model.
[**awEventbookingEventPrototypeDestroyByIdAttributes**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypedestroybyidattributes) | **DELETE** /AwEventbookingEvents/{id}/attributes/{fk} | Delete a related item by id for attributes.
[**awEventbookingEventPrototypeDestroyByIdPurchasedTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypedestroybyidpurchasedtickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Delete a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeDestroyByIdTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypedestroybyidtickets) | **DELETE** /AwEventbookingEvents/{id}/tickets/{fk} | Delete a related item by id for tickets.
[**awEventbookingEventPrototypeExistsPurchasedTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypeexistspurchasedtickets) | **HEAD** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Check the existence of purchasedTickets relation to an item by id.
[**awEventbookingEventPrototypeFindByIdAttributes**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypefindbyidattributes) | **GET** /AwEventbookingEvents/{id}/attributes/{fk} | Find a related item by id for attributes.
[**awEventbookingEventPrototypeFindByIdPurchasedTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypefindbyidpurchasedtickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Find a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeFindByIdTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypefindbyidtickets) | **GET** /AwEventbookingEvents/{id}/tickets/{fk} | Find a related item by id for tickets.
[**awEventbookingEventPrototypeGetAttributes**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypegetattributes) | **GET** /AwEventbookingEvents/{id}/attributes | Queries attributes of AwEventbookingEvent.
[**awEventbookingEventPrototypeGetPurchasedTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypegetpurchasedtickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets | Queries purchasedTickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeGetTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypegettickets) | **GET** /AwEventbookingEvents/{id}/tickets | Queries tickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeLinkPurchasedTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypelinkpurchasedtickets) | **PUT** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Add a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeUnlinkPurchasedTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypeunlinkpurchasedtickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Remove the purchasedTickets relation to an item by id.
[**awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypeupdateattributespatchaweventbookingeventsid) | **PATCH** /AwEventbookingEvents/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypeupdateattributesputaweventbookingeventsid) | **PUT** /AwEventbookingEvents/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventPrototypeUpdateByIdAttributes**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypeupdatebyidattributes) | **PUT** /AwEventbookingEvents/{id}/attributes/{fk} | Update a related item by id for attributes.
[**awEventbookingEventPrototypeUpdateByIdPurchasedTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypeupdatebyidpurchasedtickets) | **PUT** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Update a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeUpdateByIdTickets**](HRPAwEventbookingEventApi.md#aweventbookingeventprototypeupdatebyidtickets) | **PUT** /AwEventbookingEvents/{id}/tickets/{fk} | Update a related item by id for tickets.
[**awEventbookingEventReplaceById**](HRPAwEventbookingEventApi.md#aweventbookingeventreplacebyid) | **POST** /AwEventbookingEvents/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingEventReplaceOrCreate**](HRPAwEventbookingEventApi.md#aweventbookingeventreplaceorcreate) | **POST** /AwEventbookingEvents/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingEventUpdateAll**](HRPAwEventbookingEventApi.md#aweventbookingeventupdateall) | **POST** /AwEventbookingEvents/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingEventUpsertPatchAwEventbookingEvents**](HRPAwEventbookingEventApi.md#aweventbookingeventupsertpatchaweventbookingevents) | **PATCH** /AwEventbookingEvents | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventUpsertPutAwEventbookingEvents**](HRPAwEventbookingEventApi.md#aweventbookingeventupsertputaweventbookingevents) | **PUT** /AwEventbookingEvents | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventUpsertWithWhere**](HRPAwEventbookingEventApi.md#aweventbookingeventupsertwithwhere) | **POST** /AwEventbookingEvents/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awEventbookingEventCount**
```objc
-(NSNumber*) awEventbookingEventCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance awEventbookingEventCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventCreate**
```objc
-(NSNumber*) awEventbookingEventCreateWithData: (HRPAwEventbookingEvent*) data
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingEvent* data = [[HRPAwEventbookingEvent alloc] init]; // Model instance data (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance awEventbookingEventCreateWithData:data
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingEvent***](HRPAwEventbookingEvent*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream**
```objc
-(NSNumber*) awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Create a change stream.
[apiInstance awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream**
```objc
-(NSNumber*) awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Create a change stream.
[apiInstance awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventDeleteById**
```objc
-(NSNumber*) awEventbookingEventDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance awEventbookingEventDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventExistsGetAwEventbookingEventsidExists**
```objc
-(NSNumber*) awEventbookingEventExistsGetAwEventbookingEventsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awEventbookingEventExistsGetAwEventbookingEventsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventExistsGetAwEventbookingEventsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventExistsHeadAwEventbookingEventsid**
```objc
-(NSNumber*) awEventbookingEventExistsHeadAwEventbookingEventsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awEventbookingEventExistsHeadAwEventbookingEventsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventExistsHeadAwEventbookingEventsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventFind**
```objc
-(NSNumber*) awEventbookingEventFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwEventbookingEvent>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance awEventbookingEventFindWithFilter:filter
          completionHandler: ^(NSArray<HRPAwEventbookingEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPAwEventbookingEvent>***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventFindById**
```objc
-(NSNumber*) awEventbookingEventFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance awEventbookingEventFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventFindOne**
```objc
-(NSNumber*) awEventbookingEventFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance awEventbookingEventFindOneWithFilter:filter
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeCountAttributes**
```objc
-(NSNumber*) awEventbookingEventPrototypeCountAttributesWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts attributes of AwEventbookingEvent.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Counts attributes of AwEventbookingEvent.
[apiInstance awEventbookingEventPrototypeCountAttributesWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeCountAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeCountPurchasedTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeCountPurchasedTicketsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts purchasedTickets of AwEventbookingEvent.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Counts purchasedTickets of AwEventbookingEvent.
[apiInstance awEventbookingEventPrototypeCountPurchasedTicketsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeCountPurchasedTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeCountTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeCountTicketsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts tickets of AwEventbookingEvent.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Counts tickets of AwEventbookingEvent.
[apiInstance awEventbookingEventPrototypeCountTicketsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeCountTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeCreateAttributes**
```objc
-(NSNumber*) awEventbookingEventPrototypeCreateAttributesWithId: (NSString*) _id
    data: (HRPAwEventbookingEventAttribute*) data
        completionHandler: (void (^)(HRPAwEventbookingEventAttribute* output, NSError* error)) handler;
```

Creates a new instance in attributes of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id
HRPAwEventbookingEventAttribute* data = [[HRPAwEventbookingEventAttribute alloc] init]; //  (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Creates a new instance in attributes of this model.
[apiInstance awEventbookingEventPrototypeCreateAttributesWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEventAttribute* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeCreateAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 
 **data** | [**HRPAwEventbookingEventAttribute***](HRPAwEventbookingEventAttribute*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingEventAttribute***](HRPAwEventbookingEventAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeCreatePurchasedTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeCreatePurchasedTicketsWithId: (NSString*) _id
    data: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Creates a new instance in purchasedTickets of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id
HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; //  (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Creates a new instance in purchasedTickets of this model.
[apiInstance awEventbookingEventPrototypeCreatePurchasedTicketsWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeCreatePurchasedTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeCreateTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeCreateTicketsWithId: (NSString*) _id
    data: (HRPAwEventbookingEventTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Creates a new instance in tickets of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id
HRPAwEventbookingEventTicket* data = [[HRPAwEventbookingEventTicket alloc] init]; //  (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Creates a new instance in tickets of this model.
[apiInstance awEventbookingEventPrototypeCreateTicketsWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeCreateTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 
 **data** | [**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeDeleteAttributes**
```objc
-(NSNumber*) awEventbookingEventPrototypeDeleteAttributesWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all attributes of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Deletes all attributes of this model.
[apiInstance awEventbookingEventPrototypeDeleteAttributesWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeDeleteAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeDeletePurchasedTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeDeletePurchasedTicketsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all purchasedTickets of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Deletes all purchasedTickets of this model.
[apiInstance awEventbookingEventPrototypeDeletePurchasedTicketsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeDeletePurchasedTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeDeleteTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeDeleteTicketsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all tickets of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Deletes all tickets of this model.
[apiInstance awEventbookingEventPrototypeDeleteTicketsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeDeleteTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeDestroyByIdAttributes**
```objc
-(NSNumber*) awEventbookingEventPrototypeDestroyByIdAttributesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for attributes.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for attributes
NSString* _id = @"_id_example"; // AwEventbookingEvent id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Delete a related item by id for attributes.
[apiInstance awEventbookingEventPrototypeDestroyByIdAttributesWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeDestroyByIdAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for attributes | 
 **_id** | **NSString***| AwEventbookingEvent id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeDestroyByIdPurchasedTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeDestroyByIdPurchasedTicketsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for purchasedTickets.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for purchasedTickets
NSString* _id = @"_id_example"; // AwEventbookingEvent id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Delete a related item by id for purchasedTickets.
[apiInstance awEventbookingEventPrototypeDestroyByIdPurchasedTicketsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeDestroyByIdPurchasedTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for purchasedTickets | 
 **_id** | **NSString***| AwEventbookingEvent id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeDestroyByIdTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeDestroyByIdTicketsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for tickets.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for tickets
NSString* _id = @"_id_example"; // AwEventbookingEvent id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Delete a related item by id for tickets.
[apiInstance awEventbookingEventPrototypeDestroyByIdTicketsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeDestroyByIdTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for tickets | 
 **_id** | **NSString***| AwEventbookingEvent id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeExistsPurchasedTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeExistsPurchasedTicketsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSNumber* output, NSError* error)) handler;
```

Check the existence of purchasedTickets relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for purchasedTickets
NSString* _id = @"_id_example"; // AwEventbookingEvent id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Check the existence of purchasedTickets relation to an item by id.
[apiInstance awEventbookingEventPrototypeExistsPurchasedTicketsWithFk:fk
              _id:_id
          completionHandler: ^(NSNumber* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeExistsPurchasedTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for purchasedTickets | 
 **_id** | **NSString***| AwEventbookingEvent id | 

### Return type

**NSNumber***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeFindByIdAttributes**
```objc
-(NSNumber*) awEventbookingEventPrototypeFindByIdAttributesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPAwEventbookingEventAttribute* output, NSError* error)) handler;
```

Find a related item by id for attributes.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for attributes
NSString* _id = @"_id_example"; // AwEventbookingEvent id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Find a related item by id for attributes.
[apiInstance awEventbookingEventPrototypeFindByIdAttributesWithFk:fk
              _id:_id
          completionHandler: ^(HRPAwEventbookingEventAttribute* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeFindByIdAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for attributes | 
 **_id** | **NSString***| AwEventbookingEvent id | 

### Return type

[**HRPAwEventbookingEventAttribute***](HRPAwEventbookingEventAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeFindByIdPurchasedTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeFindByIdPurchasedTicketsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Find a related item by id for purchasedTickets.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for purchasedTickets
NSString* _id = @"_id_example"; // AwEventbookingEvent id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Find a related item by id for purchasedTickets.
[apiInstance awEventbookingEventPrototypeFindByIdPurchasedTicketsWithFk:fk
              _id:_id
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeFindByIdPurchasedTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for purchasedTickets | 
 **_id** | **NSString***| AwEventbookingEvent id | 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeFindByIdTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeFindByIdTicketsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Find a related item by id for tickets.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for tickets
NSString* _id = @"_id_example"; // AwEventbookingEvent id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Find a related item by id for tickets.
[apiInstance awEventbookingEventPrototypeFindByIdTicketsWithFk:fk
              _id:_id
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeFindByIdTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for tickets | 
 **_id** | **NSString***| AwEventbookingEvent id | 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeGetAttributes**
```objc
-(NSNumber*) awEventbookingEventPrototypeGetAttributesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwEventbookingEventAttribute>* output, NSError* error)) handler;
```

Queries attributes of AwEventbookingEvent.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id
NSString* filter = @"filter_example"; //  (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Queries attributes of AwEventbookingEvent.
[apiInstance awEventbookingEventPrototypeGetAttributesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPAwEventbookingEventAttribute>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeGetAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPAwEventbookingEventAttribute>***](HRPAwEventbookingEventAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeGetPurchasedTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeGetPurchasedTicketsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwEventbookingTicket>* output, NSError* error)) handler;
```

Queries purchasedTickets of AwEventbookingEvent.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id
NSString* filter = @"filter_example"; //  (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Queries purchasedTickets of AwEventbookingEvent.
[apiInstance awEventbookingEventPrototypeGetPurchasedTicketsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPAwEventbookingTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeGetPurchasedTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPAwEventbookingTicket>***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeGetTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeGetTicketsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwEventbookingEventTicket>* output, NSError* error)) handler;
```

Queries tickets of AwEventbookingEvent.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id
NSString* filter = @"filter_example"; //  (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Queries tickets of AwEventbookingEvent.
[apiInstance awEventbookingEventPrototypeGetTicketsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPAwEventbookingEventTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeGetTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPAwEventbookingEventTicket>***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeLinkPurchasedTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeLinkPurchasedTicketsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPAwEventbookingEventTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Add a related item by id for purchasedTickets.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for purchasedTickets
NSString* _id = @"_id_example"; // AwEventbookingEvent id
HRPAwEventbookingEventTicket* data = [[HRPAwEventbookingEventTicket alloc] init]; //  (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Add a related item by id for purchasedTickets.
[apiInstance awEventbookingEventPrototypeLinkPurchasedTicketsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeLinkPurchasedTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for purchasedTickets | 
 **_id** | **NSString***| AwEventbookingEvent id | 
 **data** | [**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeUnlinkPurchasedTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeUnlinkPurchasedTicketsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Remove the purchasedTickets relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for purchasedTickets
NSString* _id = @"_id_example"; // AwEventbookingEvent id

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Remove the purchasedTickets relation to an item by id.
[apiInstance awEventbookingEventPrototypeUnlinkPurchasedTicketsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeUnlinkPurchasedTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for purchasedTickets | 
 **_id** | **NSString***| AwEventbookingEvent id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid**
```objc
-(NSNumber*) awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsidWithId: (NSString*) _id
    data: (HRPAwEventbookingEvent*) data
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id
HRPAwEventbookingEvent* data = [[HRPAwEventbookingEvent alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsidWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 
 **data** | [**HRPAwEventbookingEvent***](HRPAwEventbookingEvent*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid**
```objc
-(NSNumber*) awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsidWithId: (NSString*) _id
    data: (HRPAwEventbookingEvent*) data
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingEvent id
HRPAwEventbookingEvent* data = [[HRPAwEventbookingEvent alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsidWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingEvent id | 
 **data** | [**HRPAwEventbookingEvent***](HRPAwEventbookingEvent*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeUpdateByIdAttributes**
```objc
-(NSNumber*) awEventbookingEventPrototypeUpdateByIdAttributesWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPAwEventbookingEventAttribute*) data
        completionHandler: (void (^)(HRPAwEventbookingEventAttribute* output, NSError* error)) handler;
```

Update a related item by id for attributes.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for attributes
NSString* _id = @"_id_example"; // AwEventbookingEvent id
HRPAwEventbookingEventAttribute* data = [[HRPAwEventbookingEventAttribute alloc] init]; //  (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Update a related item by id for attributes.
[apiInstance awEventbookingEventPrototypeUpdateByIdAttributesWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEventAttribute* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeUpdateByIdAttributes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for attributes | 
 **_id** | **NSString***| AwEventbookingEvent id | 
 **data** | [**HRPAwEventbookingEventAttribute***](HRPAwEventbookingEventAttribute*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingEventAttribute***](HRPAwEventbookingEventAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeUpdateByIdPurchasedTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeUpdateByIdPurchasedTicketsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Update a related item by id for purchasedTickets.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for purchasedTickets
NSString* _id = @"_id_example"; // AwEventbookingEvent id
HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; //  (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Update a related item by id for purchasedTickets.
[apiInstance awEventbookingEventPrototypeUpdateByIdPurchasedTicketsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeUpdateByIdPurchasedTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for purchasedTickets | 
 **_id** | **NSString***| AwEventbookingEvent id | 
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventPrototypeUpdateByIdTickets**
```objc
-(NSNumber*) awEventbookingEventPrototypeUpdateByIdTicketsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPAwEventbookingEventTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingEventTicket* output, NSError* error)) handler;
```

Update a related item by id for tickets.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for tickets
NSString* _id = @"_id_example"; // AwEventbookingEvent id
HRPAwEventbookingEventTicket* data = [[HRPAwEventbookingEventTicket alloc] init]; //  (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Update a related item by id for tickets.
[apiInstance awEventbookingEventPrototypeUpdateByIdTicketsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEventTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventPrototypeUpdateByIdTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for tickets | 
 **_id** | **NSString***| AwEventbookingEvent id | 
 **data** | [**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingEventTicket***](HRPAwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventReplaceById**
```objc
-(NSNumber*) awEventbookingEventReplaceByIdWithId: (NSString*) _id
    data: (HRPAwEventbookingEvent*) data
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPAwEventbookingEvent* data = [[HRPAwEventbookingEvent alloc] init]; // Model instance data (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance awEventbookingEventReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPAwEventbookingEvent***](HRPAwEventbookingEvent*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventReplaceOrCreate**
```objc
-(NSNumber*) awEventbookingEventReplaceOrCreateWithData: (HRPAwEventbookingEvent*) data
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingEvent* data = [[HRPAwEventbookingEvent alloc] init]; // Model instance data (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance awEventbookingEventReplaceOrCreateWithData:data
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingEvent***](HRPAwEventbookingEvent*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventUpdateAll**
```objc
-(NSNumber*) awEventbookingEventUpdateAllWithWhere: (NSString*) where
    data: (HRPAwEventbookingEvent*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwEventbookingEvent* data = [[HRPAwEventbookingEvent alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance awEventbookingEventUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwEventbookingEvent***](HRPAwEventbookingEvent*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventUpsertPatchAwEventbookingEvents**
```objc
-(NSNumber*) awEventbookingEventUpsertPatchAwEventbookingEventsWithData: (HRPAwEventbookingEvent*) data
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingEvent* data = [[HRPAwEventbookingEvent alloc] init]; // Model instance data (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awEventbookingEventUpsertPatchAwEventbookingEventsWithData:data
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventUpsertPatchAwEventbookingEvents: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingEvent***](HRPAwEventbookingEvent*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventUpsertPutAwEventbookingEvents**
```objc
-(NSNumber*) awEventbookingEventUpsertPutAwEventbookingEventsWithData: (HRPAwEventbookingEvent*) data
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingEvent* data = [[HRPAwEventbookingEvent alloc] init]; // Model instance data (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awEventbookingEventUpsertPutAwEventbookingEventsWithData:data
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventUpsertPutAwEventbookingEvents: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingEvent***](HRPAwEventbookingEvent*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingEventUpsertWithWhere**
```objc
-(NSNumber*) awEventbookingEventUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPAwEventbookingEvent*) data
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwEventbookingEvent* data = [[HRPAwEventbookingEvent alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingEventApi*apiInstance = [[HRPAwEventbookingEventApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance awEventbookingEventUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingEventApi->awEventbookingEventUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwEventbookingEvent***](HRPAwEventbookingEvent*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

