# HRPFollowing

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **NSString*** |  | 
**firstName** | **NSString*** |  | 
**lastName** | **NSString*** |  | 
**dob** | **NSString*** |  | [optional] 
**gender** | **NSString*** |  | [optional] [default to @"other"]
**password** | **NSString*** |  | 
**profilePicture** | **NSString*** |  | [optional] 
**cover** | **NSString*** |  | [optional] 
**profilePictureUpload** | [**HRPMagentoImageUpload***](HRPMagentoImageUpload.md) |  | [optional] 
**coverUpload** | [**HRPMagentoImageUpload***](HRPMagentoImageUpload.md) |  | [optional] 
**metadata** | **NSObject*** |  | [optional] 
**connection** | [**HRPCustomerConnection***](HRPCustomerConnection.md) |  | [optional] 
**followingCount** | **NSNumber*** |  | [optional] [default to @0.0]
**followerCount** | **NSNumber*** |  | [optional] [default to @0.0]
**isFollowed** | **NSNumber*** |  | [optional] [default to @0]
**isFollower** | **NSNumber*** |  | [optional] [default to @0]
**notificationCount** | **NSNumber*** |  | [optional] [default to @0.0]
**authorizationCode** | **NSString*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


