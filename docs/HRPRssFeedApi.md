# HRPRssFeedApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rssFeedCount**](HRPRssFeedApi.md#rssfeedcount) | **GET** /RssFeeds/count | Count instances of the model matched by where from the data source.
[**rssFeedCreate**](HRPRssFeedApi.md#rssfeedcreate) | **POST** /RssFeeds | Create a new instance of the model and persist it into the data source.
[**rssFeedCreateChangeStreamGetRssFeedsChangeStream**](HRPRssFeedApi.md#rssfeedcreatechangestreamgetrssfeedschangestream) | **GET** /RssFeeds/change-stream | Create a change stream.
[**rssFeedCreateChangeStreamPostRssFeedsChangeStream**](HRPRssFeedApi.md#rssfeedcreatechangestreampostrssfeedschangestream) | **POST** /RssFeeds/change-stream | Create a change stream.
[**rssFeedDeleteById**](HRPRssFeedApi.md#rssfeeddeletebyid) | **DELETE** /RssFeeds/{id} | Delete a model instance by {{id}} from the data source.
[**rssFeedExistsGetRssFeedsidExists**](HRPRssFeedApi.md#rssfeedexistsgetrssfeedsidexists) | **GET** /RssFeeds/{id}/exists | Check whether a model instance exists in the data source.
[**rssFeedExistsHeadRssFeedsid**](HRPRssFeedApi.md#rssfeedexistsheadrssfeedsid) | **HEAD** /RssFeeds/{id} | Check whether a model instance exists in the data source.
[**rssFeedFind**](HRPRssFeedApi.md#rssfeedfind) | **GET** /RssFeeds | Find all instances of the model matched by filter from the data source.
[**rssFeedFindById**](HRPRssFeedApi.md#rssfeedfindbyid) | **GET** /RssFeeds/{id} | Find a model instance by {{id}} from the data source.
[**rssFeedFindOne**](HRPRssFeedApi.md#rssfeedfindone) | **GET** /RssFeeds/findOne | Find first instance of the model matched by filter from the data source.
[**rssFeedPrototypeGetRssFeedGroup**](HRPRssFeedApi.md#rssfeedprototypegetrssfeedgroup) | **GET** /RssFeeds/{id}/rssFeedGroup | Fetches belongsTo relation rssFeedGroup.
[**rssFeedPrototypeUpdateAttributesPatchRssFeedsid**](HRPRssFeedApi.md#rssfeedprototypeupdateattributespatchrssfeedsid) | **PATCH** /RssFeeds/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedPrototypeUpdateAttributesPutRssFeedsid**](HRPRssFeedApi.md#rssfeedprototypeupdateattributesputrssfeedsid) | **PUT** /RssFeeds/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedReplaceById**](HRPRssFeedApi.md#rssfeedreplacebyid) | **POST** /RssFeeds/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**rssFeedReplaceOrCreate**](HRPRssFeedApi.md#rssfeedreplaceorcreate) | **POST** /RssFeeds/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**rssFeedUpdateAll**](HRPRssFeedApi.md#rssfeedupdateall) | **POST** /RssFeeds/update | Update instances of the model matched by {{where}} from the data source.
[**rssFeedUpsertPatchRssFeeds**](HRPRssFeedApi.md#rssfeedupsertpatchrssfeeds) | **PATCH** /RssFeeds | Patch an existing model instance or insert a new one into the data source.
[**rssFeedUpsertPutRssFeeds**](HRPRssFeedApi.md#rssfeedupsertputrssfeeds) | **PUT** /RssFeeds | Patch an existing model instance or insert a new one into the data source.
[**rssFeedUpsertWithWhere**](HRPRssFeedApi.md#rssfeedupsertwithwhere) | **POST** /RssFeeds/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **rssFeedCount**
```objc
-(NSNumber*) rssFeedCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance rssFeedCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedCreate**
```objc
-(NSNumber*) rssFeedCreateWithData: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRssFeed* data = [[HRPRssFeed alloc] init]; // Model instance data (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance rssFeedCreateWithData:data
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)| Model instance data | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedCreateChangeStreamGetRssFeedsChangeStream**
```objc
-(NSNumber*) rssFeedCreateChangeStreamGetRssFeedsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Create a change stream.
[apiInstance rssFeedCreateChangeStreamGetRssFeedsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedCreateChangeStreamGetRssFeedsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedCreateChangeStreamPostRssFeedsChangeStream**
```objc
-(NSNumber*) rssFeedCreateChangeStreamPostRssFeedsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Create a change stream.
[apiInstance rssFeedCreateChangeStreamPostRssFeedsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedCreateChangeStreamPostRssFeedsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedDeleteById**
```objc
-(NSNumber*) rssFeedDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance rssFeedDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedExistsGetRssFeedsidExists**
```objc
-(NSNumber*) rssFeedExistsGetRssFeedsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance rssFeedExistsGetRssFeedsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedExistsGetRssFeedsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedExistsHeadRssFeedsid**
```objc
-(NSNumber*) rssFeedExistsHeadRssFeedsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance rssFeedExistsHeadRssFeedsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedExistsHeadRssFeedsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedFind**
```objc
-(NSNumber*) rssFeedFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRssFeed>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance rssFeedFindWithFilter:filter
          completionHandler: ^(NSArray<HRPRssFeed>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPRssFeed>***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedFindById**
```objc
-(NSNumber*) rssFeedFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance rssFeedFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedFindOne**
```objc
-(NSNumber*) rssFeedFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance rssFeedFindOneWithFilter:filter
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedPrototypeGetRssFeedGroup**
```objc
-(NSNumber*) rssFeedPrototypeGetRssFeedGroupWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Fetches belongsTo relation rssFeedGroup.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RssFeed id
NSNumber* refresh = @true; //  (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Fetches belongsTo relation rssFeedGroup.
[apiInstance rssFeedPrototypeGetRssFeedGroupWithId:_id
              refresh:refresh
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedPrototypeGetRssFeedGroup: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RssFeed id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedPrototypeUpdateAttributesPatchRssFeedsid**
```objc
-(NSNumber*) rssFeedPrototypeUpdateAttributesPatchRssFeedsidWithId: (NSString*) _id
    data: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RssFeed id
HRPRssFeed* data = [[HRPRssFeed alloc] init]; // An object of model property name/value pairs (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance rssFeedPrototypeUpdateAttributesPatchRssFeedsidWithId:_id
              data:data
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedPrototypeUpdateAttributesPatchRssFeedsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RssFeed id | 
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedPrototypeUpdateAttributesPutRssFeedsid**
```objc
-(NSNumber*) rssFeedPrototypeUpdateAttributesPutRssFeedsidWithId: (NSString*) _id
    data: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RssFeed id
HRPRssFeed* data = [[HRPRssFeed alloc] init]; // An object of model property name/value pairs (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance rssFeedPrototypeUpdateAttributesPutRssFeedsidWithId:_id
              data:data
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedPrototypeUpdateAttributesPutRssFeedsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RssFeed id | 
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedReplaceById**
```objc
-(NSNumber*) rssFeedReplaceByIdWithId: (NSString*) _id
    data: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPRssFeed* data = [[HRPRssFeed alloc] init]; // Model instance data (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance rssFeedReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)| Model instance data | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedReplaceOrCreate**
```objc
-(NSNumber*) rssFeedReplaceOrCreateWithData: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRssFeed* data = [[HRPRssFeed alloc] init]; // Model instance data (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance rssFeedReplaceOrCreateWithData:data
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)| Model instance data | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedUpdateAll**
```objc
-(NSNumber*) rssFeedUpdateAllWithWhere: (NSString*) where
    data: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRssFeed* data = [[HRPRssFeed alloc] init]; // An object of model property name/value pairs (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance rssFeedUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedUpsertPatchRssFeeds**
```objc
-(NSNumber*) rssFeedUpsertPatchRssFeedsWithData: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRssFeed* data = [[HRPRssFeed alloc] init]; // Model instance data (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance rssFeedUpsertPatchRssFeedsWithData:data
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedUpsertPatchRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)| Model instance data | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedUpsertPutRssFeeds**
```objc
-(NSNumber*) rssFeedUpsertPutRssFeedsWithData: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRssFeed* data = [[HRPRssFeed alloc] init]; // Model instance data (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance rssFeedUpsertPutRssFeedsWithData:data
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedUpsertPutRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)| Model instance data | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedUpsertWithWhere**
```objc
-(NSNumber*) rssFeedUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRssFeed* data = [[HRPRssFeed alloc] init]; // An object of model property name/value pairs (optional)

HRPRssFeedApi*apiInstance = [[HRPRssFeedApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance rssFeedUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedApi->rssFeedUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

