# HRPProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | [optional] 
**_description** | **NSString*** |  | [optional] 
**cover** | **NSString*** |  | [optional] 
**price** | **NSNumber*** |  | [optional] [default to @0.0]
**baseCurrency** | **NSString*** |  | [optional] [default to @"EUR"]
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


