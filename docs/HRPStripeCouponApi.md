# HRPStripeCouponApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeCouponCount**](HRPStripeCouponApi.md#stripecouponcount) | **GET** /StripeCoupons/count | Count instances of the model matched by where from the data source.
[**stripeCouponCreate**](HRPStripeCouponApi.md#stripecouponcreate) | **POST** /StripeCoupons | Create a new instance of the model and persist it into the data source.
[**stripeCouponCreateChangeStreamGetStripeCouponsChangeStream**](HRPStripeCouponApi.md#stripecouponcreatechangestreamgetstripecouponschangestream) | **GET** /StripeCoupons/change-stream | Create a change stream.
[**stripeCouponCreateChangeStreamPostStripeCouponsChangeStream**](HRPStripeCouponApi.md#stripecouponcreatechangestreampoststripecouponschangestream) | **POST** /StripeCoupons/change-stream | Create a change stream.
[**stripeCouponDeleteById**](HRPStripeCouponApi.md#stripecoupondeletebyid) | **DELETE** /StripeCoupons/{id} | Delete a model instance by {{id}} from the data source.
[**stripeCouponExistsGetStripeCouponsidExists**](HRPStripeCouponApi.md#stripecouponexistsgetstripecouponsidexists) | **GET** /StripeCoupons/{id}/exists | Check whether a model instance exists in the data source.
[**stripeCouponExistsHeadStripeCouponsid**](HRPStripeCouponApi.md#stripecouponexistsheadstripecouponsid) | **HEAD** /StripeCoupons/{id} | Check whether a model instance exists in the data source.
[**stripeCouponFind**](HRPStripeCouponApi.md#stripecouponfind) | **GET** /StripeCoupons | Find all instances of the model matched by filter from the data source.
[**stripeCouponFindById**](HRPStripeCouponApi.md#stripecouponfindbyid) | **GET** /StripeCoupons/{id} | Find a model instance by {{id}} from the data source.
[**stripeCouponFindOne**](HRPStripeCouponApi.md#stripecouponfindone) | **GET** /StripeCoupons/findOne | Find first instance of the model matched by filter from the data source.
[**stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid**](HRPStripeCouponApi.md#stripecouponprototypeupdateattributespatchstripecouponsid) | **PATCH** /StripeCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeCouponPrototypeUpdateAttributesPutStripeCouponsid**](HRPStripeCouponApi.md#stripecouponprototypeupdateattributesputstripecouponsid) | **PUT** /StripeCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeCouponReplaceById**](HRPStripeCouponApi.md#stripecouponreplacebyid) | **POST** /StripeCoupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeCouponReplaceOrCreate**](HRPStripeCouponApi.md#stripecouponreplaceorcreate) | **POST** /StripeCoupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeCouponUpdateAll**](HRPStripeCouponApi.md#stripecouponupdateall) | **POST** /StripeCoupons/update | Update instances of the model matched by {{where}} from the data source.
[**stripeCouponUpsertPatchStripeCoupons**](HRPStripeCouponApi.md#stripecouponupsertpatchstripecoupons) | **PATCH** /StripeCoupons | Patch an existing model instance or insert a new one into the data source.
[**stripeCouponUpsertPutStripeCoupons**](HRPStripeCouponApi.md#stripecouponupsertputstripecoupons) | **PUT** /StripeCoupons | Patch an existing model instance or insert a new one into the data source.
[**stripeCouponUpsertWithWhere**](HRPStripeCouponApi.md#stripecouponupsertwithwhere) | **POST** /StripeCoupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **stripeCouponCount**
```objc
-(NSNumber*) stripeCouponCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance stripeCouponCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponCreate**
```objc
-(NSNumber*) stripeCouponCreateWithData: (HRPStripeCoupon*) data
        completionHandler: (void (^)(HRPStripeCoupon* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeCoupon* data = [[HRPStripeCoupon alloc] init]; // Model instance data (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance stripeCouponCreateWithData:data
          completionHandler: ^(HRPStripeCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeCoupon***](HRPStripeCoupon*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeCoupon***](HRPStripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponCreateChangeStreamGetStripeCouponsChangeStream**
```objc
-(NSNumber*) stripeCouponCreateChangeStreamGetStripeCouponsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Create a change stream.
[apiInstance stripeCouponCreateChangeStreamGetStripeCouponsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponCreateChangeStreamGetStripeCouponsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponCreateChangeStreamPostStripeCouponsChangeStream**
```objc
-(NSNumber*) stripeCouponCreateChangeStreamPostStripeCouponsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Create a change stream.
[apiInstance stripeCouponCreateChangeStreamPostStripeCouponsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponCreateChangeStreamPostStripeCouponsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponDeleteById**
```objc
-(NSNumber*) stripeCouponDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance stripeCouponDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponExistsGetStripeCouponsidExists**
```objc
-(NSNumber*) stripeCouponExistsGetStripeCouponsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance stripeCouponExistsGetStripeCouponsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponExistsGetStripeCouponsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponExistsHeadStripeCouponsid**
```objc
-(NSNumber*) stripeCouponExistsHeadStripeCouponsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance stripeCouponExistsHeadStripeCouponsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponExistsHeadStripeCouponsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponFind**
```objc
-(NSNumber*) stripeCouponFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPStripeCoupon>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance stripeCouponFindWithFilter:filter
          completionHandler: ^(NSArray<HRPStripeCoupon>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPStripeCoupon>***](HRPStripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponFindById**
```objc
-(NSNumber*) stripeCouponFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPStripeCoupon* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance stripeCouponFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPStripeCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPStripeCoupon***](HRPStripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponFindOne**
```objc
-(NSNumber*) stripeCouponFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPStripeCoupon* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance stripeCouponFindOneWithFilter:filter
          completionHandler: ^(HRPStripeCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPStripeCoupon***](HRPStripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid**
```objc
-(NSNumber*) stripeCouponPrototypeUpdateAttributesPatchStripeCouponsidWithId: (NSString*) _id
    data: (HRPStripeCoupon*) data
        completionHandler: (void (^)(HRPStripeCoupon* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // StripeCoupon id
HRPStripeCoupon* data = [[HRPStripeCoupon alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance stripeCouponPrototypeUpdateAttributesPatchStripeCouponsidWithId:_id
              data:data
          completionHandler: ^(HRPStripeCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| StripeCoupon id | 
 **data** | [**HRPStripeCoupon***](HRPStripeCoupon*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeCoupon***](HRPStripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponPrototypeUpdateAttributesPutStripeCouponsid**
```objc
-(NSNumber*) stripeCouponPrototypeUpdateAttributesPutStripeCouponsidWithId: (NSString*) _id
    data: (HRPStripeCoupon*) data
        completionHandler: (void (^)(HRPStripeCoupon* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // StripeCoupon id
HRPStripeCoupon* data = [[HRPStripeCoupon alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance stripeCouponPrototypeUpdateAttributesPutStripeCouponsidWithId:_id
              data:data
          completionHandler: ^(HRPStripeCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponPrototypeUpdateAttributesPutStripeCouponsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| StripeCoupon id | 
 **data** | [**HRPStripeCoupon***](HRPStripeCoupon*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeCoupon***](HRPStripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponReplaceById**
```objc
-(NSNumber*) stripeCouponReplaceByIdWithId: (NSString*) _id
    data: (HRPStripeCoupon*) data
        completionHandler: (void (^)(HRPStripeCoupon* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPStripeCoupon* data = [[HRPStripeCoupon alloc] init]; // Model instance data (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance stripeCouponReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPStripeCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPStripeCoupon***](HRPStripeCoupon*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeCoupon***](HRPStripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponReplaceOrCreate**
```objc
-(NSNumber*) stripeCouponReplaceOrCreateWithData: (HRPStripeCoupon*) data
        completionHandler: (void (^)(HRPStripeCoupon* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeCoupon* data = [[HRPStripeCoupon alloc] init]; // Model instance data (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance stripeCouponReplaceOrCreateWithData:data
          completionHandler: ^(HRPStripeCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeCoupon***](HRPStripeCoupon*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeCoupon***](HRPStripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponUpdateAll**
```objc
-(NSNumber*) stripeCouponUpdateAllWithWhere: (NSString*) where
    data: (HRPStripeCoupon*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPStripeCoupon* data = [[HRPStripeCoupon alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance stripeCouponUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPStripeCoupon***](HRPStripeCoupon*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponUpsertPatchStripeCoupons**
```objc
-(NSNumber*) stripeCouponUpsertPatchStripeCouponsWithData: (HRPStripeCoupon*) data
        completionHandler: (void (^)(HRPStripeCoupon* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeCoupon* data = [[HRPStripeCoupon alloc] init]; // Model instance data (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance stripeCouponUpsertPatchStripeCouponsWithData:data
          completionHandler: ^(HRPStripeCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponUpsertPatchStripeCoupons: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeCoupon***](HRPStripeCoupon*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeCoupon***](HRPStripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponUpsertPutStripeCoupons**
```objc
-(NSNumber*) stripeCouponUpsertPutStripeCouponsWithData: (HRPStripeCoupon*) data
        completionHandler: (void (^)(HRPStripeCoupon* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeCoupon* data = [[HRPStripeCoupon alloc] init]; // Model instance data (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance stripeCouponUpsertPutStripeCouponsWithData:data
          completionHandler: ^(HRPStripeCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponUpsertPutStripeCoupons: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeCoupon***](HRPStripeCoupon*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeCoupon***](HRPStripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeCouponUpsertWithWhere**
```objc
-(NSNumber*) stripeCouponUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPStripeCoupon*) data
        completionHandler: (void (^)(HRPStripeCoupon* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPStripeCoupon* data = [[HRPStripeCoupon alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeCouponApi*apiInstance = [[HRPStripeCouponApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance stripeCouponUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPStripeCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeCouponApi->stripeCouponUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPStripeCoupon***](HRPStripeCoupon*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeCoupon***](HRPStripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

