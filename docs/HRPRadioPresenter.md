# HRPRadioPresenter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | 
**image** | **NSString*** |  | [optional] 
**contact** | [**HRPContact***](HRPContact.md) | Contacts for this presenter | [optional] 
**_id** | **NSNumber*** |  | [optional] 
**radioShows** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


