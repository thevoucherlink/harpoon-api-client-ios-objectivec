# HRPPlayerSource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **NSString*** | Source file | [optional] 
**type** | **NSString*** | Source type (used only in JS SDK) | [optional] 
**label** | **NSString*** | Source label | [optional] 
**_default** | **NSNumber*** | If Source is the default Source | [optional] [default to @0]
**order** | **NSNumber*** | Sort order index | [optional] [default to @0.0]
**_id** | **NSNumber*** |  | [optional] 
**playlistItemId** | **NSNumber*** |  | [optional] 
**playlistItem** | **NSObject*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


