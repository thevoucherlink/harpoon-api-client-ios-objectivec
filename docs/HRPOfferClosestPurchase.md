# HRPOfferClosestPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expiredAt** | **NSDate*** |  | [optional] 
**purchasedAt** | **NSDate*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


