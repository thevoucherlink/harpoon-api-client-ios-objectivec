# HRPHarpoonHpublicVendorconfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**type** | **NSNumber*** |  | 
**category** | **NSString*** |  | 
**logo** | **NSString*** |  | 
**cover** | **NSString*** |  | 
**facebookPageId** | **NSString*** |  | 
**facebookPageToken** | **NSString*** |  | 
**facebookEventEnabled** | **NSNumber*** |  | 
**facebookEventUpdatedAt** | **NSDate*** |  | 
**facebookEventCount** | **NSNumber*** |  | 
**facebookFeedEnabled** | **NSNumber*** |  | 
**facebookFeedUpdatedAt** | **NSDate*** |  | 
**facebookFeedCount** | **NSNumber*** |  | 
**stripePublishableKey** | **NSString*** |  | 
**stripeRefreshToken** | **NSString*** |  | 
**stripeAccessToken** | **NSString*** |  | 
**feesEventTicket** | **NSString*** |  | 
**stripeUserId** | **NSString*** |  | 
**vendorconfigId** | **NSNumber*** |  | 
**createdAt** | **NSDate*** |  | 
**updatedAt** | **NSDate*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


