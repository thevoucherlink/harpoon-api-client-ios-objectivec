# HRPRadioStream

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | 
**_description** | **NSString*** |  | [optional] 
**urlHQ** | **NSString*** |  | 
**urlLQ** | **NSString*** |  | 
**starts** | **NSDate*** | When the Stream starts of being public | [optional] 
**ends** | **NSDate*** | When the Stream ceases of being public | [optional] 
**metaDataUrl** | **NSString*** |  | [optional] 
**imgStream** | **NSString*** |  | [optional] 
**sponsorTrack** | **NSString*** | Url of the sponsor MP3 to be played | [optional] 
**tritonConfig** | [**HRPTritonConfig***](HRPTritonConfig.md) |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 
**appId** | **NSString*** |  | [optional] 
**radioShows** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


