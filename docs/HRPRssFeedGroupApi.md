# HRPRssFeedGroupApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rssFeedGroupCount**](HRPRssFeedGroupApi.md#rssfeedgroupcount) | **GET** /RssFeedGroups/count | Count instances of the model matched by where from the data source.
[**rssFeedGroupCreate**](HRPRssFeedGroupApi.md#rssfeedgroupcreate) | **POST** /RssFeedGroups | Create a new instance of the model and persist it into the data source.
[**rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream**](HRPRssFeedGroupApi.md#rssfeedgroupcreatechangestreamgetrssfeedgroupschangestream) | **GET** /RssFeedGroups/change-stream | Create a change stream.
[**rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream**](HRPRssFeedGroupApi.md#rssfeedgroupcreatechangestreampostrssfeedgroupschangestream) | **POST** /RssFeedGroups/change-stream | Create a change stream.
[**rssFeedGroupDeleteById**](HRPRssFeedGroupApi.md#rssfeedgroupdeletebyid) | **DELETE** /RssFeedGroups/{id} | Delete a model instance by {{id}} from the data source.
[**rssFeedGroupExistsGetRssFeedGroupsidExists**](HRPRssFeedGroupApi.md#rssfeedgroupexistsgetrssfeedgroupsidexists) | **GET** /RssFeedGroups/{id}/exists | Check whether a model instance exists in the data source.
[**rssFeedGroupExistsHeadRssFeedGroupsid**](HRPRssFeedGroupApi.md#rssfeedgroupexistsheadrssfeedgroupsid) | **HEAD** /RssFeedGroups/{id} | Check whether a model instance exists in the data source.
[**rssFeedGroupFind**](HRPRssFeedGroupApi.md#rssfeedgroupfind) | **GET** /RssFeedGroups | Find all instances of the model matched by filter from the data source.
[**rssFeedGroupFindById**](HRPRssFeedGroupApi.md#rssfeedgroupfindbyid) | **GET** /RssFeedGroups/{id} | Find a model instance by {{id}} from the data source.
[**rssFeedGroupFindOne**](HRPRssFeedGroupApi.md#rssfeedgroupfindone) | **GET** /RssFeedGroups/findOne | Find first instance of the model matched by filter from the data source.
[**rssFeedGroupPrototypeCountRssFeeds**](HRPRssFeedGroupApi.md#rssfeedgroupprototypecountrssfeeds) | **GET** /RssFeedGroups/{id}/rssFeeds/count | Counts rssFeeds of RssFeedGroup.
[**rssFeedGroupPrototypeCreateRssFeeds**](HRPRssFeedGroupApi.md#rssfeedgroupprototypecreaterssfeeds) | **POST** /RssFeedGroups/{id}/rssFeeds | Creates a new instance in rssFeeds of this model.
[**rssFeedGroupPrototypeDeleteRssFeeds**](HRPRssFeedGroupApi.md#rssfeedgroupprototypedeleterssfeeds) | **DELETE** /RssFeedGroups/{id}/rssFeeds | Deletes all rssFeeds of this model.
[**rssFeedGroupPrototypeDestroyByIdRssFeeds**](HRPRssFeedGroupApi.md#rssfeedgroupprototypedestroybyidrssfeeds) | **DELETE** /RssFeedGroups/{id}/rssFeeds/{fk} | Delete a related item by id for rssFeeds.
[**rssFeedGroupPrototypeFindByIdRssFeeds**](HRPRssFeedGroupApi.md#rssfeedgroupprototypefindbyidrssfeeds) | **GET** /RssFeedGroups/{id}/rssFeeds/{fk} | Find a related item by id for rssFeeds.
[**rssFeedGroupPrototypeGetRssFeeds**](HRPRssFeedGroupApi.md#rssfeedgroupprototypegetrssfeeds) | **GET** /RssFeedGroups/{id}/rssFeeds | Queries rssFeeds of RssFeedGroup.
[**rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid**](HRPRssFeedGroupApi.md#rssfeedgroupprototypeupdateattributespatchrssfeedgroupsid) | **PATCH** /RssFeedGroups/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid**](HRPRssFeedGroupApi.md#rssfeedgroupprototypeupdateattributesputrssfeedgroupsid) | **PUT** /RssFeedGroups/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedGroupPrototypeUpdateByIdRssFeeds**](HRPRssFeedGroupApi.md#rssfeedgroupprototypeupdatebyidrssfeeds) | **PUT** /RssFeedGroups/{id}/rssFeeds/{fk} | Update a related item by id for rssFeeds.
[**rssFeedGroupReplaceById**](HRPRssFeedGroupApi.md#rssfeedgroupreplacebyid) | **POST** /RssFeedGroups/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**rssFeedGroupReplaceOrCreate**](HRPRssFeedGroupApi.md#rssfeedgroupreplaceorcreate) | **POST** /RssFeedGroups/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**rssFeedGroupUpdateAll**](HRPRssFeedGroupApi.md#rssfeedgroupupdateall) | **POST** /RssFeedGroups/update | Update instances of the model matched by {{where}} from the data source.
[**rssFeedGroupUpsertPatchRssFeedGroups**](HRPRssFeedGroupApi.md#rssfeedgroupupsertpatchrssfeedgroups) | **PATCH** /RssFeedGroups | Patch an existing model instance or insert a new one into the data source.
[**rssFeedGroupUpsertPutRssFeedGroups**](HRPRssFeedGroupApi.md#rssfeedgroupupsertputrssfeedgroups) | **PUT** /RssFeedGroups | Patch an existing model instance or insert a new one into the data source.
[**rssFeedGroupUpsertWithWhere**](HRPRssFeedGroupApi.md#rssfeedgroupupsertwithwhere) | **POST** /RssFeedGroups/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **rssFeedGroupCount**
```objc
-(NSNumber*) rssFeedGroupCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance rssFeedGroupCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupCreate**
```objc
-(NSNumber*) rssFeedGroupCreateWithData: (HRPRssFeedGroup*) data
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRssFeedGroup* data = [[HRPRssFeedGroup alloc] init]; // Model instance data (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance rssFeedGroupCreateWithData:data
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRssFeedGroup***](HRPRssFeedGroup*.md)| Model instance data | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream**
```objc
-(NSNumber*) rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Create a change stream.
[apiInstance rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream**
```objc
-(NSNumber*) rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Create a change stream.
[apiInstance rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupDeleteById**
```objc
-(NSNumber*) rssFeedGroupDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance rssFeedGroupDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupExistsGetRssFeedGroupsidExists**
```objc
-(NSNumber*) rssFeedGroupExistsGetRssFeedGroupsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance rssFeedGroupExistsGetRssFeedGroupsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupExistsGetRssFeedGroupsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupExistsHeadRssFeedGroupsid**
```objc
-(NSNumber*) rssFeedGroupExistsHeadRssFeedGroupsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance rssFeedGroupExistsHeadRssFeedGroupsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupExistsHeadRssFeedGroupsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupFind**
```objc
-(NSNumber*) rssFeedGroupFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRssFeedGroup>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance rssFeedGroupFindWithFilter:filter
          completionHandler: ^(NSArray<HRPRssFeedGroup>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPRssFeedGroup>***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupFindById**
```objc
-(NSNumber*) rssFeedGroupFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance rssFeedGroupFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupFindOne**
```objc
-(NSNumber*) rssFeedGroupFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance rssFeedGroupFindOneWithFilter:filter
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupPrototypeCountRssFeeds**
```objc
-(NSNumber*) rssFeedGroupPrototypeCountRssFeedsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts rssFeeds of RssFeedGroup.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RssFeedGroup id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Counts rssFeeds of RssFeedGroup.
[apiInstance rssFeedGroupPrototypeCountRssFeedsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupPrototypeCountRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RssFeedGroup id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupPrototypeCreateRssFeeds**
```objc
-(NSNumber*) rssFeedGroupPrototypeCreateRssFeedsWithId: (NSString*) _id
    data: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Creates a new instance in rssFeeds of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RssFeedGroup id
HRPRssFeed* data = [[HRPRssFeed alloc] init]; //  (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Creates a new instance in rssFeeds of this model.
[apiInstance rssFeedGroupPrototypeCreateRssFeedsWithId:_id
              data:data
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupPrototypeCreateRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RssFeedGroup id | 
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)|  | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupPrototypeDeleteRssFeeds**
```objc
-(NSNumber*) rssFeedGroupPrototypeDeleteRssFeedsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all rssFeeds of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RssFeedGroup id

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Deletes all rssFeeds of this model.
[apiInstance rssFeedGroupPrototypeDeleteRssFeedsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupPrototypeDeleteRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RssFeedGroup id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupPrototypeDestroyByIdRssFeeds**
```objc
-(NSNumber*) rssFeedGroupPrototypeDestroyByIdRssFeedsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for rssFeeds.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for rssFeeds
NSString* _id = @"_id_example"; // RssFeedGroup id

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Delete a related item by id for rssFeeds.
[apiInstance rssFeedGroupPrototypeDestroyByIdRssFeedsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupPrototypeDestroyByIdRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for rssFeeds | 
 **_id** | **NSString***| RssFeedGroup id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupPrototypeFindByIdRssFeeds**
```objc
-(NSNumber*) rssFeedGroupPrototypeFindByIdRssFeedsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Find a related item by id for rssFeeds.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for rssFeeds
NSString* _id = @"_id_example"; // RssFeedGroup id

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Find a related item by id for rssFeeds.
[apiInstance rssFeedGroupPrototypeFindByIdRssFeedsWithFk:fk
              _id:_id
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupPrototypeFindByIdRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for rssFeeds | 
 **_id** | **NSString***| RssFeedGroup id | 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupPrototypeGetRssFeeds**
```objc
-(NSNumber*) rssFeedGroupPrototypeGetRssFeedsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRssFeed>* output, NSError* error)) handler;
```

Queries rssFeeds of RssFeedGroup.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RssFeedGroup id
NSString* filter = @"filter_example"; //  (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Queries rssFeeds of RssFeedGroup.
[apiInstance rssFeedGroupPrototypeGetRssFeedsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPRssFeed>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupPrototypeGetRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RssFeedGroup id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPRssFeed>***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid**
```objc
-(NSNumber*) rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsidWithId: (NSString*) _id
    data: (HRPRssFeedGroup*) data
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RssFeedGroup id
HRPRssFeedGroup* data = [[HRPRssFeedGroup alloc] init]; // An object of model property name/value pairs (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsidWithId:_id
              data:data
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RssFeedGroup id | 
 **data** | [**HRPRssFeedGroup***](HRPRssFeedGroup*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid**
```objc
-(NSNumber*) rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsidWithId: (NSString*) _id
    data: (HRPRssFeedGroup*) data
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RssFeedGroup id
HRPRssFeedGroup* data = [[HRPRssFeedGroup alloc] init]; // An object of model property name/value pairs (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsidWithId:_id
              data:data
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RssFeedGroup id | 
 **data** | [**HRPRssFeedGroup***](HRPRssFeedGroup*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupPrototypeUpdateByIdRssFeeds**
```objc
-(NSNumber*) rssFeedGroupPrototypeUpdateByIdRssFeedsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Update a related item by id for rssFeeds.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for rssFeeds
NSString* _id = @"_id_example"; // RssFeedGroup id
HRPRssFeed* data = [[HRPRssFeed alloc] init]; //  (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Update a related item by id for rssFeeds.
[apiInstance rssFeedGroupPrototypeUpdateByIdRssFeedsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupPrototypeUpdateByIdRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for rssFeeds | 
 **_id** | **NSString***| RssFeedGroup id | 
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)|  | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupReplaceById**
```objc
-(NSNumber*) rssFeedGroupReplaceByIdWithId: (NSString*) _id
    data: (HRPRssFeedGroup*) data
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPRssFeedGroup* data = [[HRPRssFeedGroup alloc] init]; // Model instance data (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance rssFeedGroupReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPRssFeedGroup***](HRPRssFeedGroup*.md)| Model instance data | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupReplaceOrCreate**
```objc
-(NSNumber*) rssFeedGroupReplaceOrCreateWithData: (HRPRssFeedGroup*) data
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRssFeedGroup* data = [[HRPRssFeedGroup alloc] init]; // Model instance data (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance rssFeedGroupReplaceOrCreateWithData:data
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRssFeedGroup***](HRPRssFeedGroup*.md)| Model instance data | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupUpdateAll**
```objc
-(NSNumber*) rssFeedGroupUpdateAllWithWhere: (NSString*) where
    data: (HRPRssFeedGroup*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRssFeedGroup* data = [[HRPRssFeedGroup alloc] init]; // An object of model property name/value pairs (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance rssFeedGroupUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRssFeedGroup***](HRPRssFeedGroup*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupUpsertPatchRssFeedGroups**
```objc
-(NSNumber*) rssFeedGroupUpsertPatchRssFeedGroupsWithData: (HRPRssFeedGroup*) data
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRssFeedGroup* data = [[HRPRssFeedGroup alloc] init]; // Model instance data (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance rssFeedGroupUpsertPatchRssFeedGroupsWithData:data
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupUpsertPatchRssFeedGroups: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRssFeedGroup***](HRPRssFeedGroup*.md)| Model instance data | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupUpsertPutRssFeedGroups**
```objc
-(NSNumber*) rssFeedGroupUpsertPutRssFeedGroupsWithData: (HRPRssFeedGroup*) data
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRssFeedGroup* data = [[HRPRssFeedGroup alloc] init]; // Model instance data (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance rssFeedGroupUpsertPutRssFeedGroupsWithData:data
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupUpsertPutRssFeedGroups: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRssFeedGroup***](HRPRssFeedGroup*.md)| Model instance data | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rssFeedGroupUpsertWithWhere**
```objc
-(NSNumber*) rssFeedGroupUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPRssFeedGroup*) data
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRssFeedGroup* data = [[HRPRssFeedGroup alloc] init]; // An object of model property name/value pairs (optional)

HRPRssFeedGroupApi*apiInstance = [[HRPRssFeedGroupApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance rssFeedGroupUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRssFeedGroupApi->rssFeedGroupUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRssFeedGroup***](HRPRssFeedGroup*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

