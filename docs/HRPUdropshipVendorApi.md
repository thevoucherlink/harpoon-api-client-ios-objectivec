# HRPUdropshipVendorApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**udropshipVendorCount**](HRPUdropshipVendorApi.md#udropshipvendorcount) | **GET** /UdropshipVendors/count | Count instances of the model matched by where from the data source.
[**udropshipVendorCreate**](HRPUdropshipVendorApi.md#udropshipvendorcreate) | **POST** /UdropshipVendors | Create a new instance of the model and persist it into the data source.
[**udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream**](HRPUdropshipVendorApi.md#udropshipvendorcreatechangestreamgetudropshipvendorschangestream) | **GET** /UdropshipVendors/change-stream | Create a change stream.
[**udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream**](HRPUdropshipVendorApi.md#udropshipvendorcreatechangestreampostudropshipvendorschangestream) | **POST** /UdropshipVendors/change-stream | Create a change stream.
[**udropshipVendorDeleteById**](HRPUdropshipVendorApi.md#udropshipvendordeletebyid) | **DELETE** /UdropshipVendors/{id} | Delete a model instance by {{id}} from the data source.
[**udropshipVendorExistsGetUdropshipVendorsidExists**](HRPUdropshipVendorApi.md#udropshipvendorexistsgetudropshipvendorsidexists) | **GET** /UdropshipVendors/{id}/exists | Check whether a model instance exists in the data source.
[**udropshipVendorExistsHeadUdropshipVendorsid**](HRPUdropshipVendorApi.md#udropshipvendorexistsheadudropshipvendorsid) | **HEAD** /UdropshipVendors/{id} | Check whether a model instance exists in the data source.
[**udropshipVendorFind**](HRPUdropshipVendorApi.md#udropshipvendorfind) | **GET** /UdropshipVendors | Find all instances of the model matched by filter from the data source.
[**udropshipVendorFindById**](HRPUdropshipVendorApi.md#udropshipvendorfindbyid) | **GET** /UdropshipVendors/{id} | Find a model instance by {{id}} from the data source.
[**udropshipVendorFindOne**](HRPUdropshipVendorApi.md#udropshipvendorfindone) | **GET** /UdropshipVendors/findOne | Find first instance of the model matched by filter from the data source.
[**udropshipVendorPrototypeCountCatalogProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypecountcatalogproducts) | **GET** /UdropshipVendors/{id}/catalogProducts/count | Counts catalogProducts of UdropshipVendor.
[**udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypecountharpoonhpublicapplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/count | Counts harpoonHpublicApplicationpartners of UdropshipVendor.
[**udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories**](HRPUdropshipVendorApi.md#udropshipvendorprototypecountharpoonhpublicv12vendorappcategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/count | Counts harpoonHpublicv12VendorAppCategories of UdropshipVendor.
[**udropshipVendorPrototypeCountPartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypecountpartners) | **GET** /UdropshipVendors/{id}/partners/count | Counts partners of UdropshipVendor.
[**udropshipVendorPrototypeCountUdropshipVendorProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypecountudropshipvendorproducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts/count | Counts udropshipVendorProducts of UdropshipVendor.
[**udropshipVendorPrototypeCountVendorLocations**](HRPUdropshipVendorApi.md#udropshipvendorprototypecountvendorlocations) | **GET** /UdropshipVendors/{id}/vendorLocations/count | Counts vendorLocations of UdropshipVendor.
[**udropshipVendorPrototypeCreateCatalogProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypecreatecatalogproducts) | **POST** /UdropshipVendors/{id}/catalogProducts | Creates a new instance in catalogProducts of this model.
[**udropshipVendorPrototypeCreateConfig**](HRPUdropshipVendorApi.md#udropshipvendorprototypecreateconfig) | **POST** /UdropshipVendors/{id}/config | Creates a new instance in config of this model.
[**udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypecreateharpoonhpublicapplicationpartners) | **POST** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Creates a new instance in harpoonHpublicApplicationpartners of this model.
[**udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories**](HRPUdropshipVendorApi.md#udropshipvendorprototypecreateharpoonhpublicv12vendorappcategories) | **POST** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Creates a new instance in harpoonHpublicv12VendorAppCategories of this model.
[**udropshipVendorPrototypeCreatePartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypecreatepartners) | **POST** /UdropshipVendors/{id}/partners | Creates a new instance in partners of this model.
[**udropshipVendorPrototypeCreateUdropshipVendorProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypecreateudropshipvendorproducts) | **POST** /UdropshipVendors/{id}/udropshipVendorProducts | Creates a new instance in udropshipVendorProducts of this model.
[**udropshipVendorPrototypeCreateVendorLocations**](HRPUdropshipVendorApi.md#udropshipvendorprototypecreatevendorlocations) | **POST** /UdropshipVendors/{id}/vendorLocations | Creates a new instance in vendorLocations of this model.
[**udropshipVendorPrototypeDeleteCatalogProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypedeletecatalogproducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts | Deletes all catalogProducts of this model.
[**udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypedeleteharpoonhpublicapplicationpartners) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Deletes all harpoonHpublicApplicationpartners of this model.
[**udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories**](HRPUdropshipVendorApi.md#udropshipvendorprototypedeleteharpoonhpublicv12vendorappcategories) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Deletes all harpoonHpublicv12VendorAppCategories of this model.
[**udropshipVendorPrototypeDeletePartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypedeletepartners) | **DELETE** /UdropshipVendors/{id}/partners | Deletes all partners of this model.
[**udropshipVendorPrototypeDeleteUdropshipVendorProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypedeleteudropshipvendorproducts) | **DELETE** /UdropshipVendors/{id}/udropshipVendorProducts | Deletes all udropshipVendorProducts of this model.
[**udropshipVendorPrototypeDeleteVendorLocations**](HRPUdropshipVendorApi.md#udropshipvendorprototypedeletevendorlocations) | **DELETE** /UdropshipVendors/{id}/vendorLocations | Deletes all vendorLocations of this model.
[**udropshipVendorPrototypeDestroyByIdCatalogProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypedestroybyidcatalogproducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts/{fk} | Delete a related item by id for catalogProducts.
[**udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypedestroybyidharpoonhpublicapplicationpartners) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Delete a related item by id for harpoonHpublicApplicationpartners.
[**udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories**](HRPUdropshipVendorApi.md#udropshipvendorprototypedestroybyidharpoonhpublicv12vendorappcategories) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Delete a related item by id for harpoonHpublicv12VendorAppCategories.
[**udropshipVendorPrototypeDestroyByIdPartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypedestroybyidpartners) | **DELETE** /UdropshipVendors/{id}/partners/{fk} | Delete a related item by id for partners.
[**udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypedestroybyidudropshipvendorproducts) | **DELETE** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Delete a related item by id for udropshipVendorProducts.
[**udropshipVendorPrototypeDestroyByIdVendorLocations**](HRPUdropshipVendorApi.md#udropshipvendorprototypedestroybyidvendorlocations) | **DELETE** /UdropshipVendors/{id}/vendorLocations/{fk} | Delete a related item by id for vendorLocations.
[**udropshipVendorPrototypeDestroyConfig**](HRPUdropshipVendorApi.md#udropshipvendorprototypedestroyconfig) | **DELETE** /UdropshipVendors/{id}/config | Deletes config of this model.
[**udropshipVendorPrototypeExistsCatalogProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypeexistscatalogproducts) | **HEAD** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Check the existence of catalogProducts relation to an item by id.
[**udropshipVendorPrototypeExistsPartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypeexistspartners) | **HEAD** /UdropshipVendors/{id}/partners/rel/{fk} | Check the existence of partners relation to an item by id.
[**udropshipVendorPrototypeFindByIdCatalogProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypefindbyidcatalogproducts) | **GET** /UdropshipVendors/{id}/catalogProducts/{fk} | Find a related item by id for catalogProducts.
[**udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypefindbyidharpoonhpublicapplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Find a related item by id for harpoonHpublicApplicationpartners.
[**udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories**](HRPUdropshipVendorApi.md#udropshipvendorprototypefindbyidharpoonhpublicv12vendorappcategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Find a related item by id for harpoonHpublicv12VendorAppCategories.
[**udropshipVendorPrototypeFindByIdPartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypefindbyidpartners) | **GET** /UdropshipVendors/{id}/partners/{fk} | Find a related item by id for partners.
[**udropshipVendorPrototypeFindByIdUdropshipVendorProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypefindbyidudropshipvendorproducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Find a related item by id for udropshipVendorProducts.
[**udropshipVendorPrototypeFindByIdVendorLocations**](HRPUdropshipVendorApi.md#udropshipvendorprototypefindbyidvendorlocations) | **GET** /UdropshipVendors/{id}/vendorLocations/{fk} | Find a related item by id for vendorLocations.
[**udropshipVendorPrototypeGetCatalogProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypegetcatalogproducts) | **GET** /UdropshipVendors/{id}/catalogProducts | Queries catalogProducts of UdropshipVendor.
[**udropshipVendorPrototypeGetConfig**](HRPUdropshipVendorApi.md#udropshipvendorprototypegetconfig) | **GET** /UdropshipVendors/{id}/config | Fetches hasOne relation config.
[**udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypegetharpoonhpublicapplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Queries harpoonHpublicApplicationpartners of UdropshipVendor.
[**udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories**](HRPUdropshipVendorApi.md#udropshipvendorprototypegetharpoonhpublicv12vendorappcategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Queries harpoonHpublicv12VendorAppCategories of UdropshipVendor.
[**udropshipVendorPrototypeGetPartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypegetpartners) | **GET** /UdropshipVendors/{id}/partners | Queries partners of UdropshipVendor.
[**udropshipVendorPrototypeGetUdropshipVendorProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypegetudropshipvendorproducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts | Queries udropshipVendorProducts of UdropshipVendor.
[**udropshipVendorPrototypeGetVendorLocations**](HRPUdropshipVendorApi.md#udropshipvendorprototypegetvendorlocations) | **GET** /UdropshipVendors/{id}/vendorLocations | Queries vendorLocations of UdropshipVendor.
[**udropshipVendorPrototypeLinkCatalogProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypelinkcatalogproducts) | **PUT** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Add a related item by id for catalogProducts.
[**udropshipVendorPrototypeLinkPartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypelinkpartners) | **PUT** /UdropshipVendors/{id}/partners/rel/{fk} | Add a related item by id for partners.
[**udropshipVendorPrototypeUnlinkCatalogProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypeunlinkcatalogproducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Remove the catalogProducts relation to an item by id.
[**udropshipVendorPrototypeUnlinkPartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypeunlinkpartners) | **DELETE** /UdropshipVendors/{id}/partners/rel/{fk} | Remove the partners relation to an item by id.
[**udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid**](HRPUdropshipVendorApi.md#udropshipvendorprototypeupdateattributespatchudropshipvendorsid) | **PATCH** /UdropshipVendors/{id} | Patch attributes for a model instance and persist it into the data source.
[**udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid**](HRPUdropshipVendorApi.md#udropshipvendorprototypeupdateattributesputudropshipvendorsid) | **PUT** /UdropshipVendors/{id} | Patch attributes for a model instance and persist it into the data source.
[**udropshipVendorPrototypeUpdateByIdCatalogProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypeupdatebyidcatalogproducts) | **PUT** /UdropshipVendors/{id}/catalogProducts/{fk} | Update a related item by id for catalogProducts.
[**udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypeupdatebyidharpoonhpublicapplicationpartners) | **PUT** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Update a related item by id for harpoonHpublicApplicationpartners.
[**udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories**](HRPUdropshipVendorApi.md#udropshipvendorprototypeupdatebyidharpoonhpublicv12vendorappcategories) | **PUT** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Update a related item by id for harpoonHpublicv12VendorAppCategories.
[**udropshipVendorPrototypeUpdateByIdPartners**](HRPUdropshipVendorApi.md#udropshipvendorprototypeupdatebyidpartners) | **PUT** /UdropshipVendors/{id}/partners/{fk} | Update a related item by id for partners.
[**udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts**](HRPUdropshipVendorApi.md#udropshipvendorprototypeupdatebyidudropshipvendorproducts) | **PUT** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Update a related item by id for udropshipVendorProducts.
[**udropshipVendorPrototypeUpdateByIdVendorLocations**](HRPUdropshipVendorApi.md#udropshipvendorprototypeupdatebyidvendorlocations) | **PUT** /UdropshipVendors/{id}/vendorLocations/{fk} | Update a related item by id for vendorLocations.
[**udropshipVendorPrototypeUpdateConfig**](HRPUdropshipVendorApi.md#udropshipvendorprototypeupdateconfig) | **PUT** /UdropshipVendors/{id}/config | Update config of this model.
[**udropshipVendorReplaceById**](HRPUdropshipVendorApi.md#udropshipvendorreplacebyid) | **POST** /UdropshipVendors/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**udropshipVendorReplaceOrCreate**](HRPUdropshipVendorApi.md#udropshipvendorreplaceorcreate) | **POST** /UdropshipVendors/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**udropshipVendorUpdateAll**](HRPUdropshipVendorApi.md#udropshipvendorupdateall) | **POST** /UdropshipVendors/update | Update instances of the model matched by {{where}} from the data source.
[**udropshipVendorUpsertPatchUdropshipVendors**](HRPUdropshipVendorApi.md#udropshipvendorupsertpatchudropshipvendors) | **PATCH** /UdropshipVendors | Patch an existing model instance or insert a new one into the data source.
[**udropshipVendorUpsertPutUdropshipVendors**](HRPUdropshipVendorApi.md#udropshipvendorupsertputudropshipvendors) | **PUT** /UdropshipVendors | Patch an existing model instance or insert a new one into the data source.
[**udropshipVendorUpsertWithWhere**](HRPUdropshipVendorApi.md#udropshipvendorupsertwithwhere) | **POST** /UdropshipVendors/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **udropshipVendorCount**
```objc
-(NSNumber*) udropshipVendorCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance udropshipVendorCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorCreate**
```objc
-(NSNumber*) udropshipVendorCreateWithData: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; // Model instance data (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance udropshipVendorCreateWithData:data
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)| Model instance data | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream**
```objc
-(NSNumber*) udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Create a change stream.
[apiInstance udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream**
```objc
-(NSNumber*) udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Create a change stream.
[apiInstance udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorDeleteById**
```objc
-(NSNumber*) udropshipVendorDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance udropshipVendorDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorExistsGetUdropshipVendorsidExists**
```objc
-(NSNumber*) udropshipVendorExistsGetUdropshipVendorsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance udropshipVendorExistsGetUdropshipVendorsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorExistsGetUdropshipVendorsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorExistsHeadUdropshipVendorsid**
```objc
-(NSNumber*) udropshipVendorExistsHeadUdropshipVendorsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance udropshipVendorExistsHeadUdropshipVendorsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorExistsHeadUdropshipVendorsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorFind**
```objc
-(NSNumber*) udropshipVendorFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPUdropshipVendor>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance udropshipVendorFindWithFilter:filter
          completionHandler: ^(NSArray<HRPUdropshipVendor>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPUdropshipVendor>***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorFindById**
```objc
-(NSNumber*) udropshipVendorFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance udropshipVendorFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorFindOne**
```objc
-(NSNumber*) udropshipVendorFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance udropshipVendorFindOneWithFilter:filter
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCountCatalogProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeCountCatalogProductsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts catalogProducts of UdropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Counts catalogProducts of UdropshipVendor.
[apiInstance udropshipVendorPrototypeCountCatalogProductsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCountCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners**
```objc
-(NSNumber*) udropshipVendorPrototypeCountHarpoonHpublicApplicationpartnersWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts harpoonHpublicApplicationpartners of UdropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Counts harpoonHpublicApplicationpartners of UdropshipVendor.
[apiInstance udropshipVendorPrototypeCountHarpoonHpublicApplicationpartnersWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories**
```objc
-(NSNumber*) udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategoriesWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts harpoonHpublicv12VendorAppCategories of UdropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Counts harpoonHpublicv12VendorAppCategories of UdropshipVendor.
[apiInstance udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategoriesWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCountPartners**
```objc
-(NSNumber*) udropshipVendorPrototypeCountPartnersWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts partners of UdropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Counts partners of UdropshipVendor.
[apiInstance udropshipVendorPrototypeCountPartnersWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCountPartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCountUdropshipVendorProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeCountUdropshipVendorProductsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts udropshipVendorProducts of UdropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Counts udropshipVendorProducts of UdropshipVendor.
[apiInstance udropshipVendorPrototypeCountUdropshipVendorProductsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCountUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCountVendorLocations**
```objc
-(NSNumber*) udropshipVendorPrototypeCountVendorLocationsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts vendorLocations of UdropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Counts vendorLocations of UdropshipVendor.
[apiInstance udropshipVendorPrototypeCountVendorLocationsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCountVendorLocations: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCreateCatalogProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeCreateCatalogProductsWithId: (NSString*) _id
    data: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Creates a new instance in catalogProducts of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Creates a new instance in catalogProducts of this model.
[apiInstance udropshipVendorPrototypeCreateCatalogProductsWithId:_id
              data:data
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCreateCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)|  | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCreateConfig**
```objc
-(NSNumber*) udropshipVendorPrototypeCreateConfigWithId: (NSString*) _id
    data: (HRPHarpoonHpublicVendorconfig*) data
        completionHandler: (void (^)(HRPHarpoonHpublicVendorconfig* output, NSError* error)) handler;
```

Creates a new instance in config of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
HRPHarpoonHpublicVendorconfig* data = [[HRPHarpoonHpublicVendorconfig alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Creates a new instance in config of this model.
[apiInstance udropshipVendorPrototypeCreateConfigWithId:_id
              data:data
          completionHandler: ^(HRPHarpoonHpublicVendorconfig* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCreateConfig: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPHarpoonHpublicVendorconfig***](HRPHarpoonHpublicVendorconfig*.md)|  | [optional] 

### Return type

[**HRPHarpoonHpublicVendorconfig***](HRPHarpoonHpublicVendorconfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners**
```objc
-(NSNumber*) udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartnersWithId: (NSString*) _id
    data: (HRPHarpoonHpublicApplicationpartner*) data
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Creates a new instance in harpoonHpublicApplicationpartners of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
HRPHarpoonHpublicApplicationpartner* data = [[HRPHarpoonHpublicApplicationpartner alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Creates a new instance in harpoonHpublicApplicationpartners of this model.
[apiInstance udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartnersWithId:_id
              data:data
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner*.md)|  | [optional] 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories**
```objc
-(NSNumber*) udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategoriesWithId: (NSString*) _id
    data: (HRPHarpoonHpublicv12VendorAppCategory*) data
        completionHandler: (void (^)(HRPHarpoonHpublicv12VendorAppCategory* output, NSError* error)) handler;
```

Creates a new instance in harpoonHpublicv12VendorAppCategories of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
HRPHarpoonHpublicv12VendorAppCategory* data = [[HRPHarpoonHpublicv12VendorAppCategory alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Creates a new instance in harpoonHpublicv12VendorAppCategories of this model.
[apiInstance udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategoriesWithId:_id
              data:data
          completionHandler: ^(HRPHarpoonHpublicv12VendorAppCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPHarpoonHpublicv12VendorAppCategory***](HRPHarpoonHpublicv12VendorAppCategory*.md)|  | [optional] 

### Return type

[**HRPHarpoonHpublicv12VendorAppCategory***](HRPHarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCreatePartners**
```objc
-(NSNumber*) udropshipVendorPrototypeCreatePartnersWithId: (NSString*) _id
    data: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Creates a new instance in partners of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Creates a new instance in partners of this model.
[apiInstance udropshipVendorPrototypeCreatePartnersWithId:_id
              data:data
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCreatePartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)|  | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCreateUdropshipVendorProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeCreateUdropshipVendorProductsWithId: (NSString*) _id
    data: (HRPUdropshipVendorProduct*) data
        completionHandler: (void (^)(HRPUdropshipVendorProduct* output, NSError* error)) handler;
```

Creates a new instance in udropshipVendorProducts of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
HRPUdropshipVendorProduct* data = [[HRPUdropshipVendorProduct alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Creates a new instance in udropshipVendorProducts of this model.
[apiInstance udropshipVendorPrototypeCreateUdropshipVendorProductsWithId:_id
              data:data
          completionHandler: ^(HRPUdropshipVendorProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCreateUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPUdropshipVendorProduct***](HRPUdropshipVendorProduct*.md)|  | [optional] 

### Return type

[**HRPUdropshipVendorProduct***](HRPUdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeCreateVendorLocations**
```objc
-(NSNumber*) udropshipVendorPrototypeCreateVendorLocationsWithId: (NSString*) _id
    data: (HRPHarpoonHpublicBrandvenue*) data
        completionHandler: (void (^)(HRPHarpoonHpublicBrandvenue* output, NSError* error)) handler;
```

Creates a new instance in vendorLocations of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
HRPHarpoonHpublicBrandvenue* data = [[HRPHarpoonHpublicBrandvenue alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Creates a new instance in vendorLocations of this model.
[apiInstance udropshipVendorPrototypeCreateVendorLocationsWithId:_id
              data:data
          completionHandler: ^(HRPHarpoonHpublicBrandvenue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeCreateVendorLocations: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPHarpoonHpublicBrandvenue***](HRPHarpoonHpublicBrandvenue*.md)|  | [optional] 

### Return type

[**HRPHarpoonHpublicBrandvenue***](HRPHarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDeleteCatalogProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeDeleteCatalogProductsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all catalogProducts of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Deletes all catalogProducts of this model.
[apiInstance udropshipVendorPrototypeDeleteCatalogProductsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDeleteCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners**
```objc
-(NSNumber*) udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartnersWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all harpoonHpublicApplicationpartners of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Deletes all harpoonHpublicApplicationpartners of this model.
[apiInstance udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartnersWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories**
```objc
-(NSNumber*) udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategoriesWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all harpoonHpublicv12VendorAppCategories of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Deletes all harpoonHpublicv12VendorAppCategories of this model.
[apiInstance udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategoriesWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDeletePartners**
```objc
-(NSNumber*) udropshipVendorPrototypeDeletePartnersWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all partners of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Deletes all partners of this model.
[apiInstance udropshipVendorPrototypeDeletePartnersWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDeletePartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDeleteUdropshipVendorProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeDeleteUdropshipVendorProductsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all udropshipVendorProducts of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Deletes all udropshipVendorProducts of this model.
[apiInstance udropshipVendorPrototypeDeleteUdropshipVendorProductsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDeleteUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDeleteVendorLocations**
```objc
-(NSNumber*) udropshipVendorPrototypeDeleteVendorLocationsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all vendorLocations of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Deletes all vendorLocations of this model.
[apiInstance udropshipVendorPrototypeDeleteVendorLocationsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDeleteVendorLocations: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDestroyByIdCatalogProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeDestroyByIdCatalogProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for catalogProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogProducts
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Delete a related item by id for catalogProducts.
[apiInstance udropshipVendorPrototypeDestroyByIdCatalogProductsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDestroyByIdCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogProducts | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners**
```objc
-(NSNumber*) udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartnersWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for harpoonHpublicApplicationpartners.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for harpoonHpublicApplicationpartners
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Delete a related item by id for harpoonHpublicApplicationpartners.
[apiInstance udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartnersWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for harpoonHpublicApplicationpartners | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories**
```objc
-(NSNumber*) udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategoriesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for harpoonHpublicv12VendorAppCategories.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for harpoonHpublicv12VendorAppCategories
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Delete a related item by id for harpoonHpublicv12VendorAppCategories.
[apiInstance udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategoriesWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for harpoonHpublicv12VendorAppCategories | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDestroyByIdPartners**
```objc
-(NSNumber*) udropshipVendorPrototypeDestroyByIdPartnersWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for partners.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for partners
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Delete a related item by id for partners.
[apiInstance udropshipVendorPrototypeDestroyByIdPartnersWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDestroyByIdPartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for partners | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeDestroyByIdUdropshipVendorProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for udropshipVendorProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for udropshipVendorProducts
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Delete a related item by id for udropshipVendorProducts.
[apiInstance udropshipVendorPrototypeDestroyByIdUdropshipVendorProductsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for udropshipVendorProducts | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDestroyByIdVendorLocations**
```objc
-(NSNumber*) udropshipVendorPrototypeDestroyByIdVendorLocationsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for vendorLocations.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for vendorLocations
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Delete a related item by id for vendorLocations.
[apiInstance udropshipVendorPrototypeDestroyByIdVendorLocationsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDestroyByIdVendorLocations: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for vendorLocations | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeDestroyConfig**
```objc
-(NSNumber*) udropshipVendorPrototypeDestroyConfigWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes config of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Deletes config of this model.
[apiInstance udropshipVendorPrototypeDestroyConfigWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeDestroyConfig: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeExistsCatalogProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeExistsCatalogProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSNumber* output, NSError* error)) handler;
```

Check the existence of catalogProducts relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogProducts
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Check the existence of catalogProducts relation to an item by id.
[apiInstance udropshipVendorPrototypeExistsCatalogProductsWithFk:fk
              _id:_id
          completionHandler: ^(NSNumber* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeExistsCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogProducts | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

**NSNumber***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeExistsPartners**
```objc
-(NSNumber*) udropshipVendorPrototypeExistsPartnersWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSNumber* output, NSError* error)) handler;
```

Check the existence of partners relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for partners
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Check the existence of partners relation to an item by id.
[apiInstance udropshipVendorPrototypeExistsPartnersWithFk:fk
              _id:_id
          completionHandler: ^(NSNumber* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeExistsPartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for partners | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

**NSNumber***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeFindByIdCatalogProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeFindByIdCatalogProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Find a related item by id for catalogProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogProducts
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Find a related item by id for catalogProducts.
[apiInstance udropshipVendorPrototypeFindByIdCatalogProductsWithFk:fk
              _id:_id
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeFindByIdCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogProducts | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners**
```objc
-(NSNumber*) udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartnersWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Find a related item by id for harpoonHpublicApplicationpartners.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for harpoonHpublicApplicationpartners
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Find a related item by id for harpoonHpublicApplicationpartners.
[apiInstance udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartnersWithFk:fk
              _id:_id
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for harpoonHpublicApplicationpartners | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories**
```objc
-(NSNumber*) udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategoriesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPHarpoonHpublicv12VendorAppCategory* output, NSError* error)) handler;
```

Find a related item by id for harpoonHpublicv12VendorAppCategories.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for harpoonHpublicv12VendorAppCategories
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Find a related item by id for harpoonHpublicv12VendorAppCategories.
[apiInstance udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategoriesWithFk:fk
              _id:_id
          completionHandler: ^(HRPHarpoonHpublicv12VendorAppCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for harpoonHpublicv12VendorAppCategories | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

[**HRPHarpoonHpublicv12VendorAppCategory***](HRPHarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeFindByIdPartners**
```objc
-(NSNumber*) udropshipVendorPrototypeFindByIdPartnersWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Find a related item by id for partners.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for partners
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Find a related item by id for partners.
[apiInstance udropshipVendorPrototypeFindByIdPartnersWithFk:fk
              _id:_id
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeFindByIdPartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for partners | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeFindByIdUdropshipVendorProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeFindByIdUdropshipVendorProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPUdropshipVendorProduct* output, NSError* error)) handler;
```

Find a related item by id for udropshipVendorProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for udropshipVendorProducts
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Find a related item by id for udropshipVendorProducts.
[apiInstance udropshipVendorPrototypeFindByIdUdropshipVendorProductsWithFk:fk
              _id:_id
          completionHandler: ^(HRPUdropshipVendorProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeFindByIdUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for udropshipVendorProducts | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

[**HRPUdropshipVendorProduct***](HRPUdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeFindByIdVendorLocations**
```objc
-(NSNumber*) udropshipVendorPrototypeFindByIdVendorLocationsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPHarpoonHpublicBrandvenue* output, NSError* error)) handler;
```

Find a related item by id for vendorLocations.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for vendorLocations
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Find a related item by id for vendorLocations.
[apiInstance udropshipVendorPrototypeFindByIdVendorLocationsWithFk:fk
              _id:_id
          completionHandler: ^(HRPHarpoonHpublicBrandvenue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeFindByIdVendorLocations: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for vendorLocations | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

[**HRPHarpoonHpublicBrandvenue***](HRPHarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeGetCatalogProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeGetCatalogProductsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCatalogProduct>* output, NSError* error)) handler;
```

Queries catalogProducts of UdropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSString* filter = @"filter_example"; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Queries catalogProducts of UdropshipVendor.
[apiInstance udropshipVendorPrototypeGetCatalogProductsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPCatalogProduct>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeGetCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPCatalogProduct>***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeGetConfig**
```objc
-(NSNumber*) udropshipVendorPrototypeGetConfigWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPHarpoonHpublicVendorconfig* output, NSError* error)) handler;
```

Fetches hasOne relation config.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSNumber* refresh = @true; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Fetches hasOne relation config.
[apiInstance udropshipVendorPrototypeGetConfigWithId:_id
              refresh:refresh
          completionHandler: ^(HRPHarpoonHpublicVendorconfig* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeGetConfig: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPHarpoonHpublicVendorconfig***](HRPHarpoonHpublicVendorconfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners**
```objc
-(NSNumber*) udropshipVendorPrototypeGetHarpoonHpublicApplicationpartnersWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPHarpoonHpublicApplicationpartner>* output, NSError* error)) handler;
```

Queries harpoonHpublicApplicationpartners of UdropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSString* filter = @"filter_example"; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Queries harpoonHpublicApplicationpartners of UdropshipVendor.
[apiInstance udropshipVendorPrototypeGetHarpoonHpublicApplicationpartnersWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPHarpoonHpublicApplicationpartner>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPHarpoonHpublicApplicationpartner>***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories**
```objc
-(NSNumber*) udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategoriesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPHarpoonHpublicv12VendorAppCategory>* output, NSError* error)) handler;
```

Queries harpoonHpublicv12VendorAppCategories of UdropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSString* filter = @"filter_example"; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Queries harpoonHpublicv12VendorAppCategories of UdropshipVendor.
[apiInstance udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategoriesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPHarpoonHpublicv12VendorAppCategory>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPHarpoonHpublicv12VendorAppCategory>***](HRPHarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeGetPartners**
```objc
-(NSNumber*) udropshipVendorPrototypeGetPartnersWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPUdropshipVendor>* output, NSError* error)) handler;
```

Queries partners of UdropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSString* filter = @"filter_example"; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Queries partners of UdropshipVendor.
[apiInstance udropshipVendorPrototypeGetPartnersWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPUdropshipVendor>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeGetPartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPUdropshipVendor>***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeGetUdropshipVendorProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeGetUdropshipVendorProductsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPUdropshipVendorProduct>* output, NSError* error)) handler;
```

Queries udropshipVendorProducts of UdropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSString* filter = @"filter_example"; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Queries udropshipVendorProducts of UdropshipVendor.
[apiInstance udropshipVendorPrototypeGetUdropshipVendorProductsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPUdropshipVendorProduct>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeGetUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPUdropshipVendorProduct>***](HRPUdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeGetVendorLocations**
```objc
-(NSNumber*) udropshipVendorPrototypeGetVendorLocationsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPHarpoonHpublicBrandvenue>* output, NSError* error)) handler;
```

Queries vendorLocations of UdropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
NSString* filter = @"filter_example"; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Queries vendorLocations of UdropshipVendor.
[apiInstance udropshipVendorPrototypeGetVendorLocationsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPHarpoonHpublicBrandvenue>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeGetVendorLocations: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPHarpoonHpublicBrandvenue>***](HRPHarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeLinkCatalogProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeLinkCatalogProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPUdropshipVendorProduct*) data
        completionHandler: (void (^)(HRPUdropshipVendorProduct* output, NSError* error)) handler;
```

Add a related item by id for catalogProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogProducts
NSString* _id = @"_id_example"; // UdropshipVendor id
HRPUdropshipVendorProduct* data = [[HRPUdropshipVendorProduct alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Add a related item by id for catalogProducts.
[apiInstance udropshipVendorPrototypeLinkCatalogProductsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPUdropshipVendorProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeLinkCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogProducts | 
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPUdropshipVendorProduct***](HRPUdropshipVendorProduct*.md)|  | [optional] 

### Return type

[**HRPUdropshipVendorProduct***](HRPUdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeLinkPartners**
```objc
-(NSNumber*) udropshipVendorPrototypeLinkPartnersWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPUdropshipVendorPartner*) data
        completionHandler: (void (^)(HRPUdropshipVendorPartner* output, NSError* error)) handler;
```

Add a related item by id for partners.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for partners
NSString* _id = @"_id_example"; // UdropshipVendor id
HRPUdropshipVendorPartner* data = [[HRPUdropshipVendorPartner alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Add a related item by id for partners.
[apiInstance udropshipVendorPrototypeLinkPartnersWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPUdropshipVendorPartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeLinkPartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for partners | 
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPUdropshipVendorPartner***](HRPUdropshipVendorPartner*.md)|  | [optional] 

### Return type

[**HRPUdropshipVendorPartner***](HRPUdropshipVendorPartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeUnlinkCatalogProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeUnlinkCatalogProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Remove the catalogProducts relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogProducts
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Remove the catalogProducts relation to an item by id.
[apiInstance udropshipVendorPrototypeUnlinkCatalogProductsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeUnlinkCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogProducts | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeUnlinkPartners**
```objc
-(NSNumber*) udropshipVendorPrototypeUnlinkPartnersWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Remove the partners relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for partners
NSString* _id = @"_id_example"; // UdropshipVendor id

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Remove the partners relation to an item by id.
[apiInstance udropshipVendorPrototypeUnlinkPartnersWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeUnlinkPartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for partners | 
 **_id** | **NSString***| UdropshipVendor id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid**
```objc
-(NSNumber*) udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsidWithId: (NSString*) _id
    data: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; // An object of model property name/value pairs (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsidWithId:_id
              data:data
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid**
```objc
-(NSNumber*) udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsidWithId: (NSString*) _id
    data: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; // An object of model property name/value pairs (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsidWithId:_id
              data:data
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeUpdateByIdCatalogProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeUpdateByIdCatalogProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Update a related item by id for catalogProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogProducts
NSString* _id = @"_id_example"; // UdropshipVendor id
HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Update a related item by id for catalogProducts.
[apiInstance udropshipVendorPrototypeUpdateByIdCatalogProductsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeUpdateByIdCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogProducts | 
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)|  | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners**
```objc
-(NSNumber*) udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartnersWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPHarpoonHpublicApplicationpartner*) data
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Update a related item by id for harpoonHpublicApplicationpartners.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for harpoonHpublicApplicationpartners
NSString* _id = @"_id_example"; // UdropshipVendor id
HRPHarpoonHpublicApplicationpartner* data = [[HRPHarpoonHpublicApplicationpartner alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Update a related item by id for harpoonHpublicApplicationpartners.
[apiInstance udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartnersWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for harpoonHpublicApplicationpartners | 
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner*.md)|  | [optional] 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories**
```objc
-(NSNumber*) udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategoriesWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPHarpoonHpublicv12VendorAppCategory*) data
        completionHandler: (void (^)(HRPHarpoonHpublicv12VendorAppCategory* output, NSError* error)) handler;
```

Update a related item by id for harpoonHpublicv12VendorAppCategories.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for harpoonHpublicv12VendorAppCategories
NSString* _id = @"_id_example"; // UdropshipVendor id
HRPHarpoonHpublicv12VendorAppCategory* data = [[HRPHarpoonHpublicv12VendorAppCategory alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Update a related item by id for harpoonHpublicv12VendorAppCategories.
[apiInstance udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategoriesWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPHarpoonHpublicv12VendorAppCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for harpoonHpublicv12VendorAppCategories | 
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPHarpoonHpublicv12VendorAppCategory***](HRPHarpoonHpublicv12VendorAppCategory*.md)|  | [optional] 

### Return type

[**HRPHarpoonHpublicv12VendorAppCategory***](HRPHarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeUpdateByIdPartners**
```objc
-(NSNumber*) udropshipVendorPrototypeUpdateByIdPartnersWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Update a related item by id for partners.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for partners
NSString* _id = @"_id_example"; // UdropshipVendor id
HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Update a related item by id for partners.
[apiInstance udropshipVendorPrototypeUpdateByIdPartnersWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeUpdateByIdPartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for partners | 
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)|  | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts**
```objc
-(NSNumber*) udropshipVendorPrototypeUpdateByIdUdropshipVendorProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPUdropshipVendorProduct*) data
        completionHandler: (void (^)(HRPUdropshipVendorProduct* output, NSError* error)) handler;
```

Update a related item by id for udropshipVendorProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for udropshipVendorProducts
NSString* _id = @"_id_example"; // UdropshipVendor id
HRPUdropshipVendorProduct* data = [[HRPUdropshipVendorProduct alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Update a related item by id for udropshipVendorProducts.
[apiInstance udropshipVendorPrototypeUpdateByIdUdropshipVendorProductsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPUdropshipVendorProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for udropshipVendorProducts | 
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPUdropshipVendorProduct***](HRPUdropshipVendorProduct*.md)|  | [optional] 

### Return type

[**HRPUdropshipVendorProduct***](HRPUdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeUpdateByIdVendorLocations**
```objc
-(NSNumber*) udropshipVendorPrototypeUpdateByIdVendorLocationsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPHarpoonHpublicBrandvenue*) data
        completionHandler: (void (^)(HRPHarpoonHpublicBrandvenue* output, NSError* error)) handler;
```

Update a related item by id for vendorLocations.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for vendorLocations
NSString* _id = @"_id_example"; // UdropshipVendor id
HRPHarpoonHpublicBrandvenue* data = [[HRPHarpoonHpublicBrandvenue alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Update a related item by id for vendorLocations.
[apiInstance udropshipVendorPrototypeUpdateByIdVendorLocationsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPHarpoonHpublicBrandvenue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeUpdateByIdVendorLocations: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for vendorLocations | 
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPHarpoonHpublicBrandvenue***](HRPHarpoonHpublicBrandvenue*.md)|  | [optional] 

### Return type

[**HRPHarpoonHpublicBrandvenue***](HRPHarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorPrototypeUpdateConfig**
```objc
-(NSNumber*) udropshipVendorPrototypeUpdateConfigWithId: (NSString*) _id
    data: (HRPHarpoonHpublicVendorconfig*) data
        completionHandler: (void (^)(HRPHarpoonHpublicVendorconfig* output, NSError* error)) handler;
```

Update config of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // UdropshipVendor id
HRPHarpoonHpublicVendorconfig* data = [[HRPHarpoonHpublicVendorconfig alloc] init]; //  (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Update config of this model.
[apiInstance udropshipVendorPrototypeUpdateConfigWithId:_id
              data:data
          completionHandler: ^(HRPHarpoonHpublicVendorconfig* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorPrototypeUpdateConfig: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| UdropshipVendor id | 
 **data** | [**HRPHarpoonHpublicVendorconfig***](HRPHarpoonHpublicVendorconfig*.md)|  | [optional] 

### Return type

[**HRPHarpoonHpublicVendorconfig***](HRPHarpoonHpublicVendorconfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorReplaceById**
```objc
-(NSNumber*) udropshipVendorReplaceByIdWithId: (NSString*) _id
    data: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; // Model instance data (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance udropshipVendorReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)| Model instance data | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorReplaceOrCreate**
```objc
-(NSNumber*) udropshipVendorReplaceOrCreateWithData: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; // Model instance data (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance udropshipVendorReplaceOrCreateWithData:data
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)| Model instance data | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorUpdateAll**
```objc
-(NSNumber*) udropshipVendorUpdateAllWithWhere: (NSString*) where
    data: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; // An object of model property name/value pairs (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance udropshipVendorUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorUpsertPatchUdropshipVendors**
```objc
-(NSNumber*) udropshipVendorUpsertPatchUdropshipVendorsWithData: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; // Model instance data (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance udropshipVendorUpsertPatchUdropshipVendorsWithData:data
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorUpsertPatchUdropshipVendors: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)| Model instance data | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorUpsertPutUdropshipVendors**
```objc
-(NSNumber*) udropshipVendorUpsertPutUdropshipVendorsWithData: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; // Model instance data (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance udropshipVendorUpsertPutUdropshipVendorsWithData:data
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorUpsertPutUdropshipVendors: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)| Model instance data | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **udropshipVendorUpsertWithWhere**
```objc
-(NSNumber*) udropshipVendorUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; // An object of model property name/value pairs (optional)

HRPUdropshipVendorApi*apiInstance = [[HRPUdropshipVendorApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance udropshipVendorUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPUdropshipVendorApi->udropshipVendorUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

