# HRPStripePlan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **NSString*** |  | [optional] 
**currency** | **NSString*** |  | [optional] 
**interval** | **NSString*** |  | [optional] 
**intervalCount** | **NSNumber*** |  | [optional] 
**metadata** | **NSString*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**statementDescriptor** | **NSString*** |  | [optional] 
**trialPeriodDays** | **NSNumber*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


