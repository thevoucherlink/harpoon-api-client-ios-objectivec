# HRPPlaylist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** | Playlist name | [optional] 
**shortDescription** | **NSString*** | Playlist short description | [optional] 
**image** | **NSString*** | Playlist image | [optional] 
**isMain** | **NSNumber*** | If Playlist is included in Main | [optional] [default to @0]
**_id** | **NSNumber*** |  | [optional] 
**appId** | **NSString*** |  | [optional] 
**playlistItems** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


