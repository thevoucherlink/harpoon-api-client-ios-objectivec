# HRPDealApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dealFind**](HRPDealApi.md#dealfind) | **GET** /Deals | Find all instances of the model matched by filter from the data source.
[**dealFindById**](HRPDealApi.md#dealfindbyid) | **GET** /Deals/{id} | Find a model instance by {{id}} from the data source.
[**dealFindOne**](HRPDealApi.md#dealfindone) | **GET** /Deals/findOne | Find first instance of the model matched by filter from the data source.
[**dealReplaceById**](HRPDealApi.md#dealreplacebyid) | **POST** /Deals/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**dealReplaceOrCreate**](HRPDealApi.md#dealreplaceorcreate) | **POST** /Deals/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**dealUpsertWithWhere**](HRPDealApi.md#dealupsertwithwhere) | **POST** /Deals/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **dealFind**
```objc
-(NSNumber*) dealFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPDeal>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPDealApi*apiInstance = [[HRPDealApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance dealFindWithFilter:filter
          completionHandler: ^(NSArray<HRPDeal>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealApi->dealFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPDeal>***](HRPDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealFindById**
```objc
-(NSNumber*) dealFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPDeal* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPDealApi*apiInstance = [[HRPDealApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance dealFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealApi->dealFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPDeal***](HRPDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealFindOne**
```objc
-(NSNumber*) dealFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPDeal* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPDealApi*apiInstance = [[HRPDealApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance dealFindOneWithFilter:filter
          completionHandler: ^(HRPDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealApi->dealFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPDeal***](HRPDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealReplaceById**
```objc
-(NSNumber*) dealReplaceByIdWithId: (NSString*) _id
    data: (HRPDeal*) data
        completionHandler: (void (^)(HRPDeal* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPDeal* data = [[HRPDeal alloc] init]; // Model instance data (optional)

HRPDealApi*apiInstance = [[HRPDealApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance dealReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealApi->dealReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPDeal***](HRPDeal*.md)| Model instance data | [optional] 

### Return type

[**HRPDeal***](HRPDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealReplaceOrCreate**
```objc
-(NSNumber*) dealReplaceOrCreateWithData: (HRPDeal*) data
        completionHandler: (void (^)(HRPDeal* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPDeal* data = [[HRPDeal alloc] init]; // Model instance data (optional)

HRPDealApi*apiInstance = [[HRPDealApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance dealReplaceOrCreateWithData:data
          completionHandler: ^(HRPDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealApi->dealReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPDeal***](HRPDeal*.md)| Model instance data | [optional] 

### Return type

[**HRPDeal***](HRPDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealUpsertWithWhere**
```objc
-(NSNumber*) dealUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPDeal*) data
        completionHandler: (void (^)(HRPDeal* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPDeal* data = [[HRPDeal alloc] init]; // An object of model property name/value pairs (optional)

HRPDealApi*apiInstance = [[HRPDealApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance dealUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealApi->dealUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPDeal***](HRPDeal*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPDeal***](HRPDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

