# HRPUdropshipVendorProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**vendorId** | **NSNumber*** |  | 
**productId** | **NSNumber*** |  | 
**priority** | **NSNumber*** |  | 
**carrierCode** | **NSString*** |  | [optional] 
**vendorSku** | **NSString*** |  | [optional] 
**vendorCost** | **NSString*** |  | [optional] 
**stockQty** | **NSString*** |  | [optional] 
**backorders** | **NSNumber*** |  | 
**shippingPrice** | **NSString*** |  | [optional] 
**status** | **NSNumber*** |  | 
**reservedQty** | **NSString*** |  | [optional] 
**availState** | **NSString*** |  | [optional] 
**availDate** | **NSDate*** |  | [optional] 
**relationshipStatus** | **NSString*** |  | [optional] 
**udropshipVendor** | **NSObject*** |  | [optional] 
**catalogProduct** | **NSObject*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


