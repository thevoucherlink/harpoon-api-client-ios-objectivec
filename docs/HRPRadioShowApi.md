# HRPRadioShowApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioShowCount**](HRPRadioShowApi.md#radioshowcount) | **GET** /RadioShows/count | Count instances of the model matched by where from the data source.
[**radioShowCreate**](HRPRadioShowApi.md#radioshowcreate) | **POST** /RadioShows | Create a new instance of the model and persist it into the data source.
[**radioShowCreateChangeStreamGetRadioShowsChangeStream**](HRPRadioShowApi.md#radioshowcreatechangestreamgetradioshowschangestream) | **GET** /RadioShows/change-stream | Create a change stream.
[**radioShowCreateChangeStreamPostRadioShowsChangeStream**](HRPRadioShowApi.md#radioshowcreatechangestreampostradioshowschangestream) | **POST** /RadioShows/change-stream | Create a change stream.
[**radioShowDeleteById**](HRPRadioShowApi.md#radioshowdeletebyid) | **DELETE** /RadioShows/{id} | Delete a model instance by {{id}} from the data source.
[**radioShowExistsGetRadioShowsidExists**](HRPRadioShowApi.md#radioshowexistsgetradioshowsidexists) | **GET** /RadioShows/{id}/exists | Check whether a model instance exists in the data source.
[**radioShowExistsHeadRadioShowsid**](HRPRadioShowApi.md#radioshowexistsheadradioshowsid) | **HEAD** /RadioShows/{id} | Check whether a model instance exists in the data source.
[**radioShowFind**](HRPRadioShowApi.md#radioshowfind) | **GET** /RadioShows | Find all instances of the model matched by filter from the data source.
[**radioShowFindById**](HRPRadioShowApi.md#radioshowfindbyid) | **GET** /RadioShows/{id} | Find a model instance by {{id}} from the data source.
[**radioShowFindOne**](HRPRadioShowApi.md#radioshowfindone) | **GET** /RadioShows/findOne | Find first instance of the model matched by filter from the data source.
[**radioShowPrototypeCountRadioPresenters**](HRPRadioShowApi.md#radioshowprototypecountradiopresenters) | **GET** /RadioShows/{id}/radioPresenters/count | Counts radioPresenters of RadioShow.
[**radioShowPrototypeCountRadioShowTimes**](HRPRadioShowApi.md#radioshowprototypecountradioshowtimes) | **GET** /RadioShows/{id}/radioShowTimes/count | Counts radioShowTimes of RadioShow.
[**radioShowPrototypeCreateRadioPresenters**](HRPRadioShowApi.md#radioshowprototypecreateradiopresenters) | **POST** /RadioShows/{id}/radioPresenters | Creates a new instance in radioPresenters of this model.
[**radioShowPrototypeCreateRadioShowTimes**](HRPRadioShowApi.md#radioshowprototypecreateradioshowtimes) | **POST** /RadioShows/{id}/radioShowTimes | Creates a new instance in radioShowTimes of this model.
[**radioShowPrototypeDeleteRadioPresenters**](HRPRadioShowApi.md#radioshowprototypedeleteradiopresenters) | **DELETE** /RadioShows/{id}/radioPresenters | Deletes all radioPresenters of this model.
[**radioShowPrototypeDeleteRadioShowTimes**](HRPRadioShowApi.md#radioshowprototypedeleteradioshowtimes) | **DELETE** /RadioShows/{id}/radioShowTimes | Deletes all radioShowTimes of this model.
[**radioShowPrototypeDestroyByIdRadioPresenters**](HRPRadioShowApi.md#radioshowprototypedestroybyidradiopresenters) | **DELETE** /RadioShows/{id}/radioPresenters/{fk} | Delete a related item by id for radioPresenters.
[**radioShowPrototypeDestroyByIdRadioShowTimes**](HRPRadioShowApi.md#radioshowprototypedestroybyidradioshowtimes) | **DELETE** /RadioShows/{id}/radioShowTimes/{fk} | Delete a related item by id for radioShowTimes.
[**radioShowPrototypeExistsRadioPresenters**](HRPRadioShowApi.md#radioshowprototypeexistsradiopresenters) | **HEAD** /RadioShows/{id}/radioPresenters/rel/{fk} | Check the existence of radioPresenters relation to an item by id.
[**radioShowPrototypeFindByIdRadioPresenters**](HRPRadioShowApi.md#radioshowprototypefindbyidradiopresenters) | **GET** /RadioShows/{id}/radioPresenters/{fk} | Find a related item by id for radioPresenters.
[**radioShowPrototypeFindByIdRadioShowTimes**](HRPRadioShowApi.md#radioshowprototypefindbyidradioshowtimes) | **GET** /RadioShows/{id}/radioShowTimes/{fk} | Find a related item by id for radioShowTimes.
[**radioShowPrototypeGetPlaylistItem**](HRPRadioShowApi.md#radioshowprototypegetplaylistitem) | **GET** /RadioShows/{id}/playlistItem | Fetches belongsTo relation playlistItem.
[**radioShowPrototypeGetRadioPresenters**](HRPRadioShowApi.md#radioshowprototypegetradiopresenters) | **GET** /RadioShows/{id}/radioPresenters | Queries radioPresenters of RadioShow.
[**radioShowPrototypeGetRadioShowTimes**](HRPRadioShowApi.md#radioshowprototypegetradioshowtimes) | **GET** /RadioShows/{id}/radioShowTimes | Queries radioShowTimes of RadioShow.
[**radioShowPrototypeGetRadioStream**](HRPRadioShowApi.md#radioshowprototypegetradiostream) | **GET** /RadioShows/{id}/radioStream | Fetches belongsTo relation radioStream.
[**radioShowPrototypeLinkRadioPresenters**](HRPRadioShowApi.md#radioshowprototypelinkradiopresenters) | **PUT** /RadioShows/{id}/radioPresenters/rel/{fk} | Add a related item by id for radioPresenters.
[**radioShowPrototypeUnlinkRadioPresenters**](HRPRadioShowApi.md#radioshowprototypeunlinkradiopresenters) | **DELETE** /RadioShows/{id}/radioPresenters/rel/{fk} | Remove the radioPresenters relation to an item by id.
[**radioShowPrototypeUpdateAttributesPatchRadioShowsid**](HRPRadioShowApi.md#radioshowprototypeupdateattributespatchradioshowsid) | **PATCH** /RadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowPrototypeUpdateAttributesPutRadioShowsid**](HRPRadioShowApi.md#radioshowprototypeupdateattributesputradioshowsid) | **PUT** /RadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowPrototypeUpdateByIdRadioPresenters**](HRPRadioShowApi.md#radioshowprototypeupdatebyidradiopresenters) | **PUT** /RadioShows/{id}/radioPresenters/{fk} | Update a related item by id for radioPresenters.
[**radioShowPrototypeUpdateByIdRadioShowTimes**](HRPRadioShowApi.md#radioshowprototypeupdatebyidradioshowtimes) | **PUT** /RadioShows/{id}/radioShowTimes/{fk} | Update a related item by id for radioShowTimes.
[**radioShowReplaceById**](HRPRadioShowApi.md#radioshowreplacebyid) | **POST** /RadioShows/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioShowReplaceOrCreate**](HRPRadioShowApi.md#radioshowreplaceorcreate) | **POST** /RadioShows/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioShowUpdateAll**](HRPRadioShowApi.md#radioshowupdateall) | **POST** /RadioShows/update | Update instances of the model matched by {{where}} from the data source.
[**radioShowUploadFile**](HRPRadioShowApi.md#radioshowuploadfile) | **POST** /RadioShows/{id}/file | Upload File to Radio Show
[**radioShowUpsertPatchRadioShows**](HRPRadioShowApi.md#radioshowupsertpatchradioshows) | **PATCH** /RadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioShowUpsertPutRadioShows**](HRPRadioShowApi.md#radioshowupsertputradioshows) | **PUT** /RadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioShowUpsertWithWhere**](HRPRadioShowApi.md#radioshowupsertwithwhere) | **POST** /RadioShows/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **radioShowCount**
```objc
-(NSNumber*) radioShowCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance radioShowCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowCreate**
```objc
-(NSNumber*) radioShowCreateWithData: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioShow* data = [[HRPRadioShow alloc] init]; // Model instance data (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance radioShowCreateWithData:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowCreateChangeStreamGetRadioShowsChangeStream**
```objc
-(NSNumber*) radioShowCreateChangeStreamGetRadioShowsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Create a change stream.
[apiInstance radioShowCreateChangeStreamGetRadioShowsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowCreateChangeStreamGetRadioShowsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowCreateChangeStreamPostRadioShowsChangeStream**
```objc
-(NSNumber*) radioShowCreateChangeStreamPostRadioShowsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Create a change stream.
[apiInstance radioShowCreateChangeStreamPostRadioShowsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowCreateChangeStreamPostRadioShowsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowDeleteById**
```objc
-(NSNumber*) radioShowDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance radioShowDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowExistsGetRadioShowsidExists**
```objc
-(NSNumber*) radioShowExistsGetRadioShowsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance radioShowExistsGetRadioShowsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowExistsGetRadioShowsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowExistsHeadRadioShowsid**
```objc
-(NSNumber*) radioShowExistsHeadRadioShowsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance radioShowExistsHeadRadioShowsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowExistsHeadRadioShowsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowFind**
```objc
-(NSNumber*) radioShowFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRadioShow>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance radioShowFindWithFilter:filter
          completionHandler: ^(NSArray<HRPRadioShow>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPRadioShow>***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowFindById**
```objc
-(NSNumber*) radioShowFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance radioShowFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowFindOne**
```objc
-(NSNumber*) radioShowFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance radioShowFindOneWithFilter:filter
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeCountRadioPresenters**
```objc
-(NSNumber*) radioShowPrototypeCountRadioPresentersWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts radioPresenters of RadioShow.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShow id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Counts radioPresenters of RadioShow.
[apiInstance radioShowPrototypeCountRadioPresentersWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeCountRadioPresenters: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShow id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeCountRadioShowTimes**
```objc
-(NSNumber*) radioShowPrototypeCountRadioShowTimesWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts radioShowTimes of RadioShow.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShow id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Counts radioShowTimes of RadioShow.
[apiInstance radioShowPrototypeCountRadioShowTimesWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeCountRadioShowTimes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShow id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeCreateRadioPresenters**
```objc
-(NSNumber*) radioShowPrototypeCreateRadioPresentersWithId: (NSString*) _id
    data: (HRPRadioPresenter*) data
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Creates a new instance in radioPresenters of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShow id
HRPRadioPresenter* data = [[HRPRadioPresenter alloc] init]; //  (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Creates a new instance in radioPresenters of this model.
[apiInstance radioShowPrototypeCreateRadioPresentersWithId:_id
              data:data
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeCreateRadioPresenters: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShow id | 
 **data** | [**HRPRadioPresenter***](HRPRadioPresenter*.md)|  | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeCreateRadioShowTimes**
```objc
-(NSNumber*) radioShowPrototypeCreateRadioShowTimesWithId: (NSString*) _id
    data: (HRPRadioShowTime*) data
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Creates a new instance in radioShowTimes of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShow id
HRPRadioShowTime* data = [[HRPRadioShowTime alloc] init]; //  (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Creates a new instance in radioShowTimes of this model.
[apiInstance radioShowPrototypeCreateRadioShowTimesWithId:_id
              data:data
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeCreateRadioShowTimes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShow id | 
 **data** | [**HRPRadioShowTime***](HRPRadioShowTime*.md)|  | [optional] 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeDeleteRadioPresenters**
```objc
-(NSNumber*) radioShowPrototypeDeleteRadioPresentersWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all radioPresenters of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShow id

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Deletes all radioPresenters of this model.
[apiInstance radioShowPrototypeDeleteRadioPresentersWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeDeleteRadioPresenters: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShow id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeDeleteRadioShowTimes**
```objc
-(NSNumber*) radioShowPrototypeDeleteRadioShowTimesWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all radioShowTimes of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShow id

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Deletes all radioShowTimes of this model.
[apiInstance radioShowPrototypeDeleteRadioShowTimesWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeDeleteRadioShowTimes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShow id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeDestroyByIdRadioPresenters**
```objc
-(NSNumber*) radioShowPrototypeDestroyByIdRadioPresentersWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for radioPresenters.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioPresenters
NSString* _id = @"_id_example"; // RadioShow id

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Delete a related item by id for radioPresenters.
[apiInstance radioShowPrototypeDestroyByIdRadioPresentersWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeDestroyByIdRadioPresenters: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioPresenters | 
 **_id** | **NSString***| RadioShow id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeDestroyByIdRadioShowTimes**
```objc
-(NSNumber*) radioShowPrototypeDestroyByIdRadioShowTimesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for radioShowTimes.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShowTimes
NSString* _id = @"_id_example"; // RadioShow id

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Delete a related item by id for radioShowTimes.
[apiInstance radioShowPrototypeDestroyByIdRadioShowTimesWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeDestroyByIdRadioShowTimes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShowTimes | 
 **_id** | **NSString***| RadioShow id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeExistsRadioPresenters**
```objc
-(NSNumber*) radioShowPrototypeExistsRadioPresentersWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSNumber* output, NSError* error)) handler;
```

Check the existence of radioPresenters relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioPresenters
NSString* _id = @"_id_example"; // RadioShow id

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Check the existence of radioPresenters relation to an item by id.
[apiInstance radioShowPrototypeExistsRadioPresentersWithFk:fk
              _id:_id
          completionHandler: ^(NSNumber* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeExistsRadioPresenters: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioPresenters | 
 **_id** | **NSString***| RadioShow id | 

### Return type

**NSNumber***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeFindByIdRadioPresenters**
```objc
-(NSNumber*) radioShowPrototypeFindByIdRadioPresentersWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Find a related item by id for radioPresenters.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioPresenters
NSString* _id = @"_id_example"; // RadioShow id

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Find a related item by id for radioPresenters.
[apiInstance radioShowPrototypeFindByIdRadioPresentersWithFk:fk
              _id:_id
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeFindByIdRadioPresenters: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioPresenters | 
 **_id** | **NSString***| RadioShow id | 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeFindByIdRadioShowTimes**
```objc
-(NSNumber*) radioShowPrototypeFindByIdRadioShowTimesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Find a related item by id for radioShowTimes.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShowTimes
NSString* _id = @"_id_example"; // RadioShow id

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Find a related item by id for radioShowTimes.
[apiInstance radioShowPrototypeFindByIdRadioShowTimesWithFk:fk
              _id:_id
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeFindByIdRadioShowTimes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShowTimes | 
 **_id** | **NSString***| RadioShow id | 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeGetPlaylistItem**
```objc
-(NSNumber*) radioShowPrototypeGetPlaylistItemWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Fetches belongsTo relation playlistItem.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShow id
NSNumber* refresh = @true; //  (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Fetches belongsTo relation playlistItem.
[apiInstance radioShowPrototypeGetPlaylistItemWithId:_id
              refresh:refresh
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeGetPlaylistItem: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShow id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeGetRadioPresenters**
```objc
-(NSNumber*) radioShowPrototypeGetRadioPresentersWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRadioPresenter>* output, NSError* error)) handler;
```

Queries radioPresenters of RadioShow.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShow id
NSString* filter = @"filter_example"; //  (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Queries radioPresenters of RadioShow.
[apiInstance radioShowPrototypeGetRadioPresentersWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPRadioPresenter>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeGetRadioPresenters: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShow id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPRadioPresenter>***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeGetRadioShowTimes**
```objc
-(NSNumber*) radioShowPrototypeGetRadioShowTimesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRadioShowTime>* output, NSError* error)) handler;
```

Queries radioShowTimes of RadioShow.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShow id
NSString* filter = @"filter_example"; //  (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Queries radioShowTimes of RadioShow.
[apiInstance radioShowPrototypeGetRadioShowTimesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPRadioShowTime>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeGetRadioShowTimes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShow id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPRadioShowTime>***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeGetRadioStream**
```objc
-(NSNumber*) radioShowPrototypeGetRadioStreamWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Fetches belongsTo relation radioStream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShow id
NSNumber* refresh = @true; //  (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Fetches belongsTo relation radioStream.
[apiInstance radioShowPrototypeGetRadioStreamWithId:_id
              refresh:refresh
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeGetRadioStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShow id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeLinkRadioPresenters**
```objc
-(NSNumber*) radioShowPrototypeLinkRadioPresentersWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPRadioPresenterRadioShow*) data
        completionHandler: (void (^)(HRPRadioPresenterRadioShow* output, NSError* error)) handler;
```

Add a related item by id for radioPresenters.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioPresenters
NSString* _id = @"_id_example"; // RadioShow id
HRPRadioPresenterRadioShow* data = [[HRPRadioPresenterRadioShow alloc] init]; //  (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Add a related item by id for radioPresenters.
[apiInstance radioShowPrototypeLinkRadioPresentersWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPRadioPresenterRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeLinkRadioPresenters: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioPresenters | 
 **_id** | **NSString***| RadioShow id | 
 **data** | [**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow*.md)|  | [optional] 

### Return type

[**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeUnlinkRadioPresenters**
```objc
-(NSNumber*) radioShowPrototypeUnlinkRadioPresentersWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Remove the radioPresenters relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioPresenters
NSString* _id = @"_id_example"; // RadioShow id

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Remove the radioPresenters relation to an item by id.
[apiInstance radioShowPrototypeUnlinkRadioPresentersWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeUnlinkRadioPresenters: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioPresenters | 
 **_id** | **NSString***| RadioShow id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeUpdateAttributesPatchRadioShowsid**
```objc
-(NSNumber*) radioShowPrototypeUpdateAttributesPatchRadioShowsidWithId: (NSString*) _id
    data: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShow id
HRPRadioShow* data = [[HRPRadioShow alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance radioShowPrototypeUpdateAttributesPatchRadioShowsidWithId:_id
              data:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeUpdateAttributesPatchRadioShowsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShow id | 
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeUpdateAttributesPutRadioShowsid**
```objc
-(NSNumber*) radioShowPrototypeUpdateAttributesPutRadioShowsidWithId: (NSString*) _id
    data: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioShow id
HRPRadioShow* data = [[HRPRadioShow alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance radioShowPrototypeUpdateAttributesPutRadioShowsidWithId:_id
              data:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeUpdateAttributesPutRadioShowsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioShow id | 
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeUpdateByIdRadioPresenters**
```objc
-(NSNumber*) radioShowPrototypeUpdateByIdRadioPresentersWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPRadioPresenter*) data
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Update a related item by id for radioPresenters.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioPresenters
NSString* _id = @"_id_example"; // RadioShow id
HRPRadioPresenter* data = [[HRPRadioPresenter alloc] init]; //  (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Update a related item by id for radioPresenters.
[apiInstance radioShowPrototypeUpdateByIdRadioPresentersWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeUpdateByIdRadioPresenters: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioPresenters | 
 **_id** | **NSString***| RadioShow id | 
 **data** | [**HRPRadioPresenter***](HRPRadioPresenter*.md)|  | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowPrototypeUpdateByIdRadioShowTimes**
```objc
-(NSNumber*) radioShowPrototypeUpdateByIdRadioShowTimesWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPRadioShowTime*) data
        completionHandler: (void (^)(HRPRadioShowTime* output, NSError* error)) handler;
```

Update a related item by id for radioShowTimes.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShowTimes
NSString* _id = @"_id_example"; // RadioShow id
HRPRadioShowTime* data = [[HRPRadioShowTime alloc] init]; //  (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Update a related item by id for radioShowTimes.
[apiInstance radioShowPrototypeUpdateByIdRadioShowTimesWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPRadioShowTime* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowPrototypeUpdateByIdRadioShowTimes: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShowTimes | 
 **_id** | **NSString***| RadioShow id | 
 **data** | [**HRPRadioShowTime***](HRPRadioShowTime*.md)|  | [optional] 

### Return type

[**HRPRadioShowTime***](HRPRadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowReplaceById**
```objc
-(NSNumber*) radioShowReplaceByIdWithId: (NSString*) _id
    data: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPRadioShow* data = [[HRPRadioShow alloc] init]; // Model instance data (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance radioShowReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowReplaceOrCreate**
```objc
-(NSNumber*) radioShowReplaceOrCreateWithData: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioShow* data = [[HRPRadioShow alloc] init]; // Model instance data (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance radioShowReplaceOrCreateWithData:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowUpdateAll**
```objc
-(NSNumber*) radioShowUpdateAllWithWhere: (NSString*) where
    data: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRadioShow* data = [[HRPRadioShow alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance radioShowUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowUploadFile**
```objc
-(NSNumber*) radioShowUploadFileWithId: (NSString*) _id
    data: (HRPMagentoFileUpload*) data
        completionHandler: (void (^)(HRPMagentoFileUpload* output, NSError* error)) handler;
```

Upload File to Radio Show

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Radio Show Id
HRPMagentoFileUpload* data = [[HRPMagentoFileUpload alloc] init]; // Corresponds to the file you're uploading formatted as an object (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Upload File to Radio Show
[apiInstance radioShowUploadFileWithId:_id
              data:data
          completionHandler: ^(HRPMagentoFileUpload* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowUploadFile: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Radio Show Id | 
 **data** | [**HRPMagentoFileUpload***](HRPMagentoFileUpload*.md)| Corresponds to the file you&#39;re uploading formatted as an object | [optional] 

### Return type

[**HRPMagentoFileUpload***](HRPMagentoFileUpload.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowUpsertPatchRadioShows**
```objc
-(NSNumber*) radioShowUpsertPatchRadioShowsWithData: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioShow* data = [[HRPRadioShow alloc] init]; // Model instance data (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance radioShowUpsertPatchRadioShowsWithData:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowUpsertPatchRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowUpsertPutRadioShows**
```objc
-(NSNumber*) radioShowUpsertPutRadioShowsWithData: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioShow* data = [[HRPRadioShow alloc] init]; // Model instance data (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance radioShowUpsertPutRadioShowsWithData:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowUpsertPutRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioShowUpsertWithWhere**
```objc
-(NSNumber*) radioShowUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRadioShow* data = [[HRPRadioShow alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioShowApi*apiInstance = [[HRPRadioShowApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance radioShowUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioShowApi->radioShowUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

