# HRPRadioPlaySession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionTime** | **NSNumber*** |  | [optional] 
**playUpdateTime** | **NSDate*** |  | [optional] 
**radioStreamId** | **NSNumber*** |  | [optional] 
**radioShowId** | **NSNumber*** |  | [optional] 
**playEndTime** | **NSDate*** |  | [optional] 
**playStartTime** | **NSDate*** |  | [optional] 
**customerId** | **NSNumber*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


