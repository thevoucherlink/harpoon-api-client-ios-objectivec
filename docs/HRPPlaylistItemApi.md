# HRPPlaylistItemApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playlistItemCount**](HRPPlaylistItemApi.md#playlistitemcount) | **GET** /PlaylistItems/count | Count instances of the model matched by where from the data source.
[**playlistItemCreate**](HRPPlaylistItemApi.md#playlistitemcreate) | **POST** /PlaylistItems | Create a new instance of the model and persist it into the data source.
[**playlistItemCreateChangeStreamGetPlaylistItemsChangeStream**](HRPPlaylistItemApi.md#playlistitemcreatechangestreamgetplaylistitemschangestream) | **GET** /PlaylistItems/change-stream | Create a change stream.
[**playlistItemCreateChangeStreamPostPlaylistItemsChangeStream**](HRPPlaylistItemApi.md#playlistitemcreatechangestreampostplaylistitemschangestream) | **POST** /PlaylistItems/change-stream | Create a change stream.
[**playlistItemDeleteById**](HRPPlaylistItemApi.md#playlistitemdeletebyid) | **DELETE** /PlaylistItems/{id} | Delete a model instance by {{id}} from the data source.
[**playlistItemExistsGetPlaylistItemsidExists**](HRPPlaylistItemApi.md#playlistitemexistsgetplaylistitemsidexists) | **GET** /PlaylistItems/{id}/exists | Check whether a model instance exists in the data source.
[**playlistItemExistsHeadPlaylistItemsid**](HRPPlaylistItemApi.md#playlistitemexistsheadplaylistitemsid) | **HEAD** /PlaylistItems/{id} | Check whether a model instance exists in the data source.
[**playlistItemFind**](HRPPlaylistItemApi.md#playlistitemfind) | **GET** /PlaylistItems | Find all instances of the model matched by filter from the data source.
[**playlistItemFindById**](HRPPlaylistItemApi.md#playlistitemfindbyid) | **GET** /PlaylistItems/{id} | Find a model instance by {{id}} from the data source.
[**playlistItemFindOne**](HRPPlaylistItemApi.md#playlistitemfindone) | **GET** /PlaylistItems/findOne | Find first instance of the model matched by filter from the data source.
[**playlistItemPrototypeCountPlayerSources**](HRPPlaylistItemApi.md#playlistitemprototypecountplayersources) | **GET** /PlaylistItems/{id}/playerSources/count | Counts playerSources of PlaylistItem.
[**playlistItemPrototypeCountPlayerTracks**](HRPPlaylistItemApi.md#playlistitemprototypecountplayertracks) | **GET** /PlaylistItems/{id}/playerTracks/count | Counts playerTracks of PlaylistItem.
[**playlistItemPrototypeCountRadioShows**](HRPPlaylistItemApi.md#playlistitemprototypecountradioshows) | **GET** /PlaylistItems/{id}/radioShows/count | Counts radioShows of PlaylistItem.
[**playlistItemPrototypeCreatePlayerSources**](HRPPlaylistItemApi.md#playlistitemprototypecreateplayersources) | **POST** /PlaylistItems/{id}/playerSources | Creates a new instance in playerSources of this model.
[**playlistItemPrototypeCreatePlayerTracks**](HRPPlaylistItemApi.md#playlistitemprototypecreateplayertracks) | **POST** /PlaylistItems/{id}/playerTracks | Creates a new instance in playerTracks of this model.
[**playlistItemPrototypeCreateRadioShows**](HRPPlaylistItemApi.md#playlistitemprototypecreateradioshows) | **POST** /PlaylistItems/{id}/radioShows | Creates a new instance in radioShows of this model.
[**playlistItemPrototypeDeletePlayerSources**](HRPPlaylistItemApi.md#playlistitemprototypedeleteplayersources) | **DELETE** /PlaylistItems/{id}/playerSources | Deletes all playerSources of this model.
[**playlistItemPrototypeDeletePlayerTracks**](HRPPlaylistItemApi.md#playlistitemprototypedeleteplayertracks) | **DELETE** /PlaylistItems/{id}/playerTracks | Deletes all playerTracks of this model.
[**playlistItemPrototypeDeleteRadioShows**](HRPPlaylistItemApi.md#playlistitemprototypedeleteradioshows) | **DELETE** /PlaylistItems/{id}/radioShows | Deletes all radioShows of this model.
[**playlistItemPrototypeDestroyByIdPlayerSources**](HRPPlaylistItemApi.md#playlistitemprototypedestroybyidplayersources) | **DELETE** /PlaylistItems/{id}/playerSources/{fk} | Delete a related item by id for playerSources.
[**playlistItemPrototypeDestroyByIdPlayerTracks**](HRPPlaylistItemApi.md#playlistitemprototypedestroybyidplayertracks) | **DELETE** /PlaylistItems/{id}/playerTracks/{fk} | Delete a related item by id for playerTracks.
[**playlistItemPrototypeDestroyByIdRadioShows**](HRPPlaylistItemApi.md#playlistitemprototypedestroybyidradioshows) | **DELETE** /PlaylistItems/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
[**playlistItemPrototypeFindByIdPlayerSources**](HRPPlaylistItemApi.md#playlistitemprototypefindbyidplayersources) | **GET** /PlaylistItems/{id}/playerSources/{fk} | Find a related item by id for playerSources.
[**playlistItemPrototypeFindByIdPlayerTracks**](HRPPlaylistItemApi.md#playlistitemprototypefindbyidplayertracks) | **GET** /PlaylistItems/{id}/playerTracks/{fk} | Find a related item by id for playerTracks.
[**playlistItemPrototypeFindByIdRadioShows**](HRPPlaylistItemApi.md#playlistitemprototypefindbyidradioshows) | **GET** /PlaylistItems/{id}/radioShows/{fk} | Find a related item by id for radioShows.
[**playlistItemPrototypeGetPlayerSources**](HRPPlaylistItemApi.md#playlistitemprototypegetplayersources) | **GET** /PlaylistItems/{id}/playerSources | Queries playerSources of PlaylistItem.
[**playlistItemPrototypeGetPlayerTracks**](HRPPlaylistItemApi.md#playlistitemprototypegetplayertracks) | **GET** /PlaylistItems/{id}/playerTracks | Queries playerTracks of PlaylistItem.
[**playlistItemPrototypeGetPlaylist**](HRPPlaylistItemApi.md#playlistitemprototypegetplaylist) | **GET** /PlaylistItems/{id}/playlist | Fetches belongsTo relation playlist.
[**playlistItemPrototypeGetRadioShows**](HRPPlaylistItemApi.md#playlistitemprototypegetradioshows) | **GET** /PlaylistItems/{id}/radioShows | Queries radioShows of PlaylistItem.
[**playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid**](HRPPlaylistItemApi.md#playlistitemprototypeupdateattributespatchplaylistitemsid) | **PATCH** /PlaylistItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistItemPrototypeUpdateAttributesPutPlaylistItemsid**](HRPPlaylistItemApi.md#playlistitemprototypeupdateattributesputplaylistitemsid) | **PUT** /PlaylistItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistItemPrototypeUpdateByIdPlayerSources**](HRPPlaylistItemApi.md#playlistitemprototypeupdatebyidplayersources) | **PUT** /PlaylistItems/{id}/playerSources/{fk} | Update a related item by id for playerSources.
[**playlistItemPrototypeUpdateByIdPlayerTracks**](HRPPlaylistItemApi.md#playlistitemprototypeupdatebyidplayertracks) | **PUT** /PlaylistItems/{id}/playerTracks/{fk} | Update a related item by id for playerTracks.
[**playlistItemPrototypeUpdateByIdRadioShows**](HRPPlaylistItemApi.md#playlistitemprototypeupdatebyidradioshows) | **PUT** /PlaylistItems/{id}/radioShows/{fk} | Update a related item by id for radioShows.
[**playlistItemReplaceById**](HRPPlaylistItemApi.md#playlistitemreplacebyid) | **POST** /PlaylistItems/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playlistItemReplaceOrCreate**](HRPPlaylistItemApi.md#playlistitemreplaceorcreate) | **POST** /PlaylistItems/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playlistItemUpdateAll**](HRPPlaylistItemApi.md#playlistitemupdateall) | **POST** /PlaylistItems/update | Update instances of the model matched by {{where}} from the data source.
[**playlistItemUpsertPatchPlaylistItems**](HRPPlaylistItemApi.md#playlistitemupsertpatchplaylistitems) | **PATCH** /PlaylistItems | Patch an existing model instance or insert a new one into the data source.
[**playlistItemUpsertPutPlaylistItems**](HRPPlaylistItemApi.md#playlistitemupsertputplaylistitems) | **PUT** /PlaylistItems | Patch an existing model instance or insert a new one into the data source.
[**playlistItemUpsertWithWhere**](HRPPlaylistItemApi.md#playlistitemupsertwithwhere) | **POST** /PlaylistItems/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **playlistItemCount**
```objc
-(NSNumber*) playlistItemCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance playlistItemCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemCreate**
```objc
-(NSNumber*) playlistItemCreateWithData: (HRPPlaylistItem*) data
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlaylistItem* data = [[HRPPlaylistItem alloc] init]; // Model instance data (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance playlistItemCreateWithData:data
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlaylistItem***](HRPPlaylistItem*.md)| Model instance data | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemCreateChangeStreamGetPlaylistItemsChangeStream**
```objc
-(NSNumber*) playlistItemCreateChangeStreamGetPlaylistItemsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Create a change stream.
[apiInstance playlistItemCreateChangeStreamGetPlaylistItemsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemCreateChangeStreamGetPlaylistItemsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemCreateChangeStreamPostPlaylistItemsChangeStream**
```objc
-(NSNumber*) playlistItemCreateChangeStreamPostPlaylistItemsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Create a change stream.
[apiInstance playlistItemCreateChangeStreamPostPlaylistItemsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemCreateChangeStreamPostPlaylistItemsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemDeleteById**
```objc
-(NSNumber*) playlistItemDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance playlistItemDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemExistsGetPlaylistItemsidExists**
```objc
-(NSNumber*) playlistItemExistsGetPlaylistItemsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance playlistItemExistsGetPlaylistItemsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemExistsGetPlaylistItemsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemExistsHeadPlaylistItemsid**
```objc
-(NSNumber*) playlistItemExistsHeadPlaylistItemsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance playlistItemExistsHeadPlaylistItemsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemExistsHeadPlaylistItemsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemFind**
```objc
-(NSNumber*) playlistItemFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPPlaylistItem>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance playlistItemFindWithFilter:filter
          completionHandler: ^(NSArray<HRPPlaylistItem>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPPlaylistItem>***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemFindById**
```objc
-(NSNumber*) playlistItemFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance playlistItemFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemFindOne**
```objc
-(NSNumber*) playlistItemFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance playlistItemFindOneWithFilter:filter
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeCountPlayerSources**
```objc
-(NSNumber*) playlistItemPrototypeCountPlayerSourcesWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts playerSources of PlaylistItem.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Counts playerSources of PlaylistItem.
[apiInstance playlistItemPrototypeCountPlayerSourcesWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeCountPlayerSources: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeCountPlayerTracks**
```objc
-(NSNumber*) playlistItemPrototypeCountPlayerTracksWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts playerTracks of PlaylistItem.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Counts playerTracks of PlaylistItem.
[apiInstance playlistItemPrototypeCountPlayerTracksWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeCountPlayerTracks: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeCountRadioShows**
```objc
-(NSNumber*) playlistItemPrototypeCountRadioShowsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts radioShows of PlaylistItem.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Counts radioShows of PlaylistItem.
[apiInstance playlistItemPrototypeCountRadioShowsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeCountRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeCreatePlayerSources**
```objc
-(NSNumber*) playlistItemPrototypeCreatePlayerSourcesWithId: (NSString*) _id
    data: (HRPPlayerSource*) data
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Creates a new instance in playerSources of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id
HRPPlayerSource* data = [[HRPPlayerSource alloc] init]; //  (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Creates a new instance in playerSources of this model.
[apiInstance playlistItemPrototypeCreatePlayerSourcesWithId:_id
              data:data
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeCreatePlayerSources: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 
 **data** | [**HRPPlayerSource***](HRPPlayerSource*.md)|  | [optional] 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeCreatePlayerTracks**
```objc
-(NSNumber*) playlistItemPrototypeCreatePlayerTracksWithId: (NSString*) _id
    data: (HRPPlayerTrack*) data
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Creates a new instance in playerTracks of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id
HRPPlayerTrack* data = [[HRPPlayerTrack alloc] init]; //  (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Creates a new instance in playerTracks of this model.
[apiInstance playlistItemPrototypeCreatePlayerTracksWithId:_id
              data:data
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeCreatePlayerTracks: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 
 **data** | [**HRPPlayerTrack***](HRPPlayerTrack*.md)|  | [optional] 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeCreateRadioShows**
```objc
-(NSNumber*) playlistItemPrototypeCreateRadioShowsWithId: (NSString*) _id
    data: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Creates a new instance in radioShows of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id
HRPRadioShow* data = [[HRPRadioShow alloc] init]; //  (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Creates a new instance in radioShows of this model.
[apiInstance playlistItemPrototypeCreateRadioShowsWithId:_id
              data:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeCreateRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)|  | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeDeletePlayerSources**
```objc
-(NSNumber*) playlistItemPrototypeDeletePlayerSourcesWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all playerSources of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Deletes all playerSources of this model.
[apiInstance playlistItemPrototypeDeletePlayerSourcesWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeDeletePlayerSources: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeDeletePlayerTracks**
```objc
-(NSNumber*) playlistItemPrototypeDeletePlayerTracksWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all playerTracks of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Deletes all playerTracks of this model.
[apiInstance playlistItemPrototypeDeletePlayerTracksWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeDeletePlayerTracks: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeDeleteRadioShows**
```objc
-(NSNumber*) playlistItemPrototypeDeleteRadioShowsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all radioShows of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Deletes all radioShows of this model.
[apiInstance playlistItemPrototypeDeleteRadioShowsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeDeleteRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeDestroyByIdPlayerSources**
```objc
-(NSNumber*) playlistItemPrototypeDestroyByIdPlayerSourcesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for playerSources.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for playerSources
NSString* _id = @"_id_example"; // PlaylistItem id

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Delete a related item by id for playerSources.
[apiInstance playlistItemPrototypeDestroyByIdPlayerSourcesWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeDestroyByIdPlayerSources: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for playerSources | 
 **_id** | **NSString***| PlaylistItem id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeDestroyByIdPlayerTracks**
```objc
-(NSNumber*) playlistItemPrototypeDestroyByIdPlayerTracksWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for playerTracks.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for playerTracks
NSString* _id = @"_id_example"; // PlaylistItem id

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Delete a related item by id for playerTracks.
[apiInstance playlistItemPrototypeDestroyByIdPlayerTracksWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeDestroyByIdPlayerTracks: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for playerTracks | 
 **_id** | **NSString***| PlaylistItem id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeDestroyByIdRadioShows**
```objc
-(NSNumber*) playlistItemPrototypeDestroyByIdRadioShowsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for radioShows.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShows
NSString* _id = @"_id_example"; // PlaylistItem id

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Delete a related item by id for radioShows.
[apiInstance playlistItemPrototypeDestroyByIdRadioShowsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeDestroyByIdRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShows | 
 **_id** | **NSString***| PlaylistItem id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeFindByIdPlayerSources**
```objc
-(NSNumber*) playlistItemPrototypeFindByIdPlayerSourcesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Find a related item by id for playerSources.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for playerSources
NSString* _id = @"_id_example"; // PlaylistItem id

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Find a related item by id for playerSources.
[apiInstance playlistItemPrototypeFindByIdPlayerSourcesWithFk:fk
              _id:_id
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeFindByIdPlayerSources: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for playerSources | 
 **_id** | **NSString***| PlaylistItem id | 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeFindByIdPlayerTracks**
```objc
-(NSNumber*) playlistItemPrototypeFindByIdPlayerTracksWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Find a related item by id for playerTracks.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for playerTracks
NSString* _id = @"_id_example"; // PlaylistItem id

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Find a related item by id for playerTracks.
[apiInstance playlistItemPrototypeFindByIdPlayerTracksWithFk:fk
              _id:_id
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeFindByIdPlayerTracks: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for playerTracks | 
 **_id** | **NSString***| PlaylistItem id | 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeFindByIdRadioShows**
```objc
-(NSNumber*) playlistItemPrototypeFindByIdRadioShowsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Find a related item by id for radioShows.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShows
NSString* _id = @"_id_example"; // PlaylistItem id

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Find a related item by id for radioShows.
[apiInstance playlistItemPrototypeFindByIdRadioShowsWithFk:fk
              _id:_id
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeFindByIdRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShows | 
 **_id** | **NSString***| PlaylistItem id | 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeGetPlayerSources**
```objc
-(NSNumber*) playlistItemPrototypeGetPlayerSourcesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPPlayerSource>* output, NSError* error)) handler;
```

Queries playerSources of PlaylistItem.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id
NSString* filter = @"filter_example"; //  (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Queries playerSources of PlaylistItem.
[apiInstance playlistItemPrototypeGetPlayerSourcesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPPlayerSource>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeGetPlayerSources: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPPlayerSource>***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeGetPlayerTracks**
```objc
-(NSNumber*) playlistItemPrototypeGetPlayerTracksWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPPlayerTrack>* output, NSError* error)) handler;
```

Queries playerTracks of PlaylistItem.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id
NSString* filter = @"filter_example"; //  (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Queries playerTracks of PlaylistItem.
[apiInstance playlistItemPrototypeGetPlayerTracksWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPPlayerTrack>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeGetPlayerTracks: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPPlayerTrack>***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeGetPlaylist**
```objc
-(NSNumber*) playlistItemPrototypeGetPlaylistWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Fetches belongsTo relation playlist.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id
NSNumber* refresh = @true; //  (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Fetches belongsTo relation playlist.
[apiInstance playlistItemPrototypeGetPlaylistWithId:_id
              refresh:refresh
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeGetPlaylist: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeGetRadioShows**
```objc
-(NSNumber*) playlistItemPrototypeGetRadioShowsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRadioShow>* output, NSError* error)) handler;
```

Queries radioShows of PlaylistItem.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id
NSString* filter = @"filter_example"; //  (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Queries radioShows of PlaylistItem.
[apiInstance playlistItemPrototypeGetRadioShowsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPRadioShow>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeGetRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPRadioShow>***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid**
```objc
-(NSNumber*) playlistItemPrototypeUpdateAttributesPatchPlaylistItemsidWithId: (NSString*) _id
    data: (HRPPlaylistItem*) data
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id
HRPPlaylistItem* data = [[HRPPlaylistItem alloc] init]; // An object of model property name/value pairs (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance playlistItemPrototypeUpdateAttributesPatchPlaylistItemsidWithId:_id
              data:data
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 
 **data** | [**HRPPlaylistItem***](HRPPlaylistItem*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeUpdateAttributesPutPlaylistItemsid**
```objc
-(NSNumber*) playlistItemPrototypeUpdateAttributesPutPlaylistItemsidWithId: (NSString*) _id
    data: (HRPPlaylistItem*) data
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlaylistItem id
HRPPlaylistItem* data = [[HRPPlaylistItem alloc] init]; // An object of model property name/value pairs (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance playlistItemPrototypeUpdateAttributesPutPlaylistItemsidWithId:_id
              data:data
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeUpdateAttributesPutPlaylistItemsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlaylistItem id | 
 **data** | [**HRPPlaylistItem***](HRPPlaylistItem*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeUpdateByIdPlayerSources**
```objc
-(NSNumber*) playlistItemPrototypeUpdateByIdPlayerSourcesWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPPlayerSource*) data
        completionHandler: (void (^)(HRPPlayerSource* output, NSError* error)) handler;
```

Update a related item by id for playerSources.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for playerSources
NSString* _id = @"_id_example"; // PlaylistItem id
HRPPlayerSource* data = [[HRPPlayerSource alloc] init]; //  (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Update a related item by id for playerSources.
[apiInstance playlistItemPrototypeUpdateByIdPlayerSourcesWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPPlayerSource* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeUpdateByIdPlayerSources: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for playerSources | 
 **_id** | **NSString***| PlaylistItem id | 
 **data** | [**HRPPlayerSource***](HRPPlayerSource*.md)|  | [optional] 

### Return type

[**HRPPlayerSource***](HRPPlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeUpdateByIdPlayerTracks**
```objc
-(NSNumber*) playlistItemPrototypeUpdateByIdPlayerTracksWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPPlayerTrack*) data
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Update a related item by id for playerTracks.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for playerTracks
NSString* _id = @"_id_example"; // PlaylistItem id
HRPPlayerTrack* data = [[HRPPlayerTrack alloc] init]; //  (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Update a related item by id for playerTracks.
[apiInstance playlistItemPrototypeUpdateByIdPlayerTracksWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeUpdateByIdPlayerTracks: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for playerTracks | 
 **_id** | **NSString***| PlaylistItem id | 
 **data** | [**HRPPlayerTrack***](HRPPlayerTrack*.md)|  | [optional] 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemPrototypeUpdateByIdRadioShows**
```objc
-(NSNumber*) playlistItemPrototypeUpdateByIdRadioShowsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPRadioShow*) data
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Update a related item by id for radioShows.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioShows
NSString* _id = @"_id_example"; // PlaylistItem id
HRPRadioShow* data = [[HRPRadioShow alloc] init]; //  (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Update a related item by id for radioShows.
[apiInstance playlistItemPrototypeUpdateByIdRadioShowsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemPrototypeUpdateByIdRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioShows | 
 **_id** | **NSString***| PlaylistItem id | 
 **data** | [**HRPRadioShow***](HRPRadioShow*.md)|  | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemReplaceById**
```objc
-(NSNumber*) playlistItemReplaceByIdWithId: (NSString*) _id
    data: (HRPPlaylistItem*) data
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPPlaylistItem* data = [[HRPPlaylistItem alloc] init]; // Model instance data (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance playlistItemReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPPlaylistItem***](HRPPlaylistItem*.md)| Model instance data | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemReplaceOrCreate**
```objc
-(NSNumber*) playlistItemReplaceOrCreateWithData: (HRPPlaylistItem*) data
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlaylistItem* data = [[HRPPlaylistItem alloc] init]; // Model instance data (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance playlistItemReplaceOrCreateWithData:data
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlaylistItem***](HRPPlaylistItem*.md)| Model instance data | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemUpdateAll**
```objc
-(NSNumber*) playlistItemUpdateAllWithWhere: (NSString*) where
    data: (HRPPlaylistItem*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPPlaylistItem* data = [[HRPPlaylistItem alloc] init]; // An object of model property name/value pairs (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance playlistItemUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPPlaylistItem***](HRPPlaylistItem*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemUpsertPatchPlaylistItems**
```objc
-(NSNumber*) playlistItemUpsertPatchPlaylistItemsWithData: (HRPPlaylistItem*) data
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlaylistItem* data = [[HRPPlaylistItem alloc] init]; // Model instance data (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance playlistItemUpsertPatchPlaylistItemsWithData:data
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemUpsertPatchPlaylistItems: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlaylistItem***](HRPPlaylistItem*.md)| Model instance data | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemUpsertPutPlaylistItems**
```objc
-(NSNumber*) playlistItemUpsertPutPlaylistItemsWithData: (HRPPlaylistItem*) data
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlaylistItem* data = [[HRPPlaylistItem alloc] init]; // Model instance data (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance playlistItemUpsertPutPlaylistItemsWithData:data
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemUpsertPutPlaylistItems: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlaylistItem***](HRPPlaylistItem*.md)| Model instance data | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playlistItemUpsertWithWhere**
```objc
-(NSNumber*) playlistItemUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPPlaylistItem*) data
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPPlaylistItem* data = [[HRPPlaylistItem alloc] init]; // An object of model property name/value pairs (optional)

HRPPlaylistItemApi*apiInstance = [[HRPPlaylistItemApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance playlistItemUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlaylistItemApi->playlistItemUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPPlaylistItem***](HRPPlaylistItem*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

