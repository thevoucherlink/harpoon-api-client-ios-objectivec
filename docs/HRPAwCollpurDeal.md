# HRPAwCollpurDeal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**productId** | **NSNumber*** |  | 
**productName** | **NSString*** |  | 
**storeIds** | **NSString*** |  | 
**isActive** | **NSNumber*** |  | 
**isSuccess** | **NSNumber*** |  | 
**closeState** | **NSNumber*** |  | 
**qtyToReachDeal** | **NSNumber*** |  | 
**purchasesLeft** | **NSNumber*** |  | 
**maximumAllowedPurchases** | **NSNumber*** |  | 
**availableFrom** | **NSDate*** |  | [optional] 
**availableTo** | **NSDate*** |  | [optional] 
**price** | **NSString*** |  | 
**autoClose** | **NSNumber*** |  | 
**name** | **NSString*** |  | 
**_description** | **NSString*** |  | 
**fullDescription** | **NSString*** |  | [optional] 
**dealImage** | **NSString*** |  | 
**isFeatured** | **NSNumber*** |  | [optional] 
**enableCoupons** | **NSNumber*** |  | 
**couponPrefix** | **NSString*** |  | 
**couponExpireAfterDays** | **NSNumber*** |  | [optional] 
**expiredFlag** | **NSNumber*** |  | [optional] 
**sentBeforeFlag** | **NSNumber*** |  | [optional] 
**isSuccessedFlag** | **NSNumber*** |  | [optional] 
**awCollpurDealPurchases** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


