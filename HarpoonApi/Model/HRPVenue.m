#import "HRPVenue.h"

@implementation HRPVenue

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"name": @"name", @"address": @"address", @"city": @"city", @"country": @"country", @"coordinates": @"coordinates", @"phone": @"phone", @"email": @"email", @"brand": @"brand", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"name", @"address", @"city", @"country", @"coordinates", @"phone", @"email", @"brand", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
