#import "HRPEventTicket.h"

@implementation HRPEventTicket

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.price = @0.0;
    self.qtyBought = @0.0;
    self.qtyTotal = @0.0;
    self.qtyLeft = @0.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"name": @"name", @"price": @"price", @"qtyBought": @"qtyBought", @"qtyTotal": @"qtyTotal", @"qtyLeft": @"qtyLeft", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"name", @"price", @"qtyBought", @"qtyTotal", @"qtyLeft", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
