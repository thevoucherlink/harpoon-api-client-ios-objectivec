#import "HRPCheckoutAgreement.h"

@implementation HRPCheckoutAgreement

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"name": @"name", @"contentHeight": @"contentHeight", @"checkboxText": @"checkboxText", @"isActive": @"isActive", @"isHtml": @"isHtml", @"id": @"_id", @"content": @"content" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"name", @"contentHeight", @"checkboxText", @"content"];
  return [optionalProperties containsObject:propertyName];
}

@end
