#import "HRPCompetitionAttendee.h"

@implementation HRPCompetitionAttendee

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.gender = @"other";
    self.followingCount = @0.0;
    self.followerCount = @0.0;
    self.isFollowed = @0;
    self.isFollower = @0;
    self.notificationCount = @0.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"joinedAt": @"joinedAt", @"email": @"email", @"firstName": @"firstName", @"lastName": @"lastName", @"dob": @"dob", @"gender": @"gender", @"password": @"password", @"profilePicture": @"profilePicture", @"cover": @"cover", @"profilePictureUpload": @"profilePictureUpload", @"coverUpload": @"coverUpload", @"metadata": @"metadata", @"connection": @"connection", @"followingCount": @"followingCount", @"followerCount": @"followerCount", @"isFollowed": @"isFollowed", @"isFollower": @"isFollower", @"notificationCount": @"notificationCount", @"authorizationCode": @"authorizationCode", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"joinedAt", @"dob", @"gender", @"profilePicture", @"cover", @"profilePictureUpload", @"coverUpload", @"metadata", @"connection", @"followingCount", @"followerCount", @"isFollowed", @"isFollower", @"notificationCount", @"authorizationCode", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
