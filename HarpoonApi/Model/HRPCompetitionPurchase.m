#import "HRPCompetitionPurchase.h"

@implementation HRPCompetitionPurchase

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.unlockTime = @15.0;
    self.isAvailable = @0;
    self.status = @"unknown";
    self.basePrice = @0.0;
    self.ticketPrice = @9.0;
    self.currency = @"EUR";
    self.chanceCount = @9.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"orderId": @"orderId", @"name": @"name", @"competition": @"competition", @"redemptionType": @"redemptionType", @"unlockTime": @"unlockTime", @"code": @"code", @"qrcode": @"qrcode", @"isAvailable": @"isAvailable", @"status": @"status", @"createdAt": @"createdAt", @"expiredAt": @"expiredAt", @"redeemedAt": @"redeemedAt", @"redeemedByTerminal": @"redeemedByTerminal", @"basePrice": @"basePrice", @"ticketPrice": @"ticketPrice", @"currency": @"currency", @"attendee": @"attendee", @"chanceCount": @"chanceCount", @"winner": @"winner", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"orderId", @"name", @"competition", @"redemptionType", @"unlockTime", @"code", @"qrcode", @"isAvailable", @"status", @"createdAt", @"expiredAt", @"redeemedAt", @"redeemedByTerminal", @"basePrice", @"ticketPrice", @"currency", @"attendee", @"chanceCount", @"winner", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
