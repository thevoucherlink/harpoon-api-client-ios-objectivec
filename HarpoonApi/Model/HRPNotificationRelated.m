#import "HRPNotificationRelated.h"

@implementation HRPNotificationRelated

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.customerId = @0.0;
    self.brandId = @0.0;
    self.venueId = @0.0;
    self.competitionId = @0.0;
    self.couponId = @0.0;
    self.eventId = @0.0;
    self.dealPaidId = @0.0;
    self.dealGroupId = @0.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"customerId": @"customerId", @"brandId": @"brandId", @"venueId": @"venueId", @"competitionId": @"competitionId", @"couponId": @"couponId", @"eventId": @"eventId", @"dealPaidId": @"dealPaidId", @"dealGroupId": @"dealGroupId", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"customerId", @"brandId", @"venueId", @"competitionId", @"couponId", @"eventId", @"dealPaidId", @"dealGroupId", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
