#import <Foundation/Foundation.h>
#import "HRPObject.h"

/**
* harpoon-api
* Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
*
* OpenAPI spec version: 1.1.1
* 
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#import "HRPStripeDiscount.h"
#import "HRPStripeInvoiceItem.h"
#import "HRPStripeSubscription.h"


@protocol HRPStripeInvoice
@end

@interface HRPStripeInvoice : HRPObject


@property(nonatomic) NSString* object;

@property(nonatomic) NSNumber* amountDue;

@property(nonatomic) NSNumber* applicationFee;

@property(nonatomic) NSNumber* attemptCount;

@property(nonatomic) NSNumber* attempted;

@property(nonatomic) NSString* charge;

@property(nonatomic) NSNumber* closed;

@property(nonatomic) NSString* currency;

@property(nonatomic) NSNumber* date;

@property(nonatomic) NSString* _description;

@property(nonatomic) NSNumber* endingBalance;

@property(nonatomic) NSNumber* forgiven;

@property(nonatomic) NSNumber* livemode;

@property(nonatomic) NSString* metadata;

@property(nonatomic) NSNumber* nextPaymentAttempt;

@property(nonatomic) NSNumber* paid;

@property(nonatomic) NSNumber* periodEnd;

@property(nonatomic) NSNumber* periodStart;

@property(nonatomic) NSString* receiptNumber;

@property(nonatomic) NSNumber* subscriptionProrationDate;

@property(nonatomic) NSNumber* subtotal;

@property(nonatomic) NSNumber* tax;

@property(nonatomic) NSNumber* taxPercent;

@property(nonatomic) NSNumber* total;

@property(nonatomic) NSNumber* webhooksDeliveredAt;

@property(nonatomic) NSString* customer;

@property(nonatomic) HRPStripeDiscount* discount;

@property(nonatomic) NSString* statmentDescriptor;

@property(nonatomic) HRPStripeSubscription* subscription;

@property(nonatomic) NSArray<HRPStripeInvoiceItem>* lines;

@property(nonatomic) NSNumber* _id;

@end
