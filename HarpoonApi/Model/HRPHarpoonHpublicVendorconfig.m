#import "HRPHarpoonHpublicVendorconfig.h"

@implementation HRPHarpoonHpublicVendorconfig

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"type": @"type", @"category": @"category", @"logo": @"logo", @"cover": @"cover", @"facebookPageId": @"facebookPageId", @"facebookPageToken": @"facebookPageToken", @"facebookEventEnabled": @"facebookEventEnabled", @"facebookEventUpdatedAt": @"facebookEventUpdatedAt", @"facebookEventCount": @"facebookEventCount", @"facebookFeedEnabled": @"facebookFeedEnabled", @"facebookFeedUpdatedAt": @"facebookFeedUpdatedAt", @"facebookFeedCount": @"facebookFeedCount", @"stripePublishableKey": @"stripePublishableKey", @"stripeRefreshToken": @"stripeRefreshToken", @"stripeAccessToken": @"stripeAccessToken", @"feesEventTicket": @"feesEventTicket", @"stripeUserId": @"stripeUserId", @"vendorconfigId": @"vendorconfigId", @"createdAt": @"createdAt", @"updatedAt": @"updatedAt" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[];
  return [optionalProperties containsObject:propertyName];
}

@end
