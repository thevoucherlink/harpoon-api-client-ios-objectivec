#import "HRPUdropshipVendorProductAssoc.h"

@implementation HRPUdropshipVendorProductAssoc

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"isUdmulti": @"isUdmulti", @"id": @"_id", @"isAttribute": @"isAttribute", @"productId": @"productId", @"vendorId": @"vendorId", @"udropshipVendor": @"udropshipVendor", @"catalogProduct": @"catalogProduct" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"isUdmulti", @"udropshipVendor", @"catalogProduct"];
  return [optionalProperties containsObject:propertyName];
}

@end
