#import "HRPRssFeedGroup.h"

@implementation HRPRssFeedGroup

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.sortOrder = @0.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"name": @"name", @"sortOrder": @"sortOrder", @"id": @"_id", @"appId": @"appId", @"rssFeeds": @"rssFeeds" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"sortOrder", @"_id", @"appId", @"rssFeeds"];
  return [optionalProperties containsObject:propertyName];
}

@end
