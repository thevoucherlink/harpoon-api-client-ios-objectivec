#import "HRPStripeSubscription.h"

@implementation HRPStripeSubscription

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"applicationFeePercent": @"applicationFeePercent", @"cancelAtPeriodEnd": @"cancelAtPeriodEnd", @"canceledAt": @"canceledAt", @"currentPeriodEnd": @"currentPeriodEnd", @"currentPeriodStart": @"currentPeriodStart", @"metadata": @"metadata", @"quantity": @"quantity", @"start": @"start", @"status": @"status", @"taxPercent": @"taxPercent", @"trialEnd": @"trialEnd", @"trialStart": @"trialStart", @"customer": @"customer", @"discount": @"discount", @"plan": @"plan", @"object": @"object", @"endedAt": @"endedAt", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"applicationFeePercent", @"cancelAtPeriodEnd", @"canceledAt", @"currentPeriodEnd", @"currentPeriodStart", @"metadata", @"quantity", @"start", @"status", @"taxPercent", @"trialEnd", @"trialStart", @"discount", @"object", @"endedAt", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
