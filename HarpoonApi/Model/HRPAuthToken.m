#import "HRPAuthToken.h"

@implementation HRPAuthToken

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"accessToken": @"accessToken", @"expiresIn": @"expiresIn", @"scope": @"scope", @"refreshToken": @"refreshToken", @"tokenType": @"tokenType", @"kid": @"kid", @"macAlgorithm": @"macAlgorithm", @"macKey": @"macKey" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"expiresIn", @"scope", @"refreshToken", @"tokenType", @"kid", @"macAlgorithm", @"macKey"];
  return [optionalProperties containsObject:propertyName];
}

@end
