#import "HRPUdropshipVendor.h"

@implementation HRPUdropshipVendor

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"vendorName": @"vendorName", @"vendorAttn": @"vendorAttn", @"email": @"email", @"street": @"street", @"city": @"city", @"zip": @"zip", @"countryId": @"countryId", @"regionId": @"regionId", @"region": @"region", @"telephone": @"telephone", @"fax": @"fax", @"status": @"status", @"password": @"password", @"passwordHash": @"passwordHash", @"passwordEnc": @"passwordEnc", @"carrierCode": @"carrierCode", @"notifyNewOrder": @"notifyNewOrder", @"labelType": @"labelType", @"testMode": @"testMode", @"handlingFee": @"handlingFee", @"upsShipperNumber": @"upsShipperNumber", @"customDataCombined": @"customDataCombined", @"customVarsCombined": @"customVarsCombined", @"emailTemplate": @"emailTemplate", @"urlKey": @"urlKey", @"randomHash": @"randomHash", @"createdAt": @"createdAt", @"notifyLowstock": @"notifyLowstock", @"notifyLowstockQty": @"notifyLowstockQty", @"useHandlingFee": @"useHandlingFee", @"useRatesFallback": @"useRatesFallback", @"allowShippingExtraCharge": @"allowShippingExtraCharge", @"defaultShippingExtraChargeSuffix": @"defaultShippingExtraChargeSuffix", @"defaultShippingExtraChargeType": @"defaultShippingExtraChargeType", @"defaultShippingExtraCharge": @"defaultShippingExtraCharge", @"isExtraChargeShippingDefault": @"isExtraChargeShippingDefault", @"defaultShippingId": @"defaultShippingId", @"billingUseShipping": @"billingUseShipping", @"billingEmail": @"billingEmail", @"billingTelephone": @"billingTelephone", @"billingFax": @"billingFax", @"billingVendorAttn": @"billingVendorAttn", @"billingStreet": @"billingStreet", @"billingCity": @"billingCity", @"billingZip": @"billingZip", @"billingCountryId": @"billingCountryId", @"billingRegionId": @"billingRegionId", @"billingRegion": @"billingRegion", @"subdomainLevel": @"subdomainLevel", @"updateStoreBaseUrl": @"updateStoreBaseUrl", @"confirmation": @"confirmation", @"confirmationSent": @"confirmationSent", @"rejectReason": @"rejectReason", @"backorderByAvailability": @"backorderByAvailability", @"useReservedQty": @"useReservedQty", @"tiercomRates": @"tiercomRates", @"tiercomFixedRule": @"tiercomFixedRule", @"tiercomFixedRates": @"tiercomFixedRates", @"tiercomFixedCalcType": @"tiercomFixedCalcType", @"tiershipRates": @"tiershipRates", @"tiershipSimpleRates": @"tiershipSimpleRates", @"tiershipUseV2Rates": @"tiershipUseV2Rates", @"vacationMode": @"vacationMode", @"vacationEnd": @"vacationEnd", @"vacationMessage": @"vacationMessage", @"udmemberLimitProducts": @"udmemberLimitProducts", @"udmemberProfileId": @"udmemberProfileId", @"udmemberProfileRefid": @"udmemberProfileRefid", @"udmemberMembershipCode": @"udmemberMembershipCode", @"udmemberMembershipTitle": @"udmemberMembershipTitle", @"udmemberBillingType": @"udmemberBillingType", @"udmemberHistory": @"udmemberHistory", @"udmemberProfileSyncOff": @"udmemberProfileSyncOff", @"udmemberAllowMicrosite": @"udmemberAllowMicrosite", @"udprodTemplateSku": @"udprodTemplateSku", @"vendorTaxClass": @"vendorTaxClass", @"catalogProducts": @"catalogProducts", @"vendorLocations": @"vendorLocations", @"config": @"config", @"partners": @"partners", @"udropshipVendorProducts": @"udropshipVendorProducts", @"harpoonHpublicApplicationpartners": @"harpoonHpublicApplicationpartners", @"harpoonHpublicv12VendorAppCategories": @"harpoonHpublicv12VendorAppCategories" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"zip", @"regionId", @"region", @"telephone", @"fax", @"password", @"passwordHash", @"passwordEnc", @"carrierCode", @"upsShipperNumber", @"customDataCombined", @"customVarsCombined", @"emailTemplate", @"urlKey", @"randomHash", @"createdAt", @"notifyLowstock", @"notifyLowstockQty", @"useHandlingFee", @"useRatesFallback", @"allowShippingExtraCharge", @"defaultShippingExtraChargeSuffix", @"defaultShippingExtraChargeType", @"defaultShippingExtraCharge", @"isExtraChargeShippingDefault", @"defaultShippingId", @"billingTelephone", @"billingFax", @"billingZip", @"billingRegionId", @"billingRegion", @"confirmation", @"confirmationSent", @"rejectReason", @"backorderByAvailability", @"useReservedQty", @"tiercomRates", @"tiercomFixedRule", @"tiercomFixedRates", @"tiercomFixedCalcType", @"tiershipRates", @"tiershipSimpleRates", @"tiershipUseV2Rates", @"vacationMode", @"vacationEnd", @"vacationMessage", @"udmemberLimitProducts", @"udmemberProfileId", @"udmemberProfileRefid", @"udmemberMembershipCode", @"udmemberMembershipTitle", @"udmemberBillingType", @"udmemberHistory", @"udmemberProfileSyncOff", @"udmemberAllowMicrosite", @"udprodTemplateSku", @"vendorTaxClass", @"catalogProducts", @"vendorLocations", @"config", @"partners", @"udropshipVendorProducts", @"harpoonHpublicApplicationpartners", @"harpoonHpublicv12VendorAppCategories"];
  return [optionalProperties containsObject:propertyName];
}

@end
