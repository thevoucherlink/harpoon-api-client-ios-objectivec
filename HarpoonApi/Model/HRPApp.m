#import "HRPApp.h"

@implementation HRPApp

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.category = @"Lifestyle";
    self.keywords = @"buy,claim,offer,events,competitions";
    self.varCopyright = @"© 2016 The Voucher Link Ltd.";
    self.bundle = @"";
    self.urlScheme = @"";
    self.brandFollowOffer = @0;
    self.privacyUrl = @"http://harpoonconnect.com/privacy/";
    self.termsOfServiceUrl = @"http://harpoonconnect.com/terms/";
    self.restrictionAge = @0.0;
    self.storeAndroidUrl = @"";
    self.storeIosUrl = @"";
    self.typeNewsFeed = @"nativeFeed";
    self.status = @"sandbox";
    self.styleOfferList = @"big";
    self.multiConfigTitle = @"Choose your App:";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"internalId": @"internalId", @"vendorId": @"vendorId", @"name": @"name", @"category": @"category", @"description": @"_description", @"keywords": @"keywords", @"copyright": @"varCopyright", @"realm": @"realm", @"bundle": @"bundle", @"urlScheme": @"urlScheme", @"brandFollowOffer": @"brandFollowOffer", @"privacyUrl": @"privacyUrl", @"termsOfServiceUrl": @"termsOfServiceUrl", @"restrictionAge": @"restrictionAge", @"storeAndroidUrl": @"storeAndroidUrl", @"storeIosUrl": @"storeIosUrl", @"typeNewsFeed": @"typeNewsFeed", @"clientSecret": @"clientSecret", @"grantTypes": @"grantTypes", @"scopes": @"scopes", @"status": @"status", @"created": @"created", @"modified": @"modified", @"styleOfferList": @"styleOfferList", @"appConfig": @"appConfig", @"clientToken": @"clientToken", @"multiConfigTitle": @"multiConfigTitle", @"multiConfig": @"multiConfig", @"appId": @"appId", @"parentId": @"parentId", @"rssFeeds": @"rssFeeds", @"rssFeedGroups": @"rssFeedGroups", @"radioStreams": @"radioStreams", @"customers": @"customers", @"appConfigs": @"appConfigs", @"playlists": @"playlists", @"apps": @"apps", @"parent": @"parent" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"internalId", @"vendorId", @"category", @"_description", @"keywords", @"varCopyright", @"realm", @"bundle", @"urlScheme", @"brandFollowOffer", @"privacyUrl", @"termsOfServiceUrl", @"restrictionAge", @"storeAndroidUrl", @"storeIosUrl", @"typeNewsFeed", @"clientSecret", @"grantTypes", @"scopes", @"status", @"created", @"modified", @"styleOfferList", @"appConfig", @"clientToken", @"multiConfigTitle", @"multiConfig", @"appId", @"parentId", @"rssFeeds", @"rssFeedGroups", @"radioStreams", @"customers", @"appConfigs", @"playlists", @"apps", @"parent"];
  return [optionalProperties containsObject:propertyName];
}

@end
