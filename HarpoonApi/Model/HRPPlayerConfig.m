#import "HRPPlayerConfig.h"

@implementation HRPPlayerConfig

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.mute = @0;
    self.autostart = @0;
    self.repeat = @0;
    self.controls = @1;
    self.visualPlaylist = @1;
    self.displayTitle = @1;
    self.displayDescription = @1;
    self.stretching = @"uniform";
    self.hlshtml = @0;
    self.primary = @"html5";
    self.flashPlayer = @"/";
    self.baseSkinPath = @"/";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"mute": @"mute", @"autostart": @"autostart", @"repeat": @"repeat", @"controls": @"controls", @"visualPlaylist": @"visualPlaylist", @"displayTitle": @"displayTitle", @"displayDescription": @"displayDescription", @"stretching": @"stretching", @"hlshtml": @"hlshtml", @"primary": @"primary", @"flashPlayer": @"flashPlayer", @"baseSkinPath": @"baseSkinPath", @"preload": @"preload", @"playerAdConfig": @"playerAdConfig", @"playerCaptionConfig": @"playerCaptionConfig", @"skinName": @"skinName", @"skinCss": @"skinCss", @"skinActive": @"skinActive", @"skinInactive": @"skinInactive", @"skinBackground": @"skinBackground", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"mute", @"autostart", @"repeat", @"controls", @"visualPlaylist", @"displayTitle", @"displayDescription", @"stretching", @"hlshtml", @"primary", @"flashPlayer", @"baseSkinPath", @"preload", @"playerAdConfig", @"playerCaptionConfig", @"skinName", @"skinCss", @"skinActive", @"skinInactive", @"skinBackground", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
