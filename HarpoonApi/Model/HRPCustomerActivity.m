#import "HRPCustomerActivity.h"

@implementation HRPCustomerActivity

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"customer": @"customer", @"message": @"message", @"cover": @"cover", @"related": @"related", @"link": @"link", @"actionCode": @"actionCode", @"privacyCode": @"privacyCode", @"postedAt": @"postedAt", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"customer", @"message", @"cover", @"related", @"link", @"actionCode", @"privacyCode", @"postedAt", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
