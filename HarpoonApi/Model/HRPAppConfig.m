#import "HRPAppConfig.h"

@implementation HRPAppConfig

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.featureRadio = @0;
    self.featureRadioAutoPlay = @0;
    self.featureNews = @1;
    self.featureEvents = @1;
    self.featureEventsCategorized = @1;
    self.featureEventsAttendeeList = @1;
    self.featureCommunity = @1;
    self.featureCommunityCategorized = @1;
    self.featureCompetitions = @1;
    self.featureCompetitionsAttendeeList = @1;
    self.featureOffers = @1;
    self.featureOffersCategorized = @1;
    self.featureBrandFollow = @1;
    self.featureBrandFollowerList = @1;
    self.featureWordpressConnect = @0;
    self.featureLogin = @1;
    self.featureFacebookLogin = @1;
    self.featurePhoneVerification = @1;
    self.featurePlayer = @0;
    self.featurePlayerMainButton = @0;
    self.featureGoogleAnalytics = @0;
    self.playerMainButtonTitle = @"Main";
    self.color = @"black";
    self.pushNotificationProvider = @"none";
    self.iOSVersion = @"1.0.0";
    self.iOSBuild = @"1";
    self.iOSTeamId = @"8LXTLH6AD9";
    self.iOSAppleId = @"dev@harpoonconnect.com";
    self.androidBuild = @"1";
    self.androidVersion = @"1.0.0";
    self.typeNewsFocusSource = @"rssContent";
    self.sponsorSplashImgDisplayTime = @0.0;
    self.featureTritonPlayer = @0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"imgAppLogo": @"imgAppLogo", @"imgSplashScreen": @"imgSplashScreen", @"imgBrandLogo": @"imgBrandLogo", @"imgNavBarLogo": @"imgNavBarLogo", @"imgBrandPlaceholder": @"imgBrandPlaceholder", @"imgMenuBackground": @"imgMenuBackground", @"featureRadio": @"featureRadio", @"featureRadioAutoPlay": @"featureRadioAutoPlay", @"featureNews": @"featureNews", @"featureEvents": @"featureEvents", @"featureEventsCategorized": @"featureEventsCategorized", @"featureEventsAttendeeList": @"featureEventsAttendeeList", @"featureCommunity": @"featureCommunity", @"featureCommunityCategorized": @"featureCommunityCategorized", @"featureCompetitions": @"featureCompetitions", @"featureCompetitionsAttendeeList": @"featureCompetitionsAttendeeList", @"featureOffers": @"featureOffers", @"featureOffersCategorized": @"featureOffersCategorized", @"featureBrandFollow": @"featureBrandFollow", @"featureBrandFollowerList": @"featureBrandFollowerList", @"featureWordpressConnect": @"featureWordpressConnect", @"featureLogin": @"featureLogin", @"featureHarpoonLogin": @"featureHarpoonLogin", @"featureFacebookLogin": @"featureFacebookLogin", @"featurePhoneVerification": @"featurePhoneVerification", @"featurePlayer": @"featurePlayer", @"featurePlayerMainButton": @"featurePlayerMainButton", @"featureGoogleAnalytics": @"featureGoogleAnalytics", @"playerMainButtonTitle": @"playerMainButtonTitle", @"playerConfig": @"playerConfig", @"menu": @"menu", @"color": @"color", @"colors": @"colors", @"googleAppId": @"googleAppId", @"iOSAppId": @"iOSAppId", @"googleAnalyticsTracking": @"googleAnalyticsTracking", @"loginExternal": @"loginExternal", @"rssWebViewCss": @"rssWebViewCss", @"ads": @"ads", @"iOSAds": @"iOSAds", @"androidAds": @"androidAds", @"pushNotificationProvider": @"pushNotificationProvider", @"batchConfig": @"batchConfig", @"urbanAirshipConfig": @"urbanAirshipConfig", @"pushWooshConfig": @"pushWooshConfig", @"facebookConfig": @"facebookConfig", @"twitterConfig": @"twitterConfig", @"rssTags": @"rssTags", @"connectTo": @"connectTo", @"iOSVersion": @"iOSVersion", @"iOSBuild": @"iOSBuild", @"iOSGoogleServices": @"iOSGoogleServices", @"iOSTeamId": @"iOSTeamId", @"iOSAppleId": @"iOSAppleId", @"iOSPushNotificationPrivateKey": @"iOSPushNotificationPrivateKey", @"iOSPushNotificationCertificate": @"iOSPushNotificationCertificate", @"iOSPushNotificationPem": @"iOSPushNotificationPem", @"iOSPushNotificationPassword": @"iOSPushNotificationPassword", @"iOSReleaseNotes": @"iOSReleaseNotes", @"androidBuild": @"androidBuild", @"androidVersion": @"androidVersion", @"androidGoogleServices": @"androidGoogleServices", @"androidGoogleApiKey": @"androidGoogleApiKey", @"androidGoogleCloudMessagingSenderId": @"androidGoogleCloudMessagingSenderId", @"androidKey": @"androidKey", @"androidKeystore": @"androidKeystore", @"androidKeystoreConfig": @"androidKeystoreConfig", @"androidApk": @"androidApk", @"customerDetailsForm": @"customerDetailsForm", @"contact": @"contact", @"imgSponsorMenu": @"imgSponsorMenu", @"imgSponsorSplash": @"imgSponsorSplash", @"audioSponsorPreRollAd": @"audioSponsorPreRollAd", @"name": @"name", @"isChildConfig": @"isChildConfig", @"typeNewsFocusSource": @"typeNewsFocusSource", @"sponsorSplashImgDisplayTime": @"sponsorSplashImgDisplayTime", @"radioSessionInterval": @"radioSessionInterval", @"featureRadioSession": @"featureRadioSession", @"wordpressShareUrlStub": @"wordpressShareUrlStub", @"iosUriScheme": @"iosUriScheme", @"androidUriScheme": @"androidUriScheme", @"branchConfig": @"branchConfig", @"defaultRadioStreamId": @"defaultRadioStreamId", @"featureTritonPlayer": @"featureTritonPlayer", @"id": @"_id", @"appId": @"appId", @"app": @"app" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"imgAppLogo", @"imgSplashScreen", @"imgBrandLogo", @"imgNavBarLogo", @"imgBrandPlaceholder", @"imgMenuBackground", @"featureRadio", @"featureRadioAutoPlay", @"featureNews", @"featureEvents", @"featureEventsCategorized", @"featureEventsAttendeeList", @"featureCommunity", @"featureCommunityCategorized", @"featureCompetitions", @"featureCompetitionsAttendeeList", @"featureOffers", @"featureOffersCategorized", @"featureBrandFollow", @"featureBrandFollowerList", @"featureWordpressConnect", @"featureLogin", @"featureHarpoonLogin", @"featureFacebookLogin", @"featurePhoneVerification", @"featurePlayer", @"featurePlayerMainButton", @"featureGoogleAnalytics", @"playerMainButtonTitle", @"playerConfig", @"menu", @"color", @"colors", @"googleAppId", @"iOSAppId", @"googleAnalyticsTracking", @"loginExternal", @"rssWebViewCss", @"ads", @"iOSAds", @"androidAds", @"pushNotificationProvider", @"batchConfig", @"urbanAirshipConfig", @"pushWooshConfig", @"facebookConfig", @"twitterConfig", @"rssTags", @"connectTo", @"iOSVersion", @"iOSBuild", @"iOSGoogleServices", @"iOSTeamId", @"iOSAppleId", @"iOSPushNotificationPrivateKey", @"iOSPushNotificationCertificate", @"iOSPushNotificationPem", @"iOSPushNotificationPassword", @"iOSReleaseNotes", @"androidBuild", @"androidVersion", @"androidGoogleServices", @"androidGoogleApiKey", @"androidGoogleCloudMessagingSenderId", @"androidKey", @"androidKeystore", @"androidKeystoreConfig", @"androidApk", @"customerDetailsForm", @"contact", @"imgSponsorMenu", @"imgSponsorSplash", @"audioSponsorPreRollAd", @"name", @"isChildConfig", @"typeNewsFocusSource", @"sponsorSplashImgDisplayTime", @"radioSessionInterval", @"featureRadioSession", @"wordpressShareUrlStub", @"iosUriScheme", @"androidUriScheme", @"branchConfig", @"defaultRadioStreamId", @"featureTritonPlayer", @"_id", @"appId", @"app"];
  return [optionalProperties containsObject:propertyName];
}

@end
