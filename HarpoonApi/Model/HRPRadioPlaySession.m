#import "HRPRadioPlaySession.h"

@implementation HRPRadioPlaySession

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"sessionTime": @"sessionTime", @"playUpdateTime": @"playUpdateTime", @"radioStreamId": @"radioStreamId", @"radioShowId": @"radioShowId", @"playEndTime": @"playEndTime", @"playStartTime": @"playStartTime", @"customerId": @"customerId", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"sessionTime", @"playUpdateTime", @"radioStreamId", @"radioShowId", @"playEndTime", @"playStartTime", @"customerId", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
