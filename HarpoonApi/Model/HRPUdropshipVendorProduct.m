#import "HRPUdropshipVendorProduct.h"

@implementation HRPUdropshipVendorProduct

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"vendorId": @"vendorId", @"productId": @"productId", @"priority": @"priority", @"carrierCode": @"carrierCode", @"vendorSku": @"vendorSku", @"vendorCost": @"vendorCost", @"stockQty": @"stockQty", @"backorders": @"backorders", @"shippingPrice": @"shippingPrice", @"status": @"status", @"reservedQty": @"reservedQty", @"availState": @"availState", @"availDate": @"availDate", @"relationshipStatus": @"relationshipStatus", @"udropshipVendor": @"udropshipVendor", @"catalogProduct": @"catalogProduct" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"carrierCode", @"vendorSku", @"vendorCost", @"stockQty", @"shippingPrice", @"reservedQty", @"availState", @"availDate", @"relationshipStatus", @"udropshipVendor", @"catalogProduct"];
  return [optionalProperties containsObject:propertyName];
}

@end
