#import "HRPPlayerTrack.h"

@implementation HRPPlayerTrack

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self._default = @0;
    self.order = @0.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"file": @"file", @"type": @"type", @"label": @"label", @"default": @"_default", @"order": @"order", @"id": @"_id", @"playlistItemId": @"playlistItemId", @"playlistItem": @"playlistItem" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"file", @"type", @"label", @"_default", @"order", @"_id", @"playlistItemId", @"playlistItem"];
  return [optionalProperties containsObject:propertyName];
}

@end
