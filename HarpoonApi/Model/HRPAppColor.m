#import "HRPAppColor.h"

@implementation HRPAppColor

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.appPrimary = @"#027fd3";
    self.appSecondary = @"#26ff00";
    self.textPrimary = @"#049975";
    self.textSecondary = @"#4e5eea";
    self.radioNavBar = @"#87878c";
    self.radioPlayer = @"#af2768";
    self.radioProgressBar = @"#ff00c9";
    self.menuBackground = @"#000000";
    self.radioTextSecondary = @"#000000";
    self.radioTextPrimary = @"#000000";
    self.menuItem = @"#000000";
    self.appGradientPrimary = @"#000000";
    self.feedSegmentIndicator = @"#ff0bc6";
    self.radioPlayButton = @"#000000";
    self.radioPauseButton = @"#000000";
    self.radioPlayButtonBackground = @"#000000";
    self.radioPauseButtonBackground = @"#000000";
    self.radioPlayerItem = @"#000000";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"appPrimary": @"appPrimary", @"appSecondary": @"appSecondary", @"textPrimary": @"textPrimary", @"textSecondary": @"textSecondary", @"radioNavBar": @"radioNavBar", @"radioPlayer": @"radioPlayer", @"radioProgressBar": @"radioProgressBar", @"menuBackground": @"menuBackground", @"radioTextSecondary": @"radioTextSecondary", @"radioTextPrimary": @"radioTextPrimary", @"menuItem": @"menuItem", @"appGradientPrimary": @"appGradientPrimary", @"feedSegmentIndicator": @"feedSegmentIndicator", @"radioPlayButton": @"radioPlayButton", @"radioPauseButton": @"radioPauseButton", @"radioPlayButtonBackground": @"radioPlayButtonBackground", @"radioPauseButtonBackground": @"radioPauseButtonBackground", @"radioPlayerItem": @"radioPlayerItem" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"appSecondary", @"textPrimary", @"textSecondary", @"radioNavBar", @"radioPlayer", @"radioProgressBar", @"menuBackground", @"radioTextSecondary", @"radioTextPrimary", @"menuItem", @"appGradientPrimary", @"feedSegmentIndicator", @"radioPlayButton", @"radioPauseButton", @"radioPlayButtonBackground", @"radioPauseButtonBackground", @"radioPlayerItem"];
  return [optionalProperties containsObject:propertyName];
}

@end
