#import "HRPAwCollpurDealPurchases.h"

@implementation HRPAwCollpurDealPurchases

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"dealId": @"dealId", @"orderId": @"orderId", @"orderItemId": @"orderItemId", @"qtyPurchased": @"qtyPurchased", @"qtyWithCoupons": @"qtyWithCoupons", @"customerName": @"customerName", @"customerId": @"customerId", @"purchaseDateTime": @"purchaseDateTime", @"shippingAmount": @"shippingAmount", @"qtyOrdered": @"qtyOrdered", @"refundState": @"refundState", @"isSuccessedFlag": @"isSuccessedFlag", @"awCollpurCoupon": @"awCollpurCoupon" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"purchaseDateTime", @"refundState", @"isSuccessedFlag", @"awCollpurCoupon"];
  return [optionalProperties containsObject:propertyName];
}

@end
