#import "HRPAwEventbookingEvent.h"

@implementation HRPAwEventbookingEvent

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"productId": @"productId", @"isEnabled": @"isEnabled", @"eventStartDate": @"eventStartDate", @"eventEndDate": @"eventEndDate", @"dayCountBeforeSendReminderLetter": @"dayCountBeforeSendReminderLetter", @"isReminderSend": @"isReminderSend", @"isTermsEnabled": @"isTermsEnabled", @"generatePdfTickets": @"generatePdfTickets", @"redeemRoles": @"redeemRoles", @"location": @"location", @"tickets": @"tickets", @"attributes": @"attributes", @"purchasedTickets": @"purchasedTickets" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"productId", @"isEnabled", @"eventStartDate", @"eventEndDate", @"dayCountBeforeSendReminderLetter", @"isTermsEnabled", @"redeemRoles", @"location", @"tickets", @"attributes", @"purchasedTickets"];
  return [optionalProperties containsObject:propertyName];
}

@end
