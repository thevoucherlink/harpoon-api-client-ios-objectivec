#
# Be sure to run `pod lib lint HarpoonApi.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = "HarpoonApi"
    s.version          = "1.2.30"

    s.summary          = "harpoon-api"
    s.description      = <<-DESC
                         Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
                         DESC

    s.platform     = :ios, '7.0'
    s.requires_arc = true

    s.framework    = 'SystemConfiguration'

    s.homepage     = "https://bitbucket.org/thevoucherlink/harpoon-api-client-ios-objectivec"
    s.license      = "Apache License, Version 2.0"
    s.source       = { :git => "https://bitbucket.org/thevoucherlink/harpoon-api-client-ios-objectivec.git", :tag => "#{s.version}" }
    s.author       = { "Harpoon Connect" => "hello@harpoonconnect.com" }

    s.source_files = 'HarpoonApi/**/*.{m,h}'
    s.public_header_files = 'HarpoonApi/**/*.h'


    s.dependency 'AFNetworking', '~> 3'
    s.dependency 'JSONModel', '~> 1.2'
    s.dependency 'ISO8601', '~> 0.5'
end

